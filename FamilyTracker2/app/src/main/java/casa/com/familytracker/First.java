package casa.com.familytracker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.multidex.myapplication.R;

import org.w3c.dom.Text;


public class First extends Fragment {
    View view;
    private static final String ARG_LAYOUT_RES_ID = "imageId";
    private int imageId;
    private TextView title;
    private TextView disc;
    private ImageView imageView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_first, container, false);
        title = (TextView) view.findViewById(R.id.textTitle);
        disc = (TextView) view.findViewById(R.id.discription);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.creatg);

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "Fonts/action.ttf");
        disc.setTypeface(type);
        title.setTypeface(type);
        disc.setText("Create your own groups of your loved ones, friends, teammates for FREE.");
        title.setText("Create Group");

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}