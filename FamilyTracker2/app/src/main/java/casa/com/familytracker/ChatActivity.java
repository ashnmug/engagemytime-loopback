package casa.com.familytracker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.os.Handler;
import android.preference.Preference;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.google.android.gms.ads.AdView;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import casa.com.familytracker.gcm.MyGcmPushReceiver;
import casa.com.familytracker.gcm.NotiLstner;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static casa.com.familytracker.API.getImageDlg;


public class ChatActivity extends AppCompatActivity implements HttpProcessor.HttpResponser,NotiLstner, View.OnClickListener {
    EmojiconEditText msg;
    ImageView send;
    private boolean side = false;
    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private Handler handler;
    private Context context;
    private String string;
    ImageView emojiImageView;

    private AdView mAdView;

    View rootView;
    EmojIconActions emojIcon;
    private String msgText;
    private String id, name, url;
    private String message;
    private ArrayList<Preference> arraylist;
    Thread background;
    ImageView tlbrImg, bckBtn;
    TextView tlbrTitle;
    private SessionManager sessionManager;
    private String groupid;
    private String img_str = "";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    ImageView imageChat, select;
    private Bitmap bitmap;
    CircleImageView tImg;
    LinearLayout line1Image;
    private Container containerTemp;
    private ChatMessage chatMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        context = ChatActivity.this;
        sessionManager = new SessionManager(context);
        msg = (EmojiconEditText) findViewById(R.id.msgid);
        rootView = findViewById(R.id.root);
        send = (ImageView) findViewById(R.id.submitid);
        listView = (ListView) findViewById(R.id.msgview);
        imageChat = (ImageView) findViewById(R.id.imageViewChat);
        select = (ImageView) findViewById(R.id.select);
        tImg = (CircleImageView) findViewById(R.id.toolbarImage);
        tImg.setOnClickListener(this);
        tlbrTitle = (TextView) findViewById(R.id.textView);
        mAdView = (AdView) findViewById(R.id.adView);

//        getAdd();
        try {
            name = getIntent().getStringExtra("name");
            url = getIntent().getStringExtra("url");
        } catch (Exception e) {
            name = sessionManager.getKeyUserName();
            url = sessionManager.getProfileImagePath();
        }

        tlbrTitle.setText(name);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Picasso.with(context).load(API.IMAGE_URL + url).placeholder(R.drawable.blnk)
                .into(tImg);


        groupid = getIntent().getStringExtra("groupid");
        emojiImageView = (ImageView) findViewById(R.id.emoji_btn);
        select.setOnClickListener(this);
        emojIcon = new EmojIconActions(this, rootView, msg, emojiImageView);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

            }

            @Override
            public void onKeyboardClose() {

            }
        });

//        getSupportActionBar().setTitle(name + ", " + phone);
        arraylist = new ArrayList<>();

        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.right);
        listView.setAdapter(chatArrayAdapter);

        send.setOnClickListener(this);


        getSmsNoti();
        getRefreshSms();
    }


    @Override
    public void onResume() {
        super.onResume();
        sessionManager.saveOnnoti(false);
    }


    private void getRefreshSms() {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.GET_MESSAGE)
                .add("groupid", groupid)
                .add("userid", sessionManager.getKeyUserId())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.GET_MESSAGE);
        httpProcessor.setHttpResponserListener(this);
    }


    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("gid", groupid);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void sendMessage() {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.SEND_MESSAGE)
                .add("uid", sessionManager.getKeyUserId())
                .add("groupid", groupid)
                .add("image", img_str)
                .add("message", msgText)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, false, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.SEND_MESSAGE);
        httpProcessor.setHttpResponserListener(this);
    }

    private void getSmsNoti() {
        MyGcmPushReceiver.bindListener(new NotiLstner() {
            @Override
            public void messageReceived(Map messageText) {
                if (messageText.get("title").toString().equals("Checked In")) {
                    String message = messageText.get("message").toString();

                    String time = messageText.get("time").toString();
                    String title = messageText.get("name").toString();
                    String imgUrl = messageText.get("image").toString();
                    String latitude = messageText.get("latitude").toString();
                    String longitude = messageText.get("longitude").toString();
                    String uid = messageText.get("uid").toString();
                    String[] times = time.split(" ");
                    setText(message, time, title, uid, imgUrl, latitude, longitude);
//                bundle.setMfrom(time.split(" ")[1]);
                } else {
                    String message = messageText.get("message").toString();
                    String time = messageText.get("time").toString();
                    String title = messageText.get("name").toString();
                    String uid = messageText.get("uid").toString();

                    String[] times = time.split(" ");
                    setText(message, time, title, uid, "", "", "");
                }
            }


        });
    }

    private void getSound(String track) {
        MediaPlayer thePlayer = new MediaPlayer();
        AudioManager am =
                (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        am.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                0);

        try {
            AssetFileDescriptor afd = getAssets().openFd(track);

            thePlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            thePlayer.prepare();
            thePlayer.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setText(final String message, final String time, final String title, final String uid, final String imgUrl, final String latitude, final String longitude) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!uid.equals(sessionManager.getKeyUserId())) {
                    chatArrayAdapter.add(new ChatMessage(false, message, imgUrl, title, time, latitude, longitude, "0", "", uid));
                    chatArrayAdapter.notifyDataSetChanged();
                    if (!sessionManager.isOnnoti()) {
                        getSound("incoming.mp3");
                    }
                } else {
//                    chatArrayAdapter.add(new ChatMessage(true, message, imgUrl, title, time, "fail", longitude, "0", "", uid));

                }
            }
        });
    }



    private void getMessage() {
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);
        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);

            }
        });

        String currentDateandTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
        String[] time = currentDateandTime.split(" ");
        containerTemp = new Container();
        containerTemp.setName(sessionManager.getKeyUserName());
        containerTemp.setMfrom(currentDateandTime);
        containerTemp.setMsg(msgText);
        chatMessage = new ChatMessage(true, containerTemp.getMsg(), img_str, containerTemp.getName(), containerTemp.getMfrom(), "status", "", "0", "", sessionManager.getKeyUserId());
        chatArrayAdapter.add(chatMessage);
        chatArrayAdapter.notifyDataSetChanged();
        sendMessage();
        msg.setText("");

    }


    @Override
    protected void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        sessionManager.saveOnnoti(true);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.

                Intent returnIntent = new Intent();
                returnIntent.putExtra("gid", groupid);
                setResult(Activity.RESULT_OK, returnIntent);

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void responseResult(JSONObject result, String TAG) {
        try {
            if (TAG.equals(API.SEND_MESSAGE)) {

                if (result.optString("status").equals("success")) {
                    chatArrayAdapter.remove(chatMessage);
                    chatArrayAdapter.notifyDataSetChanged();
                    chatArrayAdapter.add(new ChatMessage(true, containerTemp.getMsg(), img_str, containerTemp.getName(), containerTemp.getMfrom(), "fail", "", "0", "", sessionManager.getKeyUserId()));
                    chatArrayAdapter.notifyDataSetChanged();
                }
                getSound("outgoing.mp3");
                img_str = "";
                sessionManager.saveUserPicture(img_str);
            }
            if (TAG.equals(API.GET_MESSAGE)) {
                if (result.optString("status").equals("success")) {

                    JSONArray jsonArray = result.getJSONArray("result");
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(a);
                        Container container = new Container();
                        String name = jsonObject.optString("name");
                        String time = jsonObject.optString("time");
                        String imgUrl = jsonObject.optString("image");
                        String message = jsonObject.optString("message");
                        String latitude = jsonObject.optString("latitude");
                        String like = jsonObject.optString("like");
                        String id = jsonObject.optString("id");
                        String longitude = jsonObject.optString("longitude");
                        String uid = jsonObject.optString("uid");
                        String status = jsonObject.optString("status");
                        String[] times = time.split(" ");
                        container.setName(name);
                        container.setMfrom(time);
                        container.setMsg(message);
                        container.setSubject(like);
                        if (uid.equals(sessionManager.getKeyUserId())) {
                            if (!status.equals("check_in")) {
                                chatArrayAdapter.add(new ChatMessage(true, message, imgUrl, name, time, latitude, longitude, like, id, uid));

                            }
                        } else {
                            if (status.equals("check_in")) {
                                chatArrayAdapter.add(new ChatMessage(false, message, "Checked In", name, time, latitude, longitude, like, id, uid));

                            } else {
                                chatArrayAdapter.add(new ChatMessage(false, message, imgUrl, name, time, latitude, longitude, like, id, uid));
                            }
                        }
                    }
                }
            }


        } catch (Exception ignored) {

        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(ChatActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            } else if (requestCode == 12) {
                msgText = data.getStringExtra("msg");
                img_str = sessionManager.getPic();
                getMessage();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Gallery"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void onCaptureImageResult(Intent data) {

        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        cropImage(data);

    }

    private void cropImage(Intent data) {

        Intent intent = new Intent(context, CropImageActivity.class);
        intent.putExtra("type", String.valueOf(data.getData()));
        startActivityForResult(intent, 12);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        bitmap = null;
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                cropImage(data);
//                imageChat.setImageBitmap(bitmap);
//                img_str = getImageString(bitmap);
//
//                line1Image.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.select) {
            selectImage();
        } else if (v.getId() == R.id.toolbarImage) {
            getImageDlg(context,url);
        } else if (v.getId() == R.id.submitid) {
            msgText = msg.getText().toString();
            if (msgText.equals("")) {
                Toast.makeText(context, "Empty message", Toast.LENGTH_LONG).show();
            } else {

                getMessage();
            }
        }
    }


    @Override
    public void messageReceived(Map messageText) {

    }
}