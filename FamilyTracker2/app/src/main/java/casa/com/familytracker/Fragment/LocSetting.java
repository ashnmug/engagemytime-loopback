package casa.com.familytracker.Fragment;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.example.android.multidex.myapplication.R;
import com.kyleduo.switchbutton.SwitchButton;

import casa.com.familytracker.SessionManager;

public class LocSetting extends Fragment {

    View view;
    SwitchButton isTrip;
    SwitchButton isLoc;
    SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_locsetting, container, false);
        isTrip = (SwitchButton) view.findViewById(R.id.switchTrip);
        isLoc = (SwitchButton) view.findViewById(R.id.switchCur);
        sessionManager = new SessionManager(getActivity());
        getStatus();
        isLoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isLoc.setChecked(true);

                    isLoc.setBackColorRes(R.color.color_switch);
                    sessionManager.saveLocIn(true);
                } else {
                    isLoc.setChecked(false);
                    isLoc.setBackColorRes(R.color.color_switch);
                    sessionManager.saveLocIn(false);
                }
            }
        });
        isTrip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isTrip.setChecked(true);
                    isTrip.setBackColorRes(R.color.color_switch);
                    sessionManager.saveTripIn(true);
                } else {
                    isTrip.setChecked(false);
                    isTrip.setBackColorRes(R.color.color_switch);
                    sessionManager.saveTripIn(false);
                }
            }
        });
        return view;
    }

    private void getStatus() {
        if (sessionManager.isLocationShare()) {
            isLoc.setChecked(true);
            isLoc.setBackColorRes(R.color.color_switch);

        } else {
            isLoc.setChecked(false);
            isLoc.setBackColorRes(R.color.color_switch);
        }

        if (sessionManager.isTripShare()) {
            isTrip.setChecked(true);
            isTrip.setBackColorRes(R.color.color_switch);

        } else {
            isTrip.setChecked(false);
            isTrip.setBackColorRes(R.color.color_switch);
        }
    }
}
