package casa.com.familytracker;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.android.multidex.myapplication.R;
import com.squareup.picasso.Picasso;

import java.io.File;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Casa Curo on 5/20/2017.
 */

public class API {

//    public static final String GCM_PUSH_NOTIFICATION_REGISTER_ID = "http://vivartha.in/demo/V029/api.php?action=get-gcmpushnotify&deviceid=";
    public static final String IMAGE_URL = "http://rbsinfo.co.in/api/locator/";
    public static final String BASE_URL = "http://rbsinfo.co.in/api/locator/locator-api.php";
    public static final String REGISTRATION = "registration";
    public static final String ACTION = "action";
    public static final String LOGIN = "login";
    public static final String ADD_GROUP = "add_group";
    public static final String FEEDBACK = "set_feedback";
    public static final String PASSWORD = "update_pswrd";
    public static final String RATEUS = "set_rating";
    public static final String GET_GROUP = "get_group";
    public static final String GET_MEMBER = "get_members";
    public static final String JOIN_CIRCLE = "join_group";
    public static final String ADD_LOCATION = "add_location";
    public static final String GET_LOCATION = "get_location";
    public static final String CHECK_IN = "send_check";
    public static final String SEND_MESSAGE = "send_message";
    public static final String UPDATE_IMAGE = "update_image";
    public static final String UPDATE__GROUP_IMAGE = "update_gimage";
    public static final String DELETE_MEMBER = "delete_member";
    public static final String DELETE_GROUP = "delete_group";
    public static final String UPDATE__GROUP_NAME = "update_group";
    public static final String UPDATE__ADMIN = "update_admin";
    public static final String UPDATE_PROFILE = "update_profile";
    public static final String GET_MESSAGE = "get_sms";
    public static final String ADD_TRIP = "add_trip";
    public static final String STATUS = "status";
    public static final String bSTATUS = "bstatus";
    public static final String SAVE_TOKEN = "set_token";
    public static final String GET_TRIP = "get_trip";
    public static final String GET_EMAIL = "get-email";


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
    public static void getImageDlg(final Context context, final String url) {
        AlertDialog.Builder ImageDialog = new AlertDialog.Builder(context);

        ImageView showImage = new ImageView(context);
        float scale = context.getResources().getDisplayMetrics().density;
        showImage.setLayoutParams(new LinearLayout.LayoutParams((int) (350 * scale + 0.5f), (int) (300 * scale + 0.5f)));
        showImage.setAdjustViewBounds(true);


        Picasso.with(context).load(API.IMAGE_URL + url).placeholder(R.drawable.loading).error(R.drawable.loading)
                .into(showImage);
        ImageDialog.setView(showImage);
        showImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent st = new Intent(context, CurrentLocation.class);
                st.putExtra("url", API.IMAGE_URL + url);
                st.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(st);
            }
        });
        ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        ImageDialog.show();
    }

    public static void AlertDialogLogOut(final Activity context) {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Are you sure you want to exit ?")
                .setConfirmText("Yes, Log out!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        SessionManager sessionManager = new SessionManager(context);
                        sessionManager.logout();

                        context.startActivity(new Intent(context, LoginScreen.class));
                        context.finish();
                    }
                })
                .show();
    }
    public static void shareCode(Context context) {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Hey!, \n I love using Family Locator application.\nThought you would find it useful too.\n Download the app here.\n https://goo.gl/xv5t8h ");
        try {
            context.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ignored) {

        }
    }
    public static Bitmap getMarkerBitmapFromView(String url, Context context) {

        View customMarkerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        CircleImageView markerImageView = (CircleImageView) customMarkerView.findViewById(R.id.imageView);

        Picasso.with(context)
                .load(API.IMAGE_URL + url).placeholder(R.drawable.blnk).error(R.drawable.blnk)
                .into(markerImageView);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

}
