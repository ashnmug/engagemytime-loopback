package casa.com.familytracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class SessionManager {

    private static final String KEY_DEVICE_ID = "DEVICE_ID";
    private static final String noti = "notification";
    private static final String LAST_UPDATE = "last_update";
    private static final String KEY_IS_LOGGED_IN = "IS_LOGGED_IN";
    private static final String LOC_SHARE = "LOC_SHARE";
    private static final String TRIP_SHARE = "TRIP_SHARE";
    private static final String KEY_USER_ID = "USER_ID";
    private static final String KEY_USER_NAME = "USER_NAME";
    private static final String KEY_EMAIL_ID = "EMAIL_ID";
    private static final String KEY_PHONE_NUMBER = "PHONE_NUMBER";
    //    private static final String KEY_SHOP_NAME = "SHOP_NAME";
    private static final String KEY_ADDRESS = "ADDRESS";
    private static final String KEY_LATITUDE = "LATITUDE";
    private static final String KEY_LONGITUDE = "LONGITUDE";
    //    private static final String KEY_CITY = "CITY";
    private static final String KEY_PROFILE_IMAGE = "KEY_PROFILE_IMAGE";

    private static final String KEY_USER_IMAGE = "KEY_USER_IMAGE";

    private Context context;

    private SharedPreferences pref;

    private Editor editor;

    private String PREFERENCE_NAME = "Family";

    private int MODE_PRIVATE = 0;
    private static final String PROFILE_TYPE = "profile_type";
    private static final String LEVEL = "level";
    private static final String ALERT_LATITUDE = "alert_lati";
    private static final String ALERT_LONGITUDE = "alert_lngi";
    private static final String ON_STATE = "on_state";
    private static final String RATING = "rating";
    private static final String CHATNUMBER = "rating";
    private static final String PASSWORD = "PASSWORD";


    public SessionManager(Context context) {
        // TODO Auto-generated constructor stub

        this.context = context;
        pref = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE);
        editor = pref.edit();
    }

    public void saveDeviceId(String deviceId) {

        editor.putString(KEY_DEVICE_ID, deviceId);
        editor.commit();

    }

    public String getDeviceId() {

        return pref.getString(KEY_DEVICE_ID, "");

    }

    public void saveAlertAddress(String deviceId) {

        editor.putString(PROFILE_TYPE, deviceId);
        editor.commit();

    }

    public String getAlertAddress() {

        return pref.getString(PROFILE_TYPE, "");

    }
    public String getPic() {

        return pref.getString(KEY_USER_IMAGE, "");

    }

    public void saveUserPicture(String bp) {

        editor.putString(KEY_USER_IMAGE, bp);

        editor.commit();
    }


    public String getUserPicture() {
        return pref.getString(KEY_PROFILE_IMAGE, "");
    }

    // method for bitmap to base64
    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

//        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    // method for base64 to bitmap
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public void saveCredentials(String userName, String userId, String emailId, String phoneNumber) {


        editor.putString(KEY_USER_ID, userId);
        editor.putString(KEY_USER_NAME, userName);
        editor.putString(KEY_EMAIL_ID, emailId);
        editor.putString(KEY_PHONE_NUMBER, phoneNumber);
        editor.commit();

    }

    public void setProfileImagePath(String profileImage) {

        editor.putString(KEY_PROFILE_IMAGE, profileImage);
        editor.commit();
    }

    public void saveLoggedIn(boolean status) {

        editor.putBoolean(KEY_IS_LOGGED_IN, status);
        editor.commit();
    }

    public boolean isLoggedIn() {

        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void saveOn(boolean status) {

        editor.putBoolean(ON_STATE, status);
        editor.commit();
    }

    public boolean isOn() {

        return pref.getBoolean(ON_STATE, false);
    }

    public void saveLocIn(boolean status) {

        editor.putBoolean(LOC_SHARE, status);
        editor.commit();
    }

    public void saveTripIn(boolean status) {

        editor.putBoolean(TRIP_SHARE, status);
        editor.commit();
    }


    public String getKeyUserId() {

        return pref.getString(KEY_USER_ID, null);
    }

    public String getProfileImagePath() {

        return pref.getString(KEY_PROFILE_IMAGE, null);
    }


    public boolean isLocationShare() {

        return pref.getBoolean(LOC_SHARE, false);
    }

    public boolean isTripShare() {

        return pref.getBoolean(TRIP_SHARE, false);
    }

    public String getKeyUserName() {

        return pref.getString(KEY_USER_NAME, "");
    }

    public String getKeyEmailId() {

        return pref.getString(KEY_EMAIL_ID, "");
    }

    public String getKeyPhoneNumber() {

        return pref.getString(KEY_PHONE_NUMBER, "");
    }

//    public void setShopName(String shopName) {
//        editor.putString(KEY_SHOP_NAME, shopName);
//        editor.commit();

//    }

    public void setPostOfferOne(String shopName, String address, String latitude, String longitude, String city) {

//        editor.putString(KEY_SHOP_NAME, shopName);
        editor.putString(KEY_ADDRESS, address);
        editor.putString(KEY_LATITUDE, latitude);
        editor.putString(KEY_LONGITUDE, longitude);
//        editor.putString(KEY_CITY, city);
        editor.commit();

    }

    public String getAddress() {
        return pref.getString(KEY_ADDRESS, "");
    }

    public String getLatitude() {
        return pref.getString(KEY_LATITUDE, "0.0");
    }

    public String getLongitude() {
        return pref.getString(KEY_LONGITUDE, "0.0");
    }

//    public String getCity() {
//        return pref.getString(KEY_CITY, "");
//    }

//    public String getKeyShopName() {
//        return pref.getString(KEY_SHOP_NAME, "");
//    }

    public void logout() {
        editor.clear();
        editor.commit();
        //    context.startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
//        ((Activity)context).finish();
    }


    public void setMsg(String msg) {
        editor.putString("msg", msg);
        editor.commit();

    }

    public String getMsg() {

        return pref.getString("msg", "");

    }

    public void setTrip(String msg) {
        editor.putString("tripid", msg);
        editor.commit();

    }

    public String geTrip() {

        return pref.getString("tripid", "");

    }

    public void setLastUpdate(String msg) {
        editor.putString(LAST_UPDATE, msg);
        editor.commit();

    }

    public String getLastUpdate() {

        return pref.getString(LAST_UPDATE, "");

    }

    public void delMsg() {
        editor.remove("msg");
        editor.commit();

    }

    public void setLocation(String lat1, String lng1) {
        editor.putString(KEY_LATITUDE, lat1);
        editor.putString(KEY_LONGITUDE, lng1);
//        editor.putString(KEY_CITY, city);
        editor.commit();
    }

    public void setAlert(String lat1, String lng1) {
        editor.putString(ALERT_LATITUDE, lat1);
        editor.putString(ALERT_LONGITUDE, lng1);
//        editor.putString(KEY_CITY, city);
        editor.commit();
    }

    public void saveBattery(String level) {
        editor.putString(LEVEL, level);
//        editor.putString(KEY_CITY, city);
        editor.commit();
    }

    public String getLevel() {

        return pref.getString(LEVEL, "0");

    }

    public String getAlertLatitude() {

        return pref.getString(ALERT_LATITUDE, "0.0");

    }

    public String getAlertLongitude() {

        return pref.getString(ALERT_LONGITUDE, "0.0");

    }

    public String getRating() {

        return pref.getString(RATING, "0");

    }

    public void setRating(String rating) {
        editor.putString(RATING, rating);
//        editor.putString(KEY_CITY, city);
        editor.commit();
    }

  public int getNumbr(String chatnumber) {

        return pref.getInt(chatnumber, 0);

    }

    public void setNumbr(int rating,String chatnumber) {
        editor.putInt(chatnumber, rating);
        editor.commit();
    }

    public String getPassword() {

        return pref.getString(PASSWORD, "");

    }

    public void setPassword(String pswrd) {
        editor.putString(PASSWORD, pswrd);
//        editor.putString(KEY_CITY, city);
        editor.commit();
    }

    public void saveOnnoti(boolean status) {

        editor.putBoolean(noti, status);
        editor.commit();
    }

    public boolean isOnnoti() {

        return pref.getBoolean(noti, true);
    }


}

