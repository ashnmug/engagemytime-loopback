package casa.com.familytracker;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Casa Curo on 5/17/2017.
 */

public class Container implements Serializable {
    private boolean left;
    private ArrayList<LatLng> latLng;
    private String uid;
    private String refId;

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    private boolean right;
    private String name;
    private String mobile;
    private String subject;
    private String msg;
    private int imageDrawable;
    private String mfrom;
    private String oldGrp;
    private String mto;
    private String image;
    private String latitude;
    private String longitude;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    private String mid;

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }
  public String refId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    private String battery;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public int getResourceIjmage() {
        return imageDrawable;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMfrom() {
        return mfrom;
    }

    public void setMfrom(String mfrom) {
        this.mfrom = mfrom;
    }

    public String getOld() {
        return oldGrp;
    }

    public void setOld(String oldGrp) {
        this.oldGrp = oldGrp;
    }

    public String getMto() {
        return mto;
    }

    public void setMto(String mto) {
        this.mto = mto;
    }


    public void setResourceImage(int backmarker) {
        this.imageDrawable = backmarker;
    }

    public void setLatLng(ArrayList<LatLng> latLng) {
        this.latLng = latLng;
    }

    public ArrayList<LatLng> getLatLng() {
        return latLng;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

}