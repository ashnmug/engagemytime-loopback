package casa.com.familytracker;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.android.multidex.myapplication.R;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

/**
 * Created by Casa Curo on 5/25/2017.
 */

public class TripAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context context;
    private String[] name;
    ArrayList<Container> data;
    String ratingS;
    private SessionManager sessionManager;
    ViewHolder viewHolder = new ViewHolder();

    public TripAdapter(Context context, ArrayList<Container> data) {
        this.context = context;
        this.name = name;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sessionManager = new SessionManager(context);
    }


    static class ViewHolder {
        TextView tripDate;
        TextView txtdctnc;
        CardView cardView;
        TextView tripDateEnd;
        TextView dateM;
        ImageView imageView;
    }


    @Override
    public int getCount() {

        return data.size();


    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        String date = data.get(i).getMobile();
        String[] time = date.split(" ");
        String[] endTime = data.get(i).getMsg().split(" ");
        ArrayList<LatLng> latLngs = data.get(i).getLatLng();
        int second = latLngs.size() / 5;
        int third = 2 * second;
        int four = 3 * second;
        int fifth = 4 * second;
        String distance = getDistance(latLngs, second, third, four, fifth);
        String latlng = String.valueOf(latLngs.get(0).latitude) + "," + String.valueOf(latLngs.get(0).longitude);
        String latlng1 = String.valueOf(latLngs.get(second).latitude) + "," + String.valueOf(latLngs.get(second).longitude);
        String latlng2 = String.valueOf(latLngs.get(third).latitude) + "," + String.valueOf(latLngs.get(third).longitude);
        String latlng3 = String.valueOf(latLngs.get(four).latitude) + "," + String.valueOf(latLngs.get(four).longitude);
        String latlng4 = String.valueOf(latLngs.get(fifth).latitude) + "," + String.valueOf(latLngs.get(fifth).longitude);
        String finalIsed = latlng + "|" + latlng1 + "|" + latlng2 + "|" + latlng3 + "|" + latlng4;
        String url = "http://maps.googleapis.com/maps/api/staticmap?size=400x400&markers=size:small|color:green|" + latlng + "&markers=size:small|color:red|" + latlng4 + "&path=color:blue|weight:8|" + finalIsed + "&sensor=false";


        View vi = convertView;

        if (vi == null) {
            vi = inflater.inflate(R.layout.trip_layout, parent, false);
        }
        viewHolder.tripDate = (TextView) vi.findViewById(R.id.date);
            viewHolder.txtdctnc = (TextView) vi.findViewById(R.id.distnc);
            viewHolder.tripDateEnd = (TextView) vi.findViewById(R.id.endDate);
            viewHolder.dateM = (TextView) vi.findViewById(R.id.dateM);
            viewHolder.imageView = (ImageView) vi.findViewById(R.id.mapStatic);



        Picasso.with(context)
                .load(url).placeholder(R.drawable.cloud)
                .into(viewHolder.imageView);

        viewHolder.txtdctnc.setText(distance + " Km");
        viewHolder.tripDateEnd.setText(time[1]);
        viewHolder.dateM.setText(endTime[0]);
        viewHolder.tripDate.setText(endTime[1]);
        if (i > 0) {
            String[] endTime1 = data.get(i - 1).getMsg().split(" ");
            if (endTime1[0].equals(endTime[0])) {
                viewHolder.dateM.setVisibility(View.GONE);
            } else {
                viewHolder.dateM.setVisibility(View.VISIBLE);
            }
        }


        return vi;

    }

    private String getDistance(ArrayList<LatLng> latLngs, int second, int third, int four, int fifth) {


        Location loc1 = new Location(LocationManager.GPS_PROVIDER);
        Location loc2 = new Location(LocationManager.GPS_PROVIDER);
        Location loc3 = new Location(LocationManager.GPS_PROVIDER);
        Location loc4 = new Location(LocationManager.GPS_PROVIDER);
        Location loc5 = new Location(LocationManager.GPS_PROVIDER);
        loc1.setLatitude(latLngs.get(0).latitude);
        loc1.setLongitude(latLngs.get(0).longitude);
        loc2.setLatitude(latLngs.get(second).latitude);
        loc2.setLongitude(latLngs.get(second).longitude);
        loc3.setLatitude(latLngs.get(third).latitude);
        loc3.setLongitude(latLngs.get(third).longitude);
        loc4.setLatitude(latLngs.get(four).latitude);
        loc4.setLongitude(latLngs.get(four).longitude);
        loc5.setLatitude(latLngs.get(fifth).latitude);
        loc5.setLongitude(latLngs.get(fifth).longitude);

        float distance = loc5.distanceTo(loc4);
        distance = distance + loc4.distanceTo(loc3);
        distance = distance + loc3.distanceTo(loc2);
        distance = distance + loc2.distanceTo(loc1);
        distance = distance / 1000;
        String distString = String.valueOf(new DecimalFormat("##.#").format(distance));

        return distString;
    }

}