package casa.com.familytracker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener, HttpProcessor.HttpResponser {
    Context context;
    private EditText name, password;
    private SessionManager sessionManager;
    TextInputLayout user, pswrd;
    private EditText input1;
    private String refreshedToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getInit();
    }

    @Override
    protected void onPause() {
        sessionManager.saveOn(false);
        super.onPause();
    }

    private void getEmail() {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.GET_EMAIL)
//                .add("andid", deviceId)
                .add("email", input1.getText().toString())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.GET_EMAIL);
        httpProcessor.setHttpResponserListener(new HttpProcessor.HttpResponser() {
            @Override
            public void responseResult(JSONObject result, String TAG) {

                if (result.optString("status").equals("success")) {
                    Toast.makeText(context, result.optString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, result.optString("message"), Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        sessionManager.saveOn(true);
    }

    private void getInit() {
        name = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
//        user = (TextInputLayout) findViewById(R.id.layoutUser);
//        pswrd = (TextInputLayout) findViewById(R.id.layoutpswrd);
        context = LoginScreen.this;
        sessionManager = new SessionManager(context);
        if (sessionManager.isLoggedIn()) {
            startActivity(new Intent(context, HomeNavigation.class));
            finish();
        }
        Button signup = (Button) findViewById(R.id.signup);
        Button login = (Button) findViewById(R.id.login);
        TextView forgot = (TextView) findViewById(R.id.forgot);
        login.setOnClickListener(this);
        signup.setOnClickListener(this);
        forgot.setOnClickListener(this);
    }

    private void getForgotPassword() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setMessage("Enter your registered MObile Number or Email ID.")

        builder.setCancelable(false).setTitle(" ");
        input1 = new EditText(LoginScreen.this);
        final TextInputLayout textInputLayout = new TextInputLayout(LoginScreen.this);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input1.setHint("Mobile Number or Email ID");

        lp1.setMargins(5, 20, 5, 5);
        input1.setSingleLine();
        input1.setLayoutParams(lp1);
        textInputLayout.addView(input1);
        builder.setView(textInputLayout);

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (!input1.getText().toString().matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    textInputLayout.setError("Please Enter Valid email Address");
                    Toast.makeText(context, "Please Enter Valid Email ID", Toast.LENGTH_LONG).show();
                } else {
                    getEmail();
                }

            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.forgot) {
            getForgotPassword();
        } else if (v.getId() == R.id.signup) {

            startActivity(new Intent(context, Signup.class));
            finish();
        } else if (v.getId() == R.id.login) {

            if (!name.getText().toString().matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                name.setError("Please enter your valid email id");
            } else if (password.getText().toString().equals("") || password.getText().toString().length() < 4) {
                password.setError("Password should be more than 4 digits");
            } else {
                getLogin();
            }

        }
    }

    private void getLogin() {
       refreshedToken = sessionManager.getDeviceId();
        if(refreshedToken.equals("")) {
            refreshedToken = FirebaseInstanceId.getInstance().getToken();
        }
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.LOGIN)
                .add("name", name.getText().toString())
                .add("password", password.getText().toString())
                .add("deviceId", refreshedToken)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.LOGIN);
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.LOGIN)) {
            if (result.optString("status").equals("success")) {
                try {

                    sessionManager.saveCredentials(result.optString("name"), result.optString("id"), result.optString("email"), result.optString("mobile"));
                    sessionManager.setProfileImagePath(result.optString("image"));
                    sessionManager.setTrip(result.optString("tid"));
                    sessionManager.setRating(result.optString("rating"));
                    sessionManager.setLastUpdate(result.optString("date"));
                    sessionManager.setPassword(result.optString("password"));
                    startActivity(new Intent(context, HomeNavigation.class));
                    Toast.makeText(context, result.optString("message"), Toast.LENGTH_LONG).show();
                    finish();
                    sessionManager.saveLoggedIn(true);
                    sessionManager.saveLocIn(true);
                    sessionManager.saveTripIn(true);
                } catch (Exception e) {

                }

            } else {
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setContentText(result.optString("message"))
                        .setConfirmText("Ok")
                        .show();
            }
        }
    }


}


