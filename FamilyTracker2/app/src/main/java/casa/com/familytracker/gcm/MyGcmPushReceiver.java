package casa.com.familytracker.gcm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import casa.com.familytracker.ChatActivity;
import casa.com.familytracker.SessionManager;
import casa.com.familytracker.Splash;


public class MyGcmPushReceiver extends FirebaseMessagingService {
    public static final String PREFS_NAME = "LoginPrefs";
    public static NotiLstner mListener;
    private static final String TAG = MyGcmPushReceiver.class.getSimpleName();

    private NotificationUtils notificationUtils;


    SharedPreferences sharedPreferences;
    private Intent resultIntent;
    private String title = "";
    private String message = "";
    private String uid = "";

    /**
     * Called when message is received.
     * <p>
     * //  * @param from   SenderID of the sender.
     * // * @param bundle Data bundle containing message data as key/value pairs.
     * For Set of keys use data.keySet().
     */

    @Override
    public void onMessageReceived(RemoteMessage map) {

        Map bundle = map.getData();

        try {
            title = bundle.get("title").toString();
            message = bundle.get("message").toString();
            String groupid = bundle.get("groupid").toString();
            uid = bundle.get("uid").toString();
            String time = bundle.get("time").toString();
            String imgUrl = bundle.get("tickerText").toString();
            SessionManager sessionManager = new SessionManager(getApplicationContext());
            int a = sessionManager.getNumbr(groupid);
            sessionManager.setNumbr(a + 1, groupid);
            if (!groupid.equals("")) {
                resultIntent = new Intent(getApplicationContext(), ChatActivity.class);
                resultIntent.putExtra("groupid", groupid);
            } else {
                resultIntent = new Intent(getApplicationContext(), Splash.class);

            }
//        resultIntent.putExtra("id", "3");
//        resultIntent.putExtra("name", "Gaurav");
            if (!sessionManager.getKeyUserId().equals(uid) && sessionManager.isOnnoti()) {
                if (TextUtils.isEmpty(imgUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, uid, resultIntent);
                } else {
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, groupid, resultIntent, time);
                }
            }
        } catch (Exception e) {
            showNotificationMessage(getApplicationContext(), title, message, uid, resultIntent);

        }
        if (null != mListener) {
            //Pass on the text to our listener.
            mListener.messageReceived(bundle);
        }
    }

    public static void bindListener(NotiLstner listener) {
        mListener = listener;
    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}