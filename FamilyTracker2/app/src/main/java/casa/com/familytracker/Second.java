package casa.com.familytracker;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.multidex.myapplication.R;


public class Second extends Fragment {
    View view;
    private static final String ARG_LAYOUT_RES_ID = "imageId";
    private int imageId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_first, container, false);
        TextView title = (TextView) view.findViewById(R.id.textTitle);
        TextView disc = (TextView) view.findViewById(R.id.discription);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.joing);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "Fonts/action.ttf");
        disc.setTypeface(type);
        title.setTypeface(type);
        disc.setText("Join new groups of your known one with auto generated code");
        title.setText("Join Group");
        return view;

    }
}