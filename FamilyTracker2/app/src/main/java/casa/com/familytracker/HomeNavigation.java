package casa.com.familytracker;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.android.multidex.myapplication.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.ldoublem.loadingviewlib.view.LVBattery;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.NiceSpinnerBaseAdapter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import casa.com.familytracker.Scrolling.BottomSheetBehaviorGoogleMapsLike;
import casa.com.familytracker.Scrolling.MergedAppBarLayoutBehavior;
import casa.com.familytracker.gcm.NotificationUtils;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

import static casa.com.familytracker.API.AlertDialogLogOut;
import static casa.com.familytracker.API.getMarkerBitmapFromView;
import static casa.com.familytracker.API.shareCode;


public class HomeNavigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, HttpProcessor.HttpResponser, View.OnClickListener {

    private int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private LatLng latLng;
    private GoogleMap mGoogleMap;
    private SupportMapFragment mFragment;
    private Marker currLocationMarker;
    private Marker currLocationMarkerCurrent;
    private Context context;
    private boolean isSetlite = false;
    //    private String API_KEY = "AIzaSyC1_FdTpyALgy5OKtWOhlO1VFcuFlYVPMs";
    private TextView create, join;
    private ArrayList<Container> profileData;
    private ListView listMember, tripList;
    private TextView customSpinner;
    private SessionManager sessionManager;
    private ArrayList<Container> arrayList;
    private CircleImageView groupImage, selectedGroup, circleBotm;
    private MarkerOptions markerOptions;
    private ArrayList<LatLng> latList;
    private String groupid;

    private AdView mAdView;
    private Button startTrip, stopTrip;
    private RelativeLayout relative, relativeMaster;
    private String groupImageUrl;

    private ImageView showList;
    private TextView toolbarText, groupText, btmTextTitle, addressText;
    private LocationManager locationManager;
    private Toolbar toolbar;
    TextView bottomSheetTextView;
    private ArrayList<Container> arrayTrip;
    private SpinnerAdapter arrayAdapter;
    private AlertDialog dialog;
    private ListView listViewG;

    private String groupName;

    LVBattery battery;
    TextView batteryTxt;
    TextView txtCount;
    ListView nice;
    private String count = "0";
    private InterstitialAd mInterstitialAd;
    private JSONArray jsonArray = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_navigation);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        startTrip = (Button) findViewById(R.id.startTrip);
        stopTrip = (Button) findViewById(R.id.stopTrip);
        relativeMaster = (RelativeLayout) findViewById(R.id.relativeMaster);
        relative = (RelativeLayout) findViewById(R.id.relative);
        batteryTxt = (TextView) findViewById(R.id.battery);
        toolbarText = (TextView) findViewById(R.id.title_text);
        bottomSheetTextView = (TextView) findViewById(R.id.bottom_sheet_title);
        addressText = (TextView) findViewById(R.id.text_dummy1);
        battery = (LVBattery) this.findViewById(R.id.bateryP);
        mAdView = (AdView) findViewById(R.id.adView);
        battery.setCellColor(Color.parseColor("#ffffff"));
        battery.setViewColor(Color.GREEN);
        battery.setShowNum(false);


        toolbarText.setOnClickListener(this);
        startTrip.setOnClickListener(this);
        stopTrip.setOnClickListener(this);
        context = HomeNavigation.this;
        showPermissionDialog();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        displayLocationSettingsRequest(context);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(" ");
        }
        tripList = (ListView) findViewById(R.id.listViewBtm);
        getBottomSheet();
        mFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mFragment.getMapAsync(this);
        sessionManager = new SessionManager(context);

        getNavigationDrawer();

        listMember = (ListView) findViewById(R.id.listImage);
        circleBotm = (CircleImageView) findViewById(R.id.circleBtm);
        getUserData();

        getGroup();

    }

    private void getNavigationDrawer() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);


        create = (TextView) header.findViewById(R.id.createGroup);
        txtCount = (TextView) header.findViewById(R.id.textCount);
        nice = (ListView) header.findViewById(R.id.nice);
        join = (TextView) header.findViewById(R.id.joinGroup);
        customSpinner = (TextView) header.findViewById(R.id.spinner);
        groupImage = (CircleImageView) header.findViewById(R.id.groupImage);
        showList = (ImageView) header.findViewById(R.id.showList);
        selectedGroup = (CircleImageView) header.findViewById(R.id.selectedGroup);
        groupText = (TextView) header.findViewById(R.id.groupText);

        customSpinner.setOnClickListener(this);
        showList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nice.getVisibility() == View.VISIBLE) {
                    nice.setVisibility(View.GONE);
                } else {
                    nice.setVisibility(View.VISIBLE);
                }
            }
        });
        groupImage.setOnClickListener(this);
        create.setOnClickListener(this);
        join.setOnClickListener(this);

    }

    private void getBottomSheet() {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorlayout);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);
        final BottomSheetBehaviorGoogleMapsLike behavior = BottomSheetBehaviorGoogleMapsLike.from(bottomSheet);
        AppBarLayout mergedAppBarLayout = (AppBarLayout) findViewById(R.id.merged_appbarlayout);
        MergedAppBarLayoutBehavior mergedAppBarLayoutBehavior = MergedAppBarLayoutBehavior.from(mergedAppBarLayout);
        mergedAppBarLayoutBehavior.setToolbarTitle("Recent Trips");
        mergedAppBarLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mergedAppBarLayoutBehavior.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
            }
        });


        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);

    }


    private void getUserData() {
        Picasso.with(context)
                .load(API.IMAGE_URL + sessionManager.getProfileImagePath()).placeholder(R.drawable.blnk).error(R.drawable.blnk)
                .into(groupImage);

        groupText.setText(sessionManager.getKeyUserName());
        Picasso.with(context)
                .load(API.IMAGE_URL + sessionManager.getProfileImagePath()).placeholder(R.drawable.blnk).error(R.drawable.blnk)
                .into(circleBotm);

        bottomSheetTextView.setText(sessionManager.getKeyUserName());
        addressText.setText(sessionManager.getKeyPhoneNumber());
        battery.setValue(Integer.parseInt(sessionManager.getLevel()));
        batteryTxt.setText(sessionManager.getLevel() + " %");

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(
                listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;

        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(
                        desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


    private void showPermissionDialog() {
        if (!AddLocationServices.checkPermission(this)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_ACCESS_COARSE_LOCATION);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_navigation, menu);

        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_change) {
            Intent intent = new Intent(context, ChangePassword.class);
            startActivity(intent);
        } else if (id == R.id.nav_loc) {
            new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                    .setContentText("Are you sure you want to share your current location ?")
                    .setConfirmText("Share now")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            checkIn();
                            sDialog.cancel();
                        }
                    }).setCancelText("Cancel")
                    .show();

        } else if (id == R.id.nav_battery) {
            Intent intent = new Intent(context, Premium.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (id == R.id.nav_editp) {
            Intent intent = new Intent(context, Setting.class);
            intent.putExtra("groupid", groupid);
            intent.putExtra("name", groupName);
            intent.putExtra("url", groupImageUrl);
            intent.putExtra("members", (Serializable) profileData);
            startActivityForResult(intent, 21);
        } else if (id == R.id.nav_faq) {
            startActivity(new Intent(context, Feedback.class));
        } else if (id == R.id.nav_premium) {
            startActivity(new Intent(context, PrimiumActivity.class));
        } else if (id == R.id.nav_share) {
            shareCode(context);
        } else if (id == R.id.nav_log) {
            AlertDialogLogOut(HomeNavigation.this);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void checkIn() {
        if (latLng != null) {
            String lati = String.valueOf(latLng.latitude);
            String lngi = String.valueOf(latLng.longitude);
            String url = "http://maps.googleapis.com/maps/api/staticmap?zoom=18&size=200x200&markers=size:large|color:red|" + lati + "," + lngi +
                    "&sensor=false";
            RequestBody requestBody = new FormEncodingBuilder()
                    .add(API.ACTION, API.CHECK_IN)
                    .add("uid", sessionManager.getKeyUserId())
                    .add("groupid", groupid)
                    .add("image", url)
                    .add("message", sessionManager.getKeyUserName() + " shared his current location")
                    .add("latitude", lati)
                    .add("longitude", lngi)
                    .build();

            HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
            httpProcessor.executeRequest(API.CHECK_IN);
            httpProcessor.setHttpResponserListener(this);
        } else {

        }
    }


    @Override
    public void onMapReady(GoogleMap gMap) {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {

            mGoogleMap = gMap;
            mGoogleMap.setMyLocationEnabled(true);
            buildGoogleApiClient();
            mGoogleApiClient.connect();
//            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
//            ActivityCompat.requestPermissions(this, new String[]{
//                            android.Manifest.permission.ACCESS_FINE_LOCATION,
//                            android.Manifest.permission.ACCESS_COARSE_LOCATION},
//                    TAG_CODE_PERMISSION_LOCATION);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSION_ACCESS_COARSE_LOCATION);
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(sessionManager.getKeyUserName());
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(sessionManager.getProfileImagePath(), context)));
            currLocationMarkerCurrent = mGoogleMap.addMarker(markerOptions);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng).zoom(12).build();

            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            if (!AddLocationServices.serviceRunning) {
                AlertDialog(mLastLocation);
            }
            getLocation(sessionManager.getKeyUserId());
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


    }

    private void AlertDialog(final Location location) {


        if (location != null) {

            double lats = location.getLatitude();
            double lngs = location.getLongitude();

            Intent intent1 = new Intent(context, AddLocationServices.class);
////                            intent1.putExtra("did", did);
            intent1.putExtra("latitude", String.valueOf(lats));
            intent1.putExtra("longitude", String.valueOf(lngs));
            startService(intent1);

//                    } else {
//                        Toast.makeText(context, "You can not start your journey at this time", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Unable to find your current location", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
//        Toast.makeText(this, "onConnectionSuspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
//        Toast.makeText(this, "onConnectionFailed", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SessionManager sessionManager1 = new SessionManager(context);
        sessionManager1.saveOn(true);
        sessionManager.setNumbr(0, groupid);
    }

    @Override
    public void onLocationChanged(Location location) {
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(sessionManager.getKeyUserName());
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(sessionManager.getProfileImagePath(), context)));

        if (currLocationMarkerCurrent == null) {
            currLocationMarkerCurrent = mGoogleMap.addMarker(markerOptions);
        } else {
            currLocationMarkerCurrent.setPosition(latLng);

        }
        String asdfg = sessionManager.getLatitude();
        if (asdfg.equals("0.0")) {
            sessionManager.setLocation(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
        }

//        addLocation(location);
//        Toast.makeText(this, "onConnectionSuspended", Toast.LENGTH_SHORT).show();
        mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                if (cameraPosition.zoom >= 17 && !isSetlite) {
                    mGoogleMap.setMapType(mGoogleMap.MAP_TYPE_SATELLITE);
                    isSetlite = true;
                } else if (cameraPosition.zoom < 17 && isSetlite) {
                    mGoogleMap.setMapType(mGoogleMap.MAP_TYPE_TERRAIN);
                    isSetlite = false;
                }
            }
        });

    }

    private void getGroup() {


        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.GET_GROUP)
                .add("uid", sessionManager.getKeyUserId())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.GET_GROUP);
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case 2:
                if (resultCode == RESULT_OK) {
                    getGroup();
                    getUserData();
                }
                break;
            case 21:
                if (resultCode == RESULT_OK) {
                    getGroup();
                    getUserData();
                }

                break;
            case 23:
                if (resultCode == RESULT_OK) {
                    groupid = data.getStringExtra("gid");
                    getMember(groupid);
                    getUserData();
                }

                break;
        }
    }


    @Override
    public void responseResult(JSONObject result, String TAG) {
        try {
            if (TAG.equals(API.GET_GROUP)) {
                if (result.optString("status").equals("success")) {

                    jsonArray = result.getJSONArray("result");
                    arrayList = new ArrayList<>();
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(a);
                        Container container = new Container();
                        container.setName(jsonObject.optString("name"));
                        container.setImage(jsonObject.optString("image"));
                        container.setMfrom(jsonObject.optString("groupid"));
                        arrayList.add(container);
                    }
                    if (arrayList.size() > 0) {
                        getMember(arrayList.get(0).getMfrom());
                    }

                    arrayAdapter = new SpinnerAdapter(context, arrayList);
                    nice.setAdapter(arrayAdapter);
                    nice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Container prjctItm = arrayList.get(i);

                            customSpinner.setText(prjctItm.getName());


                            groupid = prjctItm.getMfrom();
                            groupName = prjctItm.getName();
                            groupImageUrl = prjctItm.getImage();
                            Picasso.with(context)
                                    .load(API.IMAGE_URL + groupImageUrl).placeholder(R.drawable.blnk).error(R.drawable.blnk)
                                    .into(selectedGroup);

                            getMember(groupid);
                            nice.setVisibility(View.GONE);

                            if (customSpinner.getText().toString().equals("Group")) {
                                selectType();
                            }
                        }
                    });
                }

            } else if (TAG.equals(API.GET_MEMBER)) {
                if (result.optString("status").equals("success")) {

                    JSONArray jsonArray = result.getJSONArray("result");
                    profileData = new ArrayList<>();
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(a);
                        Container container = new Container();
                        container.setUid(jsonObject.optString("id"));
                        container.setMid(jsonObject.optString("mid"));
                        container.setName(jsonObject.optString("name"));
                        container.setImage(jsonObject.optString("image"));
                        container.setMobile(jsonObject.optString("mobile"));
                        container.setLatitude(jsonObject.optString("latitude"));
                        container.setLongitude(jsonObject.optString("longitude"));
                        container.setBattery(jsonObject.optString("battery"));
                        container.setRefId(jsonObject.optString("rid"));
                        profileData.add(container);
                    }
                    count = String.valueOf(sessionManager.getNumbr(groupid));
                    if (count.equals("0")) {
                        txtCount.setVisibility(View.GONE);
                    } else {
                        txtCount.setVisibility(View.VISIBLE);
                        txtCount.setText(count + " new");
                    }

                    HListAdapter adapter = new HListAdapter(context, profileData);

                    listMember.setAdapter(adapter);
                    listMember.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            setMarkerFormember(position);
                        }
                    });

                }

            } else if (TAG.equals(API.GET_TRIP)) {

                JSONArray jsonArray = result.getJSONArray("result");
                arrayTrip = new ArrayList<>();
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(a);
                    Container container = new Container();
//                    container.setMobile(jsonObject.optString("time"));
                    JSONArray jsonArray1 = jsonObject.getJSONArray("result1");
                    latList = new ArrayList<>();
                    for (int b = 0; b < jsonArray1.length(); b++) {
                        JSONObject jsonObject1 = jsonArray1.getJSONObject(b);
                        Container container1 = new Container();
                        container1.setLatitude(jsonObject1.optString("latitude"));
                        container1.setLongitude(jsonObject1.optString("longitude"));
                        if (b == 0) {
                            container.setMobile(jsonObject1.optString("time"));
                        }
                        container.setMsg(jsonObject1.optString("time"));
                        latList.add(new LatLng(Double.parseDouble(container1.getLatitude()), Double.parseDouble(container1.getLongitude())));

                    }

                    container.setLatLng(latList);
                    arrayTrip.add(container);
                }


                TripAdapter triAdapter = new TripAdapter(context, arrayTrip);
                tripList.setAdapter(triAdapter);
                tripList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(context, Path.class);
                        intent.putExtra("list", (Serializable) arrayTrip.get(position).getLatLng());
                        intent.putExtra("post", String.valueOf(position));
                        startActivity(intent);
                    }
                });
                setListViewHeightBasedOnChildren(tripList);
            } else if (TAG.equals(API.CHECK_IN)) {
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Location shared")
                        .setConfirmText("Ok")
                        .show();
            } else if (TAG.equals(API.ADD_TRIP)) {
                if (result.optString("status").equals("success")) {
                    sessionManager.setTrip(result.optString("id"));
                    sessionManager.setLastUpdate(result.optString("date"));
                    stopTrip.setVisibility(View.VISIBLE);
                    startTrip.setVisibility(View.GONE);
                } else {

                }
            }

        } catch (Exception e)

        {
            e.printStackTrace();

            selectType();
        }
    }

    private void selectType() {

        final CharSequence[] items = {"Create Group", "Join Group"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Create / Join Group");
        builder.setCancelable(false);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Create Group")) {


                    Intent intent = new Intent(context, AddCircle.class);
                    intent.putExtra("type", "Add");
                    startActivityForResult(intent, 21);

                } else if (items[item].equals("Join Group")) {

                    Intent intent = new Intent(context, AddCircle.class);
                    intent.putExtra("type", "join");
                    startActivityForResult(intent, 21);


                }
            }
        });
        builder.show();
    }

    private void setMarkerFormember(int position) {
        if (profileData.get(position).getUid().equals(sessionManager.getKeyUserId())) {
            relative.setVisibility(View.GONE);
            relativeMaster.setVisibility(View.VISIBLE);
            if (sessionManager.isTripShare()) {
                startTrip.setVisibility(View.GONE);
                stopTrip.setVisibility(View.VISIBLE);
            } else {
                startTrip.setVisibility(View.VISIBLE);
                stopTrip.setVisibility(View.GONE);
            }
        } else {
            relative.setVisibility(View.VISIBLE);
            relativeMaster.setVisibility(View.GONE);
            startTrip.setVisibility(View.GONE);
            stopTrip.setVisibility(View.GONE);
        }
        if (profileData.get(position).getLatitude().equals("")) {
            Toast.makeText(context, "Location not available", Toast.LENGTH_LONG).show();
        } else {
            double latitude = Double.parseDouble(profileData.get(position).getLatitude());
            double longitude = Double.parseDouble(profileData.get(position).getLongitude());
            LatLng latLng = new LatLng(latitude, longitude);

            Picasso.with(context)
                    .load(API.IMAGE_URL + profileData.get(position).getImage()).placeholder(R.drawable.blnk)
                    .into(circleBotm);

            bottomSheetTextView.setText(profileData.get(position).getName());

            addressText.setText(profileData.get(position).getMobile());
            battery.setValue(Integer.parseInt(profileData.get(position).getBattery()));
            batteryTxt.setText(profileData.get(position).getBattery() + " %");

            sessionManager.saveBattery(profileData.get(position).getBattery());

            markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(profileData.get(position).getName());
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(profileData.get(position).getImage(), context)));

            if (currLocationMarker == null) {
                currLocationMarker = mGoogleMap.addMarker(markerOptions);
            } else {
                currLocationMarker.remove();
                currLocationMarker = mGoogleMap.addMarker(markerOptions);
            }
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng).zoom(12).build();

            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            getLocation(profileData.get(position).getUid());
        }
    }

    private void getLocation(String id) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.GET_TRIP)
                .add("uid", id)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.GET_TRIP);
        httpProcessor.setHttpResponserListener(this);
    }

    private void getMember(String groupid) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.GET_MEMBER)
                .add("groupid", groupid)
                .add("userid", sessionManager.getKeyUserId())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.GET_MEMBER);
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.createGroup) {
            Intent intent = new Intent(context, AddCircle.class);
            intent.putExtra("type", "Add");
            startActivityForResult(intent, 21);

        } else if (v.getId() == R.id.joinGroup) {
            Intent intent = new Intent(context, AddCircle.class);
            intent.putExtra("type", "join");
            startActivityForResult(intent, 21);
//            Toast.makeText(context, "Create Group", Toast.LENGTH_LONG).show();
//        } else if (v.getId() == R.id.toolText) {

//            Intent intent = new Intent(context, CheckIn.class);
//            intent.putExtra("lat", String.valueOf(latLng.latitude));
//            intent.putExtra("lng", String.valueOf(latLng.longitude));
//            intent.putExtra("from", "home");
//            intent.putExtra("groupid", groupid);
//            startActivity(intent);
        } else if (v.getId() == R.id.groupImage) {

            Intent st = new Intent(context, CurrentLocation.class);
            st.putExtra("url", API.IMAGE_URL + sessionManager.getProfileImagePath());
            st.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(st);

        } else if (v.getId() == R.id.spinner) {

            Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra("name", customSpinner.getText().toString());
            intent.putExtra("url", groupImageUrl);
            intent.putExtra("groupid", groupid);
            startActivityForResult(intent, 23);
        } else if (v.getId() == R.id.startTrip) {
            if (latLng != null) {
                String lat1 = String.valueOf(latLng.latitude);
                String lng1 = String.valueOf(latLng.longitude);
                NotificationUtils.showDefaultNotification(context);
                sessionManager.saveTripIn(true);
                addTrip(lat1, lng1, sessionManager.getKeyUserId());

            }
        } else if (v.getId() == R.id.stopTrip) {
            NotificationUtils.clearNotifications(context);
            sessionManager.saveTripIn(false);
            startTrip.setVisibility(View.VISIBLE);
            stopTrip.setVisibility(View.GONE);
        }
    }


//////////////////////show add/////////////////////////////

    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
//            Logger.logStackTrace(TAG,e);
        }
        return "";
    }

    private void getAdd() {
        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        String deviceId = md5(android_id).toUpperCase();
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice(deviceId)
                .build();

        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {

                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });


    }


    @Override
    protected void onPause() {

        sessionManager.saveOn(false);
        super.onPause();
    }

    private void showAdd() {

        mInterstitialAd = new InterstitialAd(this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                super.onAdLoaded();
                mInterstitialAd.show();
                showInterstitial();
            }
        });
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    private void addTrip(String lat1, String lng1, String uid) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.ADD_TRIP)
                .add("latitude", lat1)
                .add("longitude", lng1)
                .add("battery", sessionManager.getLevel())
                .add("uid", uid)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, false, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.ADD_TRIP);
        httpProcessor.setHttpResponserListener(this);
    }

    private void displayLocationSettingsRequest(final Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
//                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(HomeNavigation.this, 102);
                        } catch (IntentSender.SendIntentException e) {
//                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

}


