package casa.com.familytracker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CheckIn extends AppCompatActivity implements HttpProcessor.HttpResponser, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private Context context;
    String lati, lngi;
    TextView textView;
    TextView textField;
    String completeAddress;
    GoogleMap mGoogleMap;
    SupportMapFragment mFragment;
    GoogleApiClient mGoogleApiClient;
    SessionManager sessionManager;
    private String groupid;
    private double latitude1;
    private double longitude1;
    private LatLng address;
    Circle circle;
    String from;
    Button setAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        textView = (TextView) findViewById(R.id.textView);
        setAlert = (Button) findViewById(R.id.setAlert);
        textField = (TextView) findViewById(R.id.textField);
        sessionManager = new SessionManager(this);

        context = CheckIn.this;
        mFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mFragment.getMapAsync(this);
        groupid = getIntent().getStringExtra("groupid");
        lati = getIntent().getStringExtra("lat");
        lngi = getIntent().getStringExtra("lng");
        from = getIntent().getStringExtra("from");
        if (!lati.equals("")) {
            latitude1 = Double.parseDouble(lati);
            longitude1 = Double.parseDouble(lngi);
        }
        address = new LatLng(latitude1, longitude1);
//        CameraPosition cameraPosition = new CameraPosition.Builder().target(address).zoom(18).bearing(0).build();


        if (from.equals("home")) {
            setAlert.setVisibility(View.GONE);
            textField.setVisibility(View.VISIBLE);
        } else  {
            textField.setVisibility(View.GONE);
            setAlert.setVisibility(View.VISIBLE);

        }
    }

    public void getCompleteAddressString(String lat, String lng) {
        RequestBody requestBody = new FormEncodingBuilder()
                .build();


        HttpProcessor httpProcessor = new HttpProcessor(context, false, "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("get-address");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        try {
            if (TAG.equals("get-address")) {
                JSONArray jsonADDArray = (JSONArray) result.get("results");
                for (int i = 0, count = jsonADDArray.length(); i < count; i++) {
                    JSONObject jsonObject = jsonADDArray.getJSONObject(0);
                    completeAddress = jsonObject.optString("formatted_address");

                }

                textView.setText(completeAddress);
            } else if (TAG.equals(API.CHECK_IN)) {
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Checked In")
                        .setConfirmText("Ok")
                        .show(); }
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
//        mGoogleMap.setMyLocationEnabled(true);
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(address).bearing(0).zoom(18).build();

        mGoogleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        mGoogleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                LatLng latLng = mGoogleMap.getCameraPosition().target;
                if (circle == null) {


                    circle = mGoogleMap.addCircle(new CircleOptions()
                            .center(latLng)
                            .radius(50)

                            .fillColor(Color.parseColor("#3408c7f7")));

                } else {
                    circle.setCenter(latLng);
                }
                lati = String.valueOf(latLng.latitude);
                lngi = String.valueOf(latLng.longitude);

//                getCompleteAddressString(lati, lngi);
                mGoogleMap.getFocusedBuilding();
            }
        });

        textField.setOnClickListener(this);
        setAlert.setOnClickListener(this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

//    private void checkIn() {
//        RequestBody requestBody = new FormEncodingBuilder()
//                .add(API.ACTION, API.CHECK_IN)
//                .add("uid", sessionManager.getKeyUserId())
//                .add("groupid", groupid)
//                .add("latitude", lati)
//                .add("longitude", lngi)
//                .build();
//
//        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
//        httpProcessor.executeRequest(API.CHECK_IN);
//        httpProcessor.setHttpResponserListener(this);
//    }
@Override
protected void onPause() {
    sessionManager.saveOn(false);
    super.onPause();
}

    @Override
    protected void onResume() {
        super.onResume();
        sessionManager.saveOn(true);
    }

    private void checkIn() {
        String url = "http://maps.googleapis.com/maps/api/staticmap?zoom=18&size=200x200&markers=size:large|color:red|" + lati + "," + lngi +
                "&sensor=false";
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.CHECK_IN)
                .add("uid", sessionManager.getKeyUserId())
                .add("groupid", groupid)
                .add("image", url)
                .add("message", "")
                .add("latitude", lati)
                .add("longitude", lngi)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.CHECK_IN);
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.textField) {
            checkIn();
        } else if (v.getId() == R.id.setAlert) {
            sessionManager.setAlert(lati, lngi);
            sessionManager.saveAlertAddress(completeAddress);

            if (completeAddress.equals("")) {
                Intent intent = new Intent();
                intent.putExtra("add", sessionManager.getAlertLatitude() + "," + sessionManager.getAlertLongitude());
                setResult(2, intent);
                finish();
            } else {
                Intent intent = new Intent();
                intent.putExtra("add", completeAddress);
                setResult(2, intent);
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
