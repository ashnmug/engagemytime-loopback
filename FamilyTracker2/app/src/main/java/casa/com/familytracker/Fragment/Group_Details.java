package casa.com.familytracker.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import casa.com.familytracker.API;
import casa.com.familytracker.Container;
import casa.com.familytracker.HttpProcessor;
import casa.com.familytracker.MembersAdapter;
import casa.com.familytracker.RoundedImageView;
import casa.com.familytracker.SessionManager;
import casa.com.familytracker.Utility;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class Group_Details extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser {

    CircleImageView profilePic;
    ImageView edit;
    EditText name;
    View view;

    private String img_str = "";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    //    ImageView imageChat, select;
    private Button submit, delete, exit, deleteGrp;
    private Bitmap bitmap;
    SessionManager sessionManager;
    ListView membersList;
    ArrayList<Container> members;
    String groupid;
    private String groupName;
    private String groupImageUrl;
    private SweetAlertDialog sweetAlertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_group, container, false);
//
        getInit();

        return view;
    }

    private void getInit() {

        profilePic = (CircleImageView) view.findViewById(R.id.roundImage);
        membersList = (ListView) view.findViewById(R.id.listViewGrp);
        edit = (ImageView) view.findViewById(R.id.iv_edit_profile);

        submit = (Button) view.findViewById(R.id.submit);
        deleteGrp = (Button) view.findViewById(R.id.dGrp);
        delete = (Button) view.findViewById(R.id.delete);
        exit = (Button) view.findViewById(R.id.exit);
        name = (EditText) view.findViewById(R.id.grpName);
        sessionManager = new SessionManager(getActivity());
        exit.setOnClickListener(this);
        profilePic.requestFocus();
        delete.setOnClickListener(this);
        submit.setOnClickListener(this);
        edit.setOnClickListener(this);
        deleteGrp.setOnClickListener(this);
        groupid = getActivity().getIntent().getStringExtra("groupid");
        groupName = getActivity().getIntent().getStringExtra("name");
        groupImageUrl = getActivity().getIntent().getStringExtra("url");
        members = new ArrayList<>();
        members = (ArrayList<Container>) getActivity().getIntent().getSerializableExtra("members");

        MembersAdapter adapter = new MembersAdapter(getActivity(), members);
        membersList.setAdapter(adapter);
        name.setText(groupName);
        Picasso.with(getActivity())
                .load(API.IMAGE_URL + groupImageUrl).placeholder(R.drawable.blnk).error(R.drawable.blnk)
                .into(profilePic);

        membersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String memberId = members.get(position).getUid();

                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
//                        .setTitleText("Are you sure?")
                        .setContentText("Wanna make him admin for this group?")
                        .setConfirmText("Yes")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                setNewAdmin(memberId);
                            }
                        })
                        .setCancelText("No").setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                        .show();

            }
        });
    }

    private void setNewAdmin(String memberId) {

        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.UPDATE__ADMIN)
                .add("groupid", groupid)
                .add("uid", memberId)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getActivity(), true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.UPDATE__ADMIN);
        httpProcessor.setHttpResponserListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {
            if (!name.getText().toString().equals("")) {
                getNameUpdate(name.getText().toString());
            } else {
                Toast.makeText(getActivity(), "Group name should not be empty", Toast.LENGTH_SHORT).show();
            }

        } else if (v.getId() == R.id.dGrp) {

            getDeleteGroup();

        } else if (v.getId() == R.id.delete) {

            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Are you sure?")
                    .setContentText("Won't be able to access this group!")
                    .setConfirmText("Yes,delete it!")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                    sDialog
                                    .setTitleText("Deleted!")
                                    .setContentText("Your group has been deleted!")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            Intent returnIntent = new Intent();
                                            getActivity().setResult(Activity.RESULT_OK, returnIntent);
                                            getActivity().finish();
                                        }
                                    })
                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        }
                    })
                    .show();

        } else if (v.getId() == R.id.exit) {
            getExitGroup();
        } else if (v.getId() == R.id.iv_edit_profile) {
            selectImage();
        }
    }

    private void getDeleteGroup() {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Won't be able to access this group!")
                .setConfirmText("Yes,delete it!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sweetAlertDialog = sDialog;
                        deleteGroup();

                    }
                })
                .show();
    }

    private void getExitGroup() {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Are you sure you want to exit from this group ?")
                .setConfirmText("Yes,delete it!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        Intent returnIntent = new Intent();
                        getActivity().setResult(Activity.RESULT_OK, returnIntent);
                        getActivity().finish();
                    }
                })
                .show();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Remove Picture"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Remove Picture")) {
                    getImageUpdate("");
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Gallery"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        try {     File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");


            ExifInterface ei = new ExifInterface(destination.toString());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);


            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotate(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotate(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotate(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:

                default:
                    break;
            }
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            FileOutputStream fo;

            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        img_str = getImageString(bitmap);
        profilePic.setImageBitmap(bitmap);

        getImageUpdate(img_str);
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }
    private void getImageUpdate(String string) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.UPDATE__GROUP_IMAGE)
                .add("groupid", groupid)
                .add("image", string)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getActivity(), true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.UPDATE__GROUP_IMAGE);
        httpProcessor.setHttpResponserListener(this);
    }

    private void deleteGroup() {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.DELETE_GROUP)
                .add("groupid", groupid)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getActivity(), true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.DELETE_GROUP);
        httpProcessor.setHttpResponserListener(this);
    }

    private void getNameUpdate(String string) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.UPDATE__GROUP_NAME)
                .add("groupid", groupid)
                .add("name", string)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getActivity(), true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.UPDATE__GROUP_NAME);
        httpProcessor.setHttpResponserListener(this);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        bitmap = null;
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                profilePic.setImageBitmap(bitmap);
                img_str = getImageString(bitmap);

                getImageUpdate(img_str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private String getImageString(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, Base64.DEFAULT);
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.UPDATE__GROUP_IMAGE)) {
            if (result.optString("status").equals("success")) {

                profilePic.setImageBitmap(bitmap);
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Image updated")
                        .setConfirmText("Ok")
                        .show();
            }
        } else if (TAG.equals(API.UPDATE__GROUP_NAME)) {
            if (result.optString("status").equals("success")) {


                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Group name updated")
                        .setConfirmText("Ok")
                        .show();
            }
        }  else if (TAG.equals(API.UPDATE__ADMIN)) {
            if (result.optString("status").equals("success")) {


                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Admin updated")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent returnIntent = new Intent();
                                getActivity().setResult(Activity.RESULT_OK, returnIntent);
                                getActivity().finish();
                            }
                        })
                        .show();
            }
        } else if (TAG.equals(API.DELETE_GROUP)) {
            if (result.optString("status").equals("success")) {

                sweetAlertDialog
                        .setTitleText("Deleted!")
                        .setContentText("Your group has been deleted!")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent returnIntent = new Intent();
                                getActivity().setResult(Activity.RESULT_OK, returnIntent);
                                getActivity().finish();
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            }
        }
    }

}
