package casa.com.familytracker.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dd.morphingbutton.MorphingButton;
import com.example.android.multidex.myapplication.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import casa.com.familytracker.API;
import casa.com.familytracker.HttpProcessor;
import casa.com.familytracker.RoundedImageView;
import casa.com.familytracker.SessionManager;
import casa.com.familytracker.Utility;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class Profile extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser {

    CircleImageView profilePic;
    ImageView edit;
    EditText name, mobile, email;
    View view;

    private String img_str = "";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    //    ImageView imageChat, select;
    private Button submit;
    private Bitmap bitmap;
    SessionManager sessionManager;
    private MorphingButton nameMorph, emailMorph, mobileMorph;
    MorphingButton.Params circle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        profilePic = (CircleImageView) view.findViewById(R.id.roundImage);
        edit = (ImageView) view.findViewById(R.id.imgProfile);
        submit = (Button) view.findViewById(R.id.submit);
        name = (EditText) view.findViewById(R.id.name);
        mobile = (EditText) view.findViewById(R.id.mobile);
        email = (EditText) view.findViewById(R.id.email);


        sessionManager = new SessionManager(getActivity());


        name.setText(sessionManager.getKeyUserName());
        mobile.setText(sessionManager.getKeyPhoneNumber());
        email.setText(sessionManager.getKeyEmailId());


        Picasso.with(getActivity())
                .load(API.IMAGE_URL + sessionManager.getProfileImagePath()).placeholder(R.drawable.blnk)
                .into(profilePic);


        edit.setOnClickListener(this);
        submit.setOnClickListener(this);
        edit.bringToFront();

// inside on click event

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgProfile) {
            selectImage();
        } else if (v.getId() == R.id.submit) {
            if (name.getText().toString().equals("") || mobile.getText().toString().equals("") || email.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "Field should not be empty", Toast.LENGTH_SHORT).show();
            } else {
                updateProfile();
            }
        }
    }

    private void updateProfile() {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.UPDATE_PROFILE)
                .add("uid", sessionManager.getKeyUserId())
                .add("name", name.getText().toString())
                .add("mobile", mobile.getText().toString())
                .add("email", email.getText().toString())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getActivity(), true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.UPDATE_PROFILE);
        httpProcessor.setHttpResponserListener(this);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Remove Picture"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Remove Picture")) {
                    getImageUpdate("");
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Gallery"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        try {     File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");


            ExifInterface ei = new ExifInterface(destination.toString());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);


            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotate(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotate(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotate(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:

                default:
                    break;
            }
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            FileOutputStream fo;

            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        img_str = getImageString(bitmap);
        profilePic.setImageBitmap(bitmap);

        getImageUpdate(img_str);
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }
    private void getImageUpdate(String string) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.UPDATE_IMAGE)
                .add("uid", sessionManager.getKeyUserId())
                .add("image", string)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getActivity(), true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.UPDATE_IMAGE);
        httpProcessor.setHttpResponserListener(this);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        bitmap = null;
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                profilePic.setImageBitmap(bitmap);
                img_str = getImageString(bitmap);

                getImageUpdate(img_str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private String getImageString(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, Base64.DEFAULT);
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.UPDATE_IMAGE)) {
            if (result.optString("status").equals("success")) {
                 profilePic.setImageBitmap(bitmap);
                sessionManager.setProfileImagePath(result.optString("image"));
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Image updated")
                        .setConfirmText("Ok")
                        .show();
            }
        } else if (TAG.equals(API.UPDATE_PROFILE)) {
            if (result.optString("status").equals("success")) {
                sessionManager.saveCredentials(result.optString("name"), sessionManager.getKeyUserId(), result.optString("email"), result.optString("mobile"));
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Profile updated")
                        .setConfirmText("Ok")
                        .show();
            }
        }
    }


}
