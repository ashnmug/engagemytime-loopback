package casa.com.familytracker;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.model.Polyline;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.R.attr.bitmap;

public class AddCircle extends AppCompatActivity implements View.OnClickListener, HttpProcessor.HttpResponser {
    EditText circleName;
    Button addCircle;
    ImageView share, icon;
    CircleImageView groupImg;
    LinearLayout linearLayout;
    LinearLayout createLinear;
    TextView code;
    TextView textType;
    String type;
    private int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;

    private AdView mAdView;
    private int SELECT_PHOTO = 101;
    private Context context;
    private String uid;
    private SessionManager sessionManager;
    private String img_str = "";


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_circle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mAdView = (AdView) findViewById(R.id.adView);

        getInit();
        getAdd();
    }

    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
//            Logger.logStackTrace(TAG,e);
        }
        return "";
    }

    private void getAdd() {
        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        String deviceId = md5(android_id).toUpperCase();
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice(deviceId)
                .build();

        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {

                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
//                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
//                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });


    }

    @Override
    protected void onPause() {
        sessionManager.saveOn(false);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sessionManager.saveOn(true);
    }

    private void getInit() {
        context = AddCircle.this;
        sessionManager = new SessionManager(context);

        uid = sessionManager.getKeyUserId();
        circleName = (EditText) findViewById(R.id.circleName);
        groupImg = (CircleImageView) findViewById(R.id.circleImage);
        code = (TextView) findViewById(R.id.code);
        textType = (TextView) findViewById(R.id.textType);
//        textInputLayout = (TextInputLayout) findViewById(R.id.inputLayout);
        linearLayout = (LinearLayout) findViewById(R.id.success);
        createLinear = (LinearLayout) findViewById(R.id.create);
        addCircle = (Button) findViewById(R.id.addCircle);
        share = (ImageView) findViewById(R.id.share);
        icon = (ImageView) findViewById(R.id.icon);
        addCircle.setOnClickListener(this);
        share.setOnClickListener(this);
        type = getIntent().getStringExtra("type");
        groupImg.setOnClickListener(this);
        if (type.equals("Add")) {
            textType.setText("Create Group");
            groupImg.setVisibility(View.VISIBLE);
            circleName.setInputType(InputType.TYPE_CLASS_TEXT);
            circleName.setHint("Enter new group name");
            icon.setImageResource(R.drawable.join);
        } else {

            textType.setText("Join Group");

            circleName.setHint("Enter your group code");

            groupImg.setVisibility(View.GONE);
            circleName.setInputType(InputType.TYPE_CLASS_NUMBER);
            icon.setImageResource(R.drawable.focusgroupicon);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.addCircle) {
            if (!circleName.getText().toString().equals("")) {
                if (type.equals("Add")) {
                    addCircle();
                } else {
                    joinCircle();
                }
            } else {
                Toast.makeText(context, "Please enter group name or group code", Toast.LENGTH_SHORT).show();

            }
        } else if (v.getId() == R.id.share) {
            shareCode();
        } else if (v.getId() == R.id.circleImage) {
            getImage();
        }
    }

    private void joinCircle() {
        String groupName = circleName.getText().toString();
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.JOIN_CIRCLE)
                .add("uid", uid)
                .add("groupid", groupName)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.JOIN_CIRCLE);
        httpProcessor.setHttpResponserListener(this);
    }

    private void getImage() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(AddCircle.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(AddCircle.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(AddCircle.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                ActivityCompat.requestPermissions(AddCircle.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        } else {

            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case 101:
                if (resultCode == RESULT_OK) {
                    try {
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        groupImg.setImageBitmap(selectedImage);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 90, stream);
                        byte[] image = stream.toByteArray();
                        img_str = Base64.encodeToString(image, 0);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
        }
    }

    private void shareCode() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Family Locator : \n \n Invitation for new group \n Our group code is : " + code.getText().toString() + "\n\n Download the app here.\n https://goo.gl/xv5t8h");
        try {
            this.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {

        }
    }

    private void setCode(String generatedCode) {
        if (type.equals("Add")) {
            linearLayout.setVisibility(View.VISIBLE);
            share.setVisibility(View.VISIBLE);
            code.setText(generatedCode);
            createLinear.setVisibility(View.GONE);
        } else if (type.equals("join")) {
            createLinear.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addCircle() {

        String groupName = circleName.getText().toString();
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.ADD_GROUP)
                .add("uid", uid)
                .add("name", groupName)
                .add("img", img_str)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.ADD_GROUP);
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.ADD_GROUP)) {
            if (result.optString("status").equals("success")) {
                setCode(result.optString("groupid"));
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setContentText(result.optString("message"))
                        .setConfirmText("Ok")
                        .show();
            } else {
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setContentText(result.optString("message"))
                        .setConfirmText("Ok")
                        .show();

            }
        } else if (TAG.equals(API.JOIN_CIRCLE)) {
            if (result.optString("status").equals("success")) {
                setCode(result.optString("groupid"));
                code.setText(result.optString("message"));
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setContentText(result.optString("message"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }
                        })
                        .show();

            } else {
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setContentText(result.optString("message"))
                        .setConfirmText("Ok")
                        .show();
            }
        }
    }
}
