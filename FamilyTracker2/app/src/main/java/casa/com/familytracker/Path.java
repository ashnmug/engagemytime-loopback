package casa.com.familytracker;

import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.android.multidex.myapplication.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Path extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //   Container lis = new Container();
        ArrayList<LatLng> latLngs = new ArrayList<>();
        latLngs = getIntent().getParcelableArrayListExtra("list");

        mMap.addMarker(new MarkerOptions().position(latLngs.get(0)).title("Starting point"));
        mMap.addMarker(new MarkerOptions().position(latLngs.get(latLngs.size() - 1)).title("Ending point"));
        CameraPosition cameraPosition = new CameraPosition.Builder()

                .target(latLngs.get(latLngs.size() - 1)).zoom(16).build();

        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));


        int second = latLngs.size() / 5;
        int third = 2 * second;
        int four = 3 * second;
        int fifth = 4 * second;

        String distance = getDistance(latLngs, second, third, four, fifth);
        String latlng = String.valueOf(latLngs.get(0).latitude) + "," + String.valueOf(latLngs.get(0).longitude);
        String latlng1 = String.valueOf(latLngs.get(second).latitude) + "," + String.valueOf(latLngs.get(second).longitude);
        String latlng2 = String.valueOf(latLngs.get(third).latitude) + "," + String.valueOf(latLngs.get(third).longitude);
        String latlng3 = String.valueOf(latLngs.get(four).latitude) + "," + String.valueOf(latLngs.get(four).longitude);
        String latlng4 = String.valueOf(latLngs.get(fifth).latitude) + "," + String.valueOf(latLngs.get(fifth).longitude);
        String finalIsed = latlng + "|" + latlng1 + "|" + latlng2 + "|" + latlng3 + "|" + latlng4;

        mMap.addPolyline(new PolylineOptions()
                .add(latLngs.get(0),latLngs.get(second),latLngs.get(third),latLngs.get(four),latLngs.get(fifth),latLngs.get(latLngs.size()-1))
                .width(10).color(Color.BLUE).geodesic(true));

        mMap.addPolyline(new PolylineOptions()
                .add(latLngs.get(0),latLngs.get(second),latLngs.get(third),latLngs.get(four),latLngs.get(fifth),latLngs.get(latLngs.size()-1))
                .width(12).color(Color.parseColor("#7b061d91")).geodesic(true));

    }


    private String getDistance(ArrayList<LatLng> latLngs, int second, int third, int four, int fifth) {


        Location loc1 = new Location(LocationManager.GPS_PROVIDER);
        Location loc2 = new Location(LocationManager.GPS_PROVIDER);
        Location loc3 = new Location(LocationManager.GPS_PROVIDER);
        Location loc4 = new Location(LocationManager.GPS_PROVIDER);
        Location loc5 = new Location(LocationManager.GPS_PROVIDER);
        loc1.setLatitude(latLngs.get(0).latitude);
        loc1.setLongitude(latLngs.get(0).longitude);
        loc2.setLatitude(latLngs.get(second).latitude);
        loc2.setLongitude(latLngs.get(second).longitude);
        loc3.setLatitude(latLngs.get(third).latitude);
        loc3.setLongitude(latLngs.get(third).longitude);
        loc4.setLatitude(latLngs.get(four).latitude);
        loc4.setLongitude(latLngs.get(four).longitude);
        loc5.setLatitude(latLngs.get(fifth).latitude);
        loc5.setLongitude(latLngs.get(fifth).longitude);

        float distance = loc5.distanceTo(loc4);
        distance = distance + loc4.distanceTo(loc3);
        distance = distance + loc3.distanceTo(loc2);
        distance = distance + loc2.distanceTo(loc1);
        distance = distance / 1000;
        String distString = String.valueOf(new DecimalFormat("##.#").format(distance));

        return distString;
    }
}
