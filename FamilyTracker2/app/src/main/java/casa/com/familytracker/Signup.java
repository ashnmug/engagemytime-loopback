package casa.com.familytracker;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class Signup extends AppCompatActivity implements View.OnClickListener, HttpProcessor.HttpResponser {

    private int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;
    private int SELECT_PHOTO = 101;
    private CircleImageView roundImage;
    private ImageView editImage;
    private Button login;
    private Button signup;
    private RelativeLayout relativeLayout;
    private SessionManager sessionManager;
    private EditText name, mobile, password, confirm, email;
    private TextInputLayout nameLayout, mobileLayout, passwordLayout, confirmLayout, emailLayout;
    private Context context;
    Bitmap bitmap;
    private String img_str = "";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signup = (Button) findViewById(R.id.signup);
        context = Signup.this;
        sessionManager = new SessionManager(context);
        getInit();
    }

    private void getInit() {
        login = (Button) findViewById(R.id.login);
        editImage = (ImageView) findViewById(R.id.iv_edit_profile);
        roundImage = (CircleImageView) findViewById(R.id.roundImage);
        relativeLayout = (RelativeLayout) findViewById(R.id.lay_profile_container);

        name = (EditText) findViewById(R.id.username);
        mobile = (EditText) findViewById(R.id.mobile);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirm = (EditText) findViewById(R.id.cpassword);
        login.setOnClickListener(this);
        editImage.setOnClickListener(this);
        signup.setOnClickListener(this);
    }


    @Override
    protected void onPause() {
        sessionManager.saveOn(false);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sessionManager.saveOn(true);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signup) {
//            startActivity(new Intent(getBaseContext(), LoginScreen.class));
//            finish();
            if (name.getText().toString().equals("")) {
                name.setError("Please enter name");
            } else if (mobile.getText().toString().length() != 10) {
                mobile.setError("Please enter your valid mobile number");
            } else if (!email.getText().toString().matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                email.setError("Please enter your valid email id");
            } else if (password.getText().toString().equals("") || password.getText().toString().length() < 4) {
                password.setError("Password should be more than 4 digits");
            } else {
                if (password.getText().toString().equals(confirm.getText().toString())) {
                    getSignUp();
                } else {
                    confirm.setError("Password and confirm password must be same");

                }
            }

        } else if (v.getId() == R.id.iv_edit_profile) {
            selectImage();
        } else if (v.getId() == R.id.login) {
            startActivity(new Intent(getBaseContext(), LoginScreen.class));
            finish();
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(context);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        bitmap = null;
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                roundImage.setImageBitmap(bitmap);
                img_str = getImageString(bitmap);

//                getImageUpdate(img_str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        img_str = getImageString(bitmap);
        roundImage.setImageBitmap(bitmap);


    }

    private void getSignUp() {

        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.REGISTRATION)
                .add("name", name.getText().toString())
                .add("mobile", mobile.getText().toString())
                .add("email", email.getText().toString())
                .add("password", password.getText().toString())
                .add("img", img_str)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.REGISTRATION);
        httpProcessor.setHttpResponserListener(this);

    }

    private String getImageString(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, Base64.DEFAULT);
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.REGISTRATION)) {
            if (result.optString("status").equals("success")) {
                startActivity(new Intent(getBaseContext(), LoginScreen.class));
                Toast.makeText(context, result.optString("message"), Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(context, result.optString("message"), Toast.LENGTH_LONG).show();
            }
        }
    }
}
