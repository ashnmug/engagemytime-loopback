package casa.com.familytracker;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.multidex.myapplication.R;


public class Third extends Fragment {
    View view;
    private static final String ARG_LAYOUT_RES_ID = "imageId";
    private int imageId;

    public static Third newInstance(int imageId) {
        Third sampleSlide = new Third();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, imageId);
        sampleSlide.setArguments(args);

        return sampleSlide;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID)) {
            imageId = getArguments().getInt(ARG_LAYOUT_RES_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_first, container, false);

        TextView title = (TextView) view.findViewById(R.id.textTitle);
        TextView disc = (TextView) view.findViewById(R.id.discription);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.dumy);
//        title.setText("Top Posts");
//        disc.setText("Find out your top posts till date...");
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "Fonts/action.ttf");
        disc.setTypeface(type);
        title.setTypeface(type);
        disc.setText("Get the current location of your group members on map and chat with them");
        title.setText("Current Location");
        return view;
    }
}