package casa.com.familytracker;

import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.IBinder;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.WindowManager;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import casa.com.familytracker.gcm.ProximityIntentReceiver;

/**
 * Created by User on 30-08-2016.
 */

public class AddLocationServices extends Service implements LocationListener, HttpProcessor.HttpResponser {
    private static final long POINT_RADIUS = 50; // in Meters
    private static final long PROX_ALERT_EXPIRATION = -1; // It will never expire
    private static final String PROX_ALERT_INTENT = "casa.com.familytracker";
    public static Boolean serviceRunning = false;
    Context context;
    LocationManager locationManager;
    String provider;
    String lat1, lng1, latS, lngS, address, uid;
    private CountDownTimer timer;
    MyDbHelper_E myDbHelper_e;
    SessionManager sessionManager;
    private Location mLastLocation;
    private String lastUpdate = "";
    String tid;
    String localId = "";

    @Override
    public void onCreate() {
        super.onCreate();
        context = AddLocationServices.this;
        myDbHelper_e = new MyDbHelper_E(this);
        sessionManager = new SessionManager(context);
        context.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            uid = sessionManager.getKeyUserId();
            latS = intent.getStringExtra("latitude");
            lngS = intent.getStringExtra("longitude");
        } catch (Exception e) {
            System.out.println("did " + uid + " has been found.");
        }


        checkPermission(context);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!sessionManager.getAlertLatitude().equals("")) {
            addProximityAlert();
        }
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        locationManager.requestLocationUpdates(provider, 6000, 5000, this);
        mLastLocation = new Location(LocationManager.GPS_PROVIDER);
        mLastLocation.setLatitude(Double.parseDouble(sessionManager.getLatitude()));
        mLastLocation.setLongitude(Double.parseDouble(sessionManager.getLongitude()));

        serviceRunning = true;
        return START_STICKY;
    }


    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onLocationChanged(Location location) {
        checkPermission(context);
        lastUpdate = sessionManager.getLastUpdate();


        locationManager.requestLocationUpdates(provider, 5000, 50000, this);
        final double lat = (location.getLatitude());
        final double lng = (location.getLongitude());
        lat1 = String.valueOf(lat);
        lng1 = String.valueOf(lng);
        tid = sessionManager.geTrip();

        if (sessionManager.isLocationShare() || sessionManager.isTripShare()) {
            statusUpdate();
            if (sessionManager.isTripShare()) {
                addLocation(lat1, lng1, uid, tid);
                sessionManager.setLocation(lat1, lng1);

            }
        }

    }

    private void statusUpdate() {
        if (isConnected()) {
            RequestBody requestBody = new FormEncodingBuilder()
                    .add(API.ACTION, API.STATUS)
                    .add("latitude", lat1)
                    .add("longitude", lng1)
                    .add("battery", sessionManager.getLevel())
                    .add("uid", uid)
                    .build();

            HttpProcessor httpProcessor = new HttpProcessor(context, false, API.BASE_URL, HttpProcessor.POST, requestBody);
            httpProcessor.executeRequest(API.STATUS);
            httpProcessor.setHttpResponserListener(this);
        }
    }


    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

            sessionManager.saveBattery(String.valueOf(level));
            getBateryUp();
        }
    };

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onDestroy() {
        stopSelf();
        super.onDestroy();
        serviceRunning = false;
    }

    private void addLocation(String lat, String lng, String uid, String tid) {
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        addLocationToServer(lat, lng, uid, tid, thisDate);
    }


    private void addLocationToServer(String lat, String lng, String uid, String tripid, String cdate) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.ADD_LOCATION)
                .add("latitude", lat)
                .add("longitude", lng)
                .add("uid", uid)
                .add("date", cdate)
                .add("battery", sessionManager.getLevel())
                .add("tid", tripid)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, false, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.ADD_LOCATION);
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.ADD_LOCATION)) {
            if (result.optString("status").equals("success")) {
                sessionManager.setLastUpdate(result.optString("date"));

            } else {
                Toast.makeText(context, result.optString("message"), Toast.LENGTH_SHORT).show();
            }

        }
    }


    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private long getDate(String dateString) {
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);

        long minutes = 0;
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date1 = format.parse(thisDate);
            Date date2 = format.parse(dateString);
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long day = difference / (24 * 60 * 60 * 1000);
            long hours = (int) (difference / (1000 * 60 * 60));
            minutes = (int) (difference / (1000 * 60));
            String hoursString = String.valueOf(hours);
            String minutesString = String.valueOf(minutes);
            String dayString = String.valueOf(day);


        } catch (Exception e) {
            Toast.makeText(context, "error in getting time", Toast.LENGTH_LONG).show();
        }

        return minutes;
    }

    private void addProximityAlert() {
        if (!sessionManager.getAlertLatitude().equals("0.0")) {
            checkPermission(context);
            double latitude = Double.parseDouble(sessionManager.getAlertLatitude());
            double longitude = Double.parseDouble(sessionManager.getAlertLongitude());
            Intent intent = new Intent(PROX_ALERT_INTENT);
            PendingIntent proximityIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            locationManager.addProximityAlert(
                    latitude, // the latitude of the central point of the alert region
                    longitude, // the longitude of the central point of the alert region
                    POINT_RADIUS, // the radius of the central point of the alert region, in meters
                    PROX_ALERT_EXPIRATION, // time for this proximity alert, in milliseconds, or -1 to indicate no                           expiration
                    proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
            );

            IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
            registerReceiver(new ProximityIntentReceiver(), filter);

//        Toast.makeText(getApplicationContext(), "Alert Added", Toast.LENGTH_SHORT).show();
        }
    }

    private void getBateryUp() {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.bSTATUS)
                .add("battery", sessionManager.getLevel())
                .add("uid", uid)
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, false, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.bSTATUS);
        httpProcessor.setHttpResponserListener(this);

    }
}

