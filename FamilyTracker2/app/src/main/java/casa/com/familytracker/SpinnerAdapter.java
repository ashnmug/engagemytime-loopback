package casa.com.familytracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.multidex.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Casa Curo on 5/20/2017.
 */

class SpinnerAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context context;
    private String[] name;
    ArrayList<Container> data;

    public SpinnerAdapter(Context context, ArrayList<Container> data) {
        this.context = context;
        this.name = name;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.spinner_layout, parent, false);

        TextView imageView = (TextView) vi.findViewById(R.id.spinnerText);
        CircleImageView checked = (CircleImageView) vi.findViewById(R.id.checked);
        imageView.setText(data.get(i).getName());
        Picasso.with(context)
                .load(API.IMAGE_URL + data.get(i).getImage()).placeholder(R.drawable.blnk).error(R.drawable.blnk)
                .into(checked);

        return vi;

    }
}