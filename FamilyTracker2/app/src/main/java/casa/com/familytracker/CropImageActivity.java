package casa.com.familytracker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;

import com.example.android.multidex.myapplication.R;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class CropImageActivity extends AppCompatActivity implements View.OnClickListener {
    public String urlImageString;
    public Uri imageUrl;
    private CropImageView mCropView;
    ImageView submitid;
    Context context;
    private Toolbar toolbar;
    private EmojiconEditText msgid;
    private EmojIconActions emojIcon;
    private ImageView emojiImageView;
    private View rootView;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submitid) {

            mCropView.startCrop(null, new CropCallback() {
                @Override
                public void onSuccess(Bitmap cropped) {
                    SessionManager sessionManager = new SessionManager(context);
                    sessionManager.saveUserPicture(SessionManager.encodeTobase64(cropped));

                }

                @Override
                public void onError() {
                    //  write errror code

                }
            }, new SaveCallback() {
                @Override
                public void onSuccess(Uri outputUri) {
                    //  write success code
                }

                @Override
                public void onError() {
                    //  write errror code

                }
            });
            Intent intent = new Intent();
            intent.putExtra("msg", msgid.getText().toString());
            setResult(RESULT_OK, intent);
            finish();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        initToolBar("Crop Image");
        context = CropImageActivity.this;
        mCropView = (CropImageView) findViewById(R.id.cropImageView);
        msgid = (EmojiconEditText) findViewById(R.id.msgid);
        submitid = (ImageView) findViewById(R.id.submitid);
        rootView = findViewById(R.id.layout_root);
        submitid.setOnClickListener(this);

        emojiImageView = (ImageView) findViewById(R.id.emoji_btn);

        emojIcon = new EmojIconActions(this, rootView, msgid, emojiImageView);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

            }

            @Override
            public void onKeyboardClose() {

            }
        });

    }

    public void initToolBar(String title) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        toolbar.setBackgroundColor(colorCode);
        //  setStatusBarColor(ContextCompat.getColor(this, R.color.color_green_primary));
        toolbar.setTitleTextColor(ContextCompat.getColor(getBaseContext(), R.color.white));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getImage();
    }

    private void getImage() {
        try {
            urlImageString = getIntent().getStringExtra("type");
            Uri stringUri = Uri.parse(getIntent().getStringExtra("type"));
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getApplicationContext().getContentResolver(), stringUri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageUrl = stringUri;

            mCropView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
