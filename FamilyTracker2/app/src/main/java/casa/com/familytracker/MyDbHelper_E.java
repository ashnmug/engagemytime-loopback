package casa.com.familytracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyDbHelper_E extends SQLiteOpenHelper {


    SQLiteDatabase db;

    private static final String DB_NAME = "location_tracker";
    private static final int DB_VERSION = 11;

    public static final String I_D = "_id";
    public static final String TABLE_LOC = "location";
    public static final String TABLE_MARKER = "marker";
    public static final String LAT = "latitude";
    public static final String LONG = "longitude";
    public static final String Cus_Id = "cid";
    public static final String TID = "tid";

    public static final String CUR_DATE="cur_date";



    private static final String CREATE_Lat = "CREATE TABLE " + TABLE_LOC + "(" + I_D + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            " " + LAT + " VARCHAR," + LONG + " VARCHAR," + Cus_Id + " INTEGER," + TID + " INTEGER," + CUR_DATE + " VARCHAR );";

    private static final String CREATE_MARKER = "CREATE TABLE " + TABLE_MARKER + "(" + I_D + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            " " + LAT + " VARCHAR );";

    public MyDbHelper_E(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(CREATE_Lat);
        db.execSQL(CREATE_MARKER);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public boolean insertLocation(String lat,String lng,String cid,String tid,String date){
//       String latLngs = latLng.toString();
        SQLiteDatabase db = getWritableDatabase();
        long status = 0;
        ContentValues cv = new ContentValues();
        cv.put(MyDbHelper_E.LAT, lat);
        cv.put(MyDbHelper_E.LONG, lng);
        cv.put(MyDbHelper_E.Cus_Id, cid);
        cv.put(MyDbHelper_E.TID, tid);
        cv.put(MyDbHelper_E.CUR_DATE, date);

        status = db.insert(TABLE_LOC, null, cv);


        if (db.isOpen()) {
            db.close();
        }

        return status > 0 ? true : false;

    }

    // Getting All Contacts
    public  List<HashMap<String,String>> getAllLocation(String cid) {
        List<HashMap<String,String>> locationList = new ArrayList<>();
        String query = "SELECT *  FROM " + TABLE_LOC + " WHERE " + Cus_Id + " = " +  cid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        HashMap<String, String> hashMap;

        if (cursor.moveToFirst()) {
            do {
                hashMap = new HashMap<>();
                hashMap.put(I_D,cursor.getString(0));
                hashMap.put(LAT,cursor.getString(1));
                hashMap.put(LONG,cursor.getString(2));
                hashMap.put(Cus_Id,cursor.getString(3));
                hashMap.put(TID,cursor.getString(4));
                hashMap.put(CUR_DATE,cursor.getString(5));
                locationList.add(hashMap);
            }
            while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }

        // return contact list
        return locationList;
    }

    public boolean insertMarker(String lat,String lng){
//       String latLngs = latLng.toString();
        SQLiteDatabase db = getWritableDatabase();
        long status = 0;
        ContentValues cv = new ContentValues();
        cv.put(MyDbHelper_E.LAT, lat + "," + lng);
//        cv.put(MyDbHelper_E.LONG, LONG);

        status = db.insert(TABLE_MARKER, null, cv);


        if (db.isOpen()) {
            db.close();
        }

        return status > 0 ? true : false;

    }

    // Getting All Contacts
    public List<LatLng> getMarker(String query) {
        List<LatLng> expenseList = new ArrayList<LatLng>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        HashMap<String, String> hashMap;

        if (cursor.moveToFirst()) {
            do {
                String[] strings=cursor.getString(1).split(",");
                double lat = Double.parseDouble(strings[0]);
                double lng = Double.parseDouble(strings[1]);
                expenseList.add(new LatLng(lat,lng) );
            }
            while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }

        // return contact list
        return expenseList;
    }
    public String getMarkerID(String query) {
        String string = new String();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
//        HashMap<String, String> hashMap;

        if (cursor.moveToFirst()) {
            do {
                 string=cursor.getString(0);

            }
            while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }

        // return contact list
        return string;
    }


    public void deleteData(String id) {
        SQLiteDatabase db = getWritableDatabase();
        // Select All Query
        String selectQuery = "DELETE  FROM " + TABLE_LOC  + " WHERE " + I_D + "=" + id;;

        db.execSQL(selectQuery);

        if (db.isOpen()) {
            db.close();
        }

    }

    public void deleteMarker(String id) {
        SQLiteDatabase db = getWritableDatabase();
        // Select All Query
        String selectQuery = "DELETE  FROM " + TABLE_MARKER + " WHERE " + I_D + "=" + id;

        db.execSQL(selectQuery);

        if (db.isOpen()) {
            db.close();
        }

    }

    }

