package casa.com.familytracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.multidex.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Gaurav on 5/17/2017.
 */

public class HListAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context context;
    private String[] name;
    ArrayList<Container> data;


    public HListAdapter(Context context, ArrayList<Container> data) {
        this.context = context;

        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.content_home_navigation, parent, false);
        }
        CircleImageView imageView = (CircleImageView) vi.findViewById(R.id.profileImage);
        TextView textView = (TextView) vi.findViewById(R.id.text);
//        imageView.setImageResource(data.get(i).getResourceIjmage());
        Picasso.with(context)
                .load(API.IMAGE_URL + data.get(i).getImage()).placeholder(R.drawable.blnk)
                .into(imageView);
        textView.setText(data.get(i).getName());
        return vi;

    }
}