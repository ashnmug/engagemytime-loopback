package casa.com.familytracker;

/**
 * Created by Casa Curo on 5/5/2017.
 */
public class ChatMessage {
    public String id;
    public  String like;
    public String title;
    public String time;
    public boolean left;
    public String message;
    public String url;
    public String latitude;
    public String longitude;
    public String uid;

    public ChatMessage(boolean left, String message, String url, String title, String time,String latitude,String longitude,String like,String id,String uid) {
        super();
        this.left = left;
        this.like = like;
        this.id = id;
        this.message = message;
        this.title = title;
        this.time = time;
        this.url = url;
        this.longitude = longitude;
        this.latitude = latitude;
        this.uid = uid;
    }

}