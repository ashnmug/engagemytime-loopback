package casa.com.familytracker;

import android.app.Application;
import android.content.res.Configuration;
import android.support.multidex.MultiDex;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by Casa Curo on 6/3/2017.
 */

public class FamilyLocator extends Application {

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//            @Override
//            public void uncaughtException(Thread thread, Throwable ex) {
//                FirebaseCrash.report(ex);
//                System.exit(0);
//
//            }
//        });


    }


}
