package casa.com.familytracker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.example.android.multidex.myapplication.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by Gaurav on 5/5/2017.
 */

public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> implements HttpProcessor.HttpResponser {
    ViewHolder viewHolder = new ViewHolder();
    String time;
    String user;

    @Override
    public void add(ChatMessage object) {

        viewHolder.chatMessageList.add(object);
        super.add(object);
    }

    @Override
    public void remove(ChatMessage object) {

        viewHolder.chatMessageList.remove(object);
        super.remove(object);
    }

    public ChatArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.viewHolder.context = context;

    }

    @Override
    public void responseResult(JSONObject result, String TAG) {

    }

    static class ViewHolder {
        private EmojiconTextView chatText;
        private LinearLayout linearLayout;
        private TextView title;
        private TextView chkin, dateText;

        private List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>();
        private ImageView cimg;
        private ImageView STATUS;

        private Context context;
        private String imgUrl = "";
        private String id = "";
        private SessionManager sessionManager;
        boolean[] chkList;

        public TextView time;
    }

    public int getCount() {
        return this.viewHolder.chatMessageList.size();
    }

    public ChatMessage getItem(int index) {
        return this.viewHolder.chatMessageList.get(index);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ChatMessage chatMessageObj = getItem(position);
        final String[] time = chatMessageObj.time.split(" ");

        viewHolder.chkList = new boolean[viewHolder.chatMessageList.size()];
        LayoutInflater inflater = (LayoutInflater) viewHolder.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (chatMessageObj.left) {
            convertView = inflater.inflate(R.layout.right, parent, false);
            viewHolder.STATUS = (ImageView) convertView.findViewById(R.id.imageStatus);
        } else {
            convertView = inflater.inflate(R.layout.left, parent, false);

            viewHolder.chkin = (TextView) convertView.findViewById(R.id.chkin);
            viewHolder.linearLayout = (LinearLayout) convertView.findViewById(R.id.linear);
            if (chatMessageObj.url.equals("Checked In")) {
                viewHolder.chkin.setVisibility(View.VISIBLE);
                viewHolder.chkin.setText(chatMessageObj.message + " at " + time[1]);

            } else {
                viewHolder.chkin.setVisibility(View.GONE);
                viewHolder.linearLayout.setVisibility(View.VISIBLE);
            }

        }
        viewHolder.time = (TextView) convertView.findViewById(R.id.time);

        viewHolder.chatText = (EmojiconTextView) convertView.findViewById(R.id.msgr);
        viewHolder.dateText = (TextView) convertView.findViewById(R.id.datestring);

        viewHolder.title = (TextView) convertView.findViewById(R.id.title);
        viewHolder.cimg = (ImageView) convertView.findViewById(R.id.imageView);
        viewHolder.time.setText(time[1]);
        String[] strings = chatMessageObj.title.split(" ");
        viewHolder.title.setText(strings[0]);
        viewHolder.chatText.setText(chatMessageObj.message + "      ");

        if (position > 0) {

            final ChatMessage chatMessageObjOld = getItem(position - 1);
            final String[] timeold = chatMessageObjOld.time.split(" ");
            if (timeold[0].equals(time[0])) {

                viewHolder.dateText.setVisibility(View.GONE);
            } else {
                viewHolder.dateText.setVisibility(View.VISIBLE);
            }

            if (chatMessageObjOld.uid.equals(chatMessageObj.uid) && !chatMessageObj.left) {

                viewHolder.title.setVisibility(View.GONE);
            } else if (!chatMessageObjOld.uid.equals(chatMessageObj.uid) && !chatMessageObj.left) {
                viewHolder.title.setVisibility(View.VISIBLE);
            }
        }
        if (chatMessageObj.left) {
            if (chatMessageObj.latitude.equals("status")) {
                viewHolder.STATUS.setImageResource(R.drawable.wait);
            } else {
                viewHolder.STATUS.setImageResource(R.drawable.single);
            }
        }
        viewHolder.dateText.setText(time[0]);
        viewHolder.id = chatMessageObj.id;

        if (!chatMessageObj.url.equals("") && !chatMessageObj.url.contains(".jpg")) {
            if (!chatMessageObj.url.equals("Checked In")) {
                viewHolder.cimg.setVisibility(View.VISIBLE);
                if (chatMessageObj.url.contains("googleapis")) {
                    AQuery aq = new AQuery(viewHolder.context);

                    aq.id(viewHolder.cimg).image(chatMessageObj.url, true, true, 0,
                            0, null, AQuery.FADE_IN);
                    viewHolder.imgUrl = chatMessageObj.url;
                } else {
                    byte[] decodedString = Base64.decode(chatMessageObj.url, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    viewHolder.cimg.setImageBitmap(decodedByte);

                }
            }
        } else if (chatMessageObj.url.contains(".jpg")) {
            viewHolder.cimg.setVisibility(View.VISIBLE);
            AQuery aq = new AQuery(viewHolder.context);
            aq.id(viewHolder.cimg).image(API.IMAGE_URL + chatMessageObj.url, true, true, 0,
                    0, null, AQuery.FADE_IN);
//            viewHolder.imgUrl = API.IMAGE_URL + chatMessageObj.url;

        } else {
            viewHolder.cimg.setVisibility(View.GONE);


        }
        viewHolder.cimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chatMessageObj.latitude.equals("")) {
                    Intent st = new Intent(viewHolder.context, CurrentLocation.class);
                    st.putExtra("url", API.IMAGE_URL + chatMessageObj.url);
                    st.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    viewHolder.context.startActivity(st);
                } else {
                    String geoUri = "http://maps.google.com/maps?q=loc:" + chatMessageObj.latitude + "," + chatMessageObj.longitude + " (" + chatMessageObj.message + ")";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    viewHolder.context.startActivity(intent);
                }
            }
        });

        return convertView;
    }
}






