package casa.com.familytracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.example.android.multidex.myapplication.R;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import org.json.JSONObject;

/**
 * Created by Casa Curo on 5/31/2017.
 */

public class Splash extends Activity implements HttpProcessor.HttpResponser {
    private static int SPLASH_TIME_OUT = 3000;
    SessionManager sessionManager;
    private int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private Context context;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_alert);
        context = Splash.this;
        sessionManager = new SessionManager(context);
        String string = FirebaseInstanceId.getInstance().getToken();
//        Toast.makeText(context, string, Toast.LENGTH_LONG).show();
        showAdd();
        getSplash();
        getSplashImage();

    }


    private void showAdd() {

        mInterstitialAd = new InterstitialAd(this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }


    private void getSplash() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (sessionManager.isLoggedIn()) {
                    Intent i = new Intent(context, HomeNavigation.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(context, IntroPage.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);


    }

    private void getSplashImage() {
        new Handler().post(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

//                getUpdate();

            }
        });


    }


    private void getUpdate() {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, "get_versions")
                .add("code", "2")
                .add("aid", "casa.com.familytracker")
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, false, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("get_versions");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (result.optString("status").equals("success")) {
            getDialogue();
        } else {
            if (sessionManager.isLoggedIn()) {
                Intent i = new Intent(context, HomeNavigation.class);
                startActivity(i);
                finish();
            } else {
                Intent i = new Intent(context, IntroPage.class);
                startActivity(i);
                finish();
            }
        }
    }

    private void getDialogue() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this); //alert for confirm to delete
        builder.setMessage("Please update to the current version of the application");    //set message
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() { //when click on DELETE
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://goo.gl/xv5t8h"));
                startActivity(browserIntent);

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (sessionManager.isLoggedIn()) {
                    Intent i = new Intent(context, HomeNavigation.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(context, IntroPage.class);
                    startActivity(i);
                    finish();
                }
                dialog.dismiss();
            }
        }).show();  //show alert dialog
    }

}
