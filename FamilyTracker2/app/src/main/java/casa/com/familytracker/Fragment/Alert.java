package casa.com.familytracker.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import casa.com.familytracker.API;
import casa.com.familytracker.CheckIn;
import casa.com.familytracker.SessionManager;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

public class Alert extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Marker currLocationMarker;
    View view;
    TextView address;
    Button setBtn;
    Circle circle;
    String groupid;
    String lat = "0.0", lng = "0.0";
    SessionManager sessionManager;
    private SupportMapFragment mFragment;
    GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private String latString = "0.0", lngString = "0.0";
    private MarkerOptions markerOptions;
    private LatLng latLngFinal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_emergency_alert, container, false);
        }
        mFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map1);
        mFragment.getMapAsync(this);
        setBtn = (Button) view.findViewById(R.id.setBtn);
        address = (TextView) view.findViewById(R.id.textView);
        sessionManager = new SessionManager(getActivity());
        groupid = getActivity().getIntent().getStringExtra("groupid");
        lat = sessionManager.getAlertLatitude();
        lng = sessionManager.getAlertLongitude();
        if (lat.equals("0.0")) {
            lat = sessionManager.getLatitude();
            lng = sessionManager.getLongitude();

        }


        return view;
    }

    private void getMarker(double v, double v1) {
        LatLng latLng1 = new LatLng(v, v1);
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng1);
        markerOptions.title(sessionManager.getKeyUserName());
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(sessionManager.getProfileImagePath())));
        currLocationMarker = mGoogleMap.addMarker(markerOptions);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng1).zoom(12).build();

        mGoogleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

    private Bitmap getMarkerBitmapFromView(String url) {

        View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.imageView);

        Picasso.with(getActivity())
                .load(API.IMAGE_URL + url).placeholder(R.drawable.blnk)
                .into(markerImageView);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
//        if (!lat.equals("0.0")) {
//
//        }

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        getMarker(Double.parseDouble(lat), Double.parseDouble(lng));
        setBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double latCamera = mGoogleMap.getCameraPosition().target.latitude;
                double lngCamera = mGoogleMap.getCameraPosition().target.longitude;
                latString = String.valueOf(latCamera);
                lngString = String.valueOf(lngCamera);
                latLngFinal = new LatLng(latCamera, lngCamera);
                sessionManager.setAlert(latString, lngString);
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Alert zone added")
                        .setConfirmText("Ok")
                        .show();
//                Toast.makeText(getActivity(), "Alert zone added at co:ordinates " + latString + "," + lngString, Toast.LENGTH_SHORT).show();
                if (currLocationMarker != null) {
                    currLocationMarker.setPosition(latLngFinal);
                } else {
                    getMarker(latCamera, lngCamera);
                }
            }
        });

    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
