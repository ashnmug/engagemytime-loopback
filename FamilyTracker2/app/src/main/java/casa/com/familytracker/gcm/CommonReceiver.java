package casa.com.familytracker.gcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.widget.Toast;

import java.security.Policy;

import casa.com.familytracker.AddLocationServices;
import casa.com.familytracker.MyDialogue;
import casa.com.familytracker.SessionManager;


/**
 * Created by Gaurav on 06-04-2017.
 */

public class CommonReceiver extends BroadcastReceiver {
    SessionManager sessionManager;
    LocationManager locationManager;

    public void onReceive(Context paramContext, Intent paramIntent) {
        sessionManager = new SessionManager(paramContext);
        locationManager = (LocationManager) paramContext.getSystemService(Context.LOCATION_SERVICE);

        if (sessionManager.isLocationShare() || sessionManager.isTripShare()) {
//            checkLocation(paramContext);

//            locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER,true);

            if (sessionManager.isLoggedIn() && !AddLocationServices.serviceRunning) {

                Intent intent = new Intent(paramContext, AddLocationServices.class);
                intent.putExtra("latitude", sessionManager.getLatitude());
                intent.putExtra("longitude", sessionManager.getLongitude());
                paramContext.startService(intent);

//

            }
            checkLocation(paramContext);
        }
    }

    private void checkLocation(final Context paramContext) {
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            SessionManager sessionManager = new SessionManager(paramContext);
            if (sessionManager.isTripShare() || sessionManager.isLocationShare()) {
                Intent i = new Intent(paramContext, MyDialogue.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                paramContext.startActivity(i);
            }
        }

    }


}