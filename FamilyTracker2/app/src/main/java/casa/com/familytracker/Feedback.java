package casa.com.familytracker;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Feedback extends AppCompatActivity implements View.OnClickListener, HttpProcessor.HttpResponser {
    EditText comment;
    Button submit;
    Button rate;
    private Context context;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        context = Feedback.this;
        sessionManager = new SessionManager(context);

        comment = (EditText) findViewById(R.id.comment);
        submit = (Button) findViewById(R.id.submit);
        rate = (Button) findViewById(R.id.rate);
        submit.setOnClickListener(this);
        rate.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {
            if (!comment.getText().toString().equals("")) {
                RequestBody requestBody = new FormEncodingBuilder()
                        .add(API.ACTION, API.FEEDBACK)
                        .add("comment", comment.getText().toString())
                        .add("uid", sessionManager.getKeyUserId())

                        .build();

                HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
                httpProcessor.executeRequest(API.FEEDBACK);
                httpProcessor.setHttpResponserListener(this);

            }
        } else if (v.getId() == R.id.rate) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=casa.com.familytracker"));
            startActivity(browserIntent);
        }
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.FEEDBACK)) {
            if (result.optString("status").equals("success")) {
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Feedback submitted")
                        .setConfirmText("Ok")
                        .show();
            }
        }
    }
}
