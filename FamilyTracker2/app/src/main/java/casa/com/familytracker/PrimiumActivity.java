package casa.com.familytracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.example.android.multidex.myapplication.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class PrimiumActivity extends AppCompatActivity implements HttpProcessor.HttpResponser {
    SessionManager sessionManager;
    private Context context;
    private String ratingS;
    private MaterialRatingBar rating;
    private ImageView emozy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primium);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        context = PrimiumActivity.this;
        sessionManager = new SessionManager(context);
        rating = (MaterialRatingBar) findViewById(R.id.rating);
        ImageView redirect = (ImageView) findViewById(R.id.bttnm);

        emozy = (ImageView) findViewById(R.id.emozy);
        if (sessionManager.getRating().equals("")) {
            rating.setRating(Float.parseFloat("0"));
        } else {
            rating.setRating(Float.parseFloat(sessionManager.getRating()));
        }
        float rating1 = rating.getRating();

        if (rating1 <= 1) {
            emozy.setImageResource(R.drawable.a);
        } else if (rating1 <= 2) {
            emozy.setImageResource(R.drawable.b);
        } else if (rating1 <= 3) {
            emozy.setImageResource(R.drawable.c);
        } else if (rating1 <= 4) {
            emozy.setImageResource(R.drawable.d);
        } else if (rating1 <= 5) {
            emozy.setImageResource(R.drawable.e);
        }
        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating <= 1) {
                    emozy.setImageResource(R.drawable.a);
                } else if (rating <= 2) {
                    emozy.setImageResource(R.drawable.b);
                } else if (rating <= 3) {
                    emozy.setImageResource(R.drawable.c);
                } else if (rating <= 4) {
                    emozy.setImageResource(R.drawable.d);
                } else if (rating <= 5) {
                    emozy.setImageResource(R.drawable.e);
                }

                getRating(rating);
            }
        });
        redirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=casa.com.familytrackerpre"));
                startActivity(browserIntent);
            }
        });
    }


    @Override
    protected void onPause() {
        sessionManager.saveOn(false);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sessionManager.saveOn(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.

                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getRating(float rating) {
        ratingS = String.valueOf(rating);
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.RATEUS)
                .add("rating", ratingS)
                .add("uid", sessionManager.getKeyUserId())

                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.RATEUS);
        httpProcessor.setHttpResponserListener(this);

    }


    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.RATEUS)) {
            sessionManager.setRating(ratingS);
            float rating = Float.parseFloat(ratingS);
            if (rating <= 1) {
                emozy.setImageResource(R.drawable.a);
            } else if (rating <= 2) {
                emozy.setImageResource(R.drawable.b);
            } else if (rating <= 3) {
                emozy.setImageResource(R.drawable.c);
            } else if (rating <= 4) {
                emozy.setImageResource(R.drawable.d);
            } else if (rating <= 5) {
                emozy.setImageResource(R.drawable.e);
            }
            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                    .setContentText(result.optString("message"))
                    .setConfirmText("Ok")
                    .show();
        }
    }

}
