package casa.com.familytracker.gcm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.android.multidex.myapplication.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import casa.com.familytracker.API;
import casa.com.familytracker.HttpProcessor;
import casa.com.familytracker.SessionManager;
import casa.com.familytracker.app.Config;

public class MyInstanceIDListenerService extends FirebaseInstanceIdService implements HttpProcessor.HttpResponser {

    private static final String TAG = MyInstanceIDListenerService.class.getSimpleName();
    private String refreshedToken;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    @Override
    public void onTokenRefresh() {
        Log.e(TAG, "onTokenRefresh");

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        if (refreshedToken != null) {
            serverToken(refreshedToken);

        }


    }

    private void serverToken(String refreshedToken) {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        this.refreshedToken = refreshedToken;
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.SAVE_TOKEN)
                .add("deviceid", refreshedToken)
                .add("uid", sessionManager.getKeyUserId())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getApplicationContext(), false, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.SAVE_TOKEN);
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        SessionManager sessionManager = new SessionManager(getApplicationContext());

        if (result.optString("status").equals("success")) {
            sessionManager.saveDeviceId(refreshedToken);
        }
    }
}