package casa.com.familytracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.multidex.myapplication.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChangePassword extends AppCompatActivity implements HttpProcessor.HttpResponser, View.OnClickListener {

    private Context context;
    private SessionManager sessionManager;
    private EditText newPswd, cPswd, oldPswd;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        context = ChangePassword.this;
        sessionManager = new SessionManager(context);

        oldPswd = (EditText) findViewById(R.id.crntp);
        newPswd = (EditText) findViewById(R.id.pswrd);
        cPswd = (EditText) findViewById(R.id.cpswrd);
        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {
            String newP = newPswd.getText().toString();
            String old = oldPswd.getText().toString();
            String cnfrm = cPswd.getText().toString();
            if (!newP.equals("") || !old.equals("") || !cnfrm.equals("")) {
                if (cPswd.getText().toString().equals(newPswd.getText().toString())) {
                    RequestBody requestBody = new FormEncodingBuilder()
                            .add(API.ACTION, API.PASSWORD)
                            .add("password", newPswd.getText().toString())
                            .add("uid", sessionManager.getKeyUserId())
                            .build();
                    HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL, HttpProcessor.POST, requestBody);
                    httpProcessor.executeRequest(API.PASSWORD);
                    httpProcessor.setHttpResponserListener(this);
                } else {
                    Toast.makeText(context, "Password and confirm password must be same", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Field should not be empty", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void responseResult(JSONObject result, String TAG) {
        if (TAG.equals(API.PASSWORD)) {
            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Password has been updated")
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            startActivity(new Intent(context, LoginScreen.class));
                            sessionManager.saveLoggedIn(false);
                            finish();
                        }
                    })
                    .show();
        }
    }
}
