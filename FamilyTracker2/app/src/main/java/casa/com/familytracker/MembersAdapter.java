package casa.com.familytracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.multidex.myapplication.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.ProgressHelper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Gaurav on 5/26/2017.
 */

public class MembersAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context context;
    private String[] name;
    ArrayList<Container> data;
    SessionManager sessionManager;

    public MembersAdapter(Context context, ArrayList<Container> data) {
        this.context = context;
        this.name = name;
        this.data = data;
        sessionManager = new SessionManager(context);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.members, parent, false);

        CircleImageView imageView = (CircleImageView) vi.findViewById(R.id.roundImage);
        ImageView delete = (ImageView) vi.findViewById(R.id.deleteMem);
        TextView name = (TextView) vi.findViewById(R.id.name);
        TextView mobile = (TextView) vi.findViewById(R.id.mobile);

        if (!sessionManager.getKeyUserId().equals(data.get(i).refId())) {
            delete.setVisibility(View.GONE);

        } else {
            if (!sessionManager.getKeyUserId().equals(data.get(i).getUid())) {
                delete.setVisibility(View.VISIBLE);
            } else {
                delete.setVisibility(View.GONE);
            }
        }
        Picasso.with(context)
                .load(API.IMAGE_URL + data.get(i).getImage()).placeholder(R.drawable.blnk)
                .into(imageView);
        name.setText(data.get(i).getName());
        mobile.setText(data.get(i).getMobile());

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteMembers(data.get(i).getMid());
            }
        });
        return vi;

    }

    private void deleteMembers(final String id) {

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Won't be able to recover this file!")
                .setConfirmText("Yes,delete it!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        delete(sDialog, id);

                    }
                })
                .show();
    }

    private void delete(final SweetAlertDialog sDialog, String id) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, API.DELETE_MEMBER)
                .add("id", id)
                .build();
        final ProgressHelper hlp = sDialog.getProgressHelper();
        hlp.spin();
        HttpProcessor httpProcessor = new HttpProcessor(context, false, API.BASE_URL, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest(API.DELETE_MEMBER);
        httpProcessor.setHttpResponserListener(new HttpProcessor.HttpResponser() {
            @Override
            public void responseResult(JSONObject result, String TAG) {

                hlp.stopSpinning();
                sDialog
                        .setTitleText("Deleted!")
                        .setContentText("Member has been deleted!")
                        .setConfirmText("OK")
                        .setConfirmClickListener(null)
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            }
        });

    }
}