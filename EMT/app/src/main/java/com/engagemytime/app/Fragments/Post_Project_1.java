package com.engagemytime.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.engagemytime.app.Activity.Post_Project;
import com.engagemytime.app.Model.ChangeNotify;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;

/**
 * Created by Casa Curo on 9/16/2017.
 */

public class Post_Project_1 extends Fragment implements View.OnClickListener {
    View view;
    Button next, exit;
    private ChangeNotify changeNotifyListener;
    EditText editText, editText1;
    SessionManager sessionManager;

    public void setChangeNotifyListener(ChangeNotify changeNotifyListener) {
        this.changeNotifyListener = changeNotifyListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_post_project_1, container, false);

        editText = (EditText) view.findViewById(R.id.editText);
        editText1 = (EditText) view.findViewById(R.id.editText1);
        next = (Button) view.findViewById(R.id.submit_project);
        exit = (Button) view.findViewById(R.id.exit);
        exit.setOnClickListener(this);
        next.setOnClickListener(this);
        sessionManager = new SessionManager(getActivity());
        return view;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.submit_project) {
            if (null != changeNotifyListener) {

                sessionManager.setProjectName(editText.getText().toString());
                sessionManager.setProDesc(editText1.getText().toString());

                changeNotifyListener.changeFragment(Post_Project.STEP_2, "Step 2");
            }
        } else if (view.getId() == R.id.exit) {
            getActivity().finish();
        }
    }
}