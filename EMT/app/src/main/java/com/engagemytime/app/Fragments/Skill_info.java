package com.engagemytime.app.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.engagemytime.app.Activity.Add_Experience;
import com.engagemytime.app.Adapter.ExpListAdapter;
import com.engagemytime.app.Adapter.SkillAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Skill_info extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser, TabLayout.OnTabSelectedListener {
    private View view;
    private static final int FILE_SELECT_CODE = 10;
    private static final String SKILL = "skill";
    private static final String RESUME = "resume";
    private static final String PDF_TYPE = "application/pdf";
    private Context context;
    int serverResponseCode = 0;
    String url = "";
    String origName = "";
    int pageNumber = 0;
    private RecyclerView recyclerSkill, recycleListExp;
    LinearLayout recyclerExp;
    private ArrayList<Container> arrayListSkill;
    Integer integer[] = {R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html};
    FloatingActionButton fabResume, fabSkill, fabSkillAdd, fabExp;
    ArrayList<Container> expListArray;
    String id = "";
    private SessionManager sessionManager;
    private Container container = null;
    private JSONArray jsonArray = null;
    private String origFile = "";
    private ArrayList<Container> arrayList;
    TabLayout tabLayout;
    private JSONArray jsonArraySkill;
    private JSONArray jsonArrayExp;
    private Spinner skillType;
    ArrayList<String> skillTypeArrays;
    private ArrayAdapter<String> arrayAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.skill_info, container, false);
        id = getActivity().getIntent().getStringExtra("pid");
        getInit();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 4);
        recyclerSkill.setLayoutManager(gridLayoutManager);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        recycleListExp.setLayoutManager(mLayoutManager);
        getSkillList();

        return view;
    }

    private void getInit() {

        context = getActivity();
        sessionManager = new SessionManager(context);
        skillTypeArrays = new ArrayList<>();
        skillTypeArrays.add("Profational");
        skillTypeArrays.add("Skilled");
        skillTypeArrays.add("Semi Skilled");
        skillTypeArrays.add("UnSkilled");
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        skillType = (Spinner) view.findViewById(R.id.skillType);
        arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, skillTypeArrays);
        skillType.setAdapter(arrayAdapter);
        tabLayout.addTab(tabLayout.newTab().setText("Skill"));
        tabLayout.addTab(tabLayout.newTab().setText("Experience"));
        recycleListExp = (RecyclerView) view.findViewById(R.id.expList);
        recyclerSkill = (RecyclerView) view.findViewById(R.id.recyclerSkill);
        fabExp = (FloatingActionButton) view.findViewById(R.id.fab1);
        fabSkillAdd = (FloatingActionButton) view.findViewById(R.id.fab11);
        fabSkill = (FloatingActionButton) view.findViewById(R.id.fab2);
        fabResume = (FloatingActionButton) view.findViewById(R.id.fab);

        fabExp.setOnClickListener(this);
        fabSkillAdd.setOnClickListener(this);
        fabResume.setOnClickListener(this);
        fabSkill.setOnClickListener(this);

        tabLayout.addOnTabSelectedListener(this);
        if (sessionManager.getKeyUserId().equals(id) || id == null) {
            fabSkill.setVisibility(View.VISIBLE);
            fabExp.setVisibility(View.VISIBLE);
            id = sessionManager.getKeyUserId();
        } else {
            fabSkill.setVisibility(View.GONE);
            fabExp.setVisibility(View.GONE);
        }
    }

    private void getPdf() {
        Uri path = Uri.parse(url);
        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
        pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfOpenintent.setDataAndType(path, PDF_TYPE);
        Intent intent = Intent.createChooser(pdfOpenintent, origName);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {

        }
    }

    private void getSkillList() {

        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("personId", id);
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "SkillProfiles?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_SKILL");
        httpProcessor.setHttpResponserListener(this);


    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab) {
            if (container != null) {
                getPdf();
            }
        } else if (view.getId() == R.id.fab2) {
            if (container != null) {
                showFileChooser();
            }
        } else if (view.getId() == R.id.fab1) {
            String skillArray = "";
            if (jsonArrayExp != null) {
                skillArray = jsonArrayExp.toString();
            }
            Intent intent = new Intent(context, Add_Experience.class);
            intent.putExtra(SKILL, skillArray);
            intent.putExtra("type", "add_exp");
            intent.putExtra("id", container.getId());
            intent.putExtra(RESUME, "");
            startActivityForResult(intent, 2);

        } else if (view.getId() == R.id.fab11) {
            String skillArray = "";
            if (jsonArraySkill != null) {
                skillArray = jsonArraySkill.toString();
            }
            Intent intent = new Intent(context, Add_Experience.class);
            intent.putExtra(SKILL, skillArray);
            intent.putExtra("type", "add_skill");
            intent.putExtra(RESUME, "");
            intent.putExtra("id", container.getId());
            startActivityForResult(intent, 2);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FILE_SELECT_CODE) {
                Uri uri = data.getData();
                Log.d(TAG, "File Uri: " + uri.toString());
                // Get the path
                //    String path = null;
                try {
                    // String path = FileUtils.getPath(getActivity(), uri);

                    // Alternatively, use FileUtils.getFile(Context, Uri)
                    if (uri.getPath() != null) {
                        File file = new File(uri.getPath());
//                        file.getAbsolutePath();


                        Toast.makeText(context, "Pdf Found : " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                        //      uploadFile("http://88.198.38.226:3001/api/uploadFiles/resume/upload", new File(file.getAbsolutePath()), context);
                        uploadFile( new File(file.getAbsolutePath()), context);

                    } else {
                        Toast.makeText(context, "Pdf Not Found", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  Log.d(TAG, "File Path: " + path);
            } else {
                getSkillList();
            }
        }
    }

    public void uploadFile(File file, final Context v) {

        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)

                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse(PDF_TYPE), file))
                .addFormDataPart("result", "my_image")
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "uploadFiles/resume/upload", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("GET_RESUME");
        httpProcessor.setHttpResponserListener(this);
    }


    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

        intent.setType(PDF_TYPE);
//        String[] mimetypes = {"application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"};
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(context, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void responseResult(String result, String TAG) {
        try {
            switch (TAG) {
                case "GET_SKILL":

                    arrayList = new ArrayList<>();
                    arrayListSkill = new ArrayList<>();

                    JSONArray jsonArrayCont = new JSONArray(result);

                    setSkillDetails(jsonArrayCont);
                    break;
                case "UPDATE_PERSON":

                    Toast.makeText(context, "Updated -->" + result, Toast.LENGTH_SHORT).show();
                    getSkillList();
                    break;
                case "GET_RESUME":
                    Toast.makeText(context, result, Toast.LENGTH_SHORT).show();

                    JSONObject imageJson = new JSONObject(result);
                    JSONObject filesJson = imageJson.optJSONObject("result");
                    JSONObject fileJson = filesJson.optJSONObject("files");
                    JSONArray fileJsonArray = fileJson.optJSONArray("file");
                    JSONObject newJsonDetails = fileJsonArray.getJSONObject(0);
                    sessionManager.setResumePath(API.RESUME_URL + newJsonDetails.optString("name"));
                    Container container = new Container();
                    container = arrayListSkill.get(0);

                    origFile = newJsonDetails.optString("originalFilename");
                    sessionManager.setOrig(origFile);
                    jsonArray = container.getJsonArray();
                    Intent intent = new Intent(context, Add_Experience.class);
                    intent.putExtra(SKILL, jsonArray.toString());
                    intent.putExtra(RESUME, sessionManager.getResumePath());
                    intent.putExtra("orig", origFile);
                    startActivityForResult(intent, 2);

                    break;

                default:
            }
        } catch (Exception ignored) {
//write code here
        }

    }

    private void setSkillDetails(JSONArray jsonArrayCont) {
        try {

            expListArray = new ArrayList<>();
            for (int a = 0; a < jsonArrayCont.length(); a++) {
                container = new Container();
                JSONObject jsonObject = jsonArrayCont.getJSONObject(a);

                jsonArraySkill = jsonObject.optJSONArray("list_of_skills");
                for (int b = 0; b < jsonArraySkill.length(); b++) {
                    Container containerSkill = new Container();
                    containerSkill.setTitle(jsonArraySkill.getString(b));
                    containerSkill.setImageResource(integer[a]);
                    arrayList.add(containerSkill);
                }
                jsonArrayExp = jsonObject.optJSONArray("list_of_experiences");
                for (int b = 0; b < jsonArrayExp.length(); b++) {

                    JSONObject jsonObjectExp = jsonArrayExp.getJSONObject(b);
                    getExperienceDetails(jsonObjectExp);
                }

                container.setResumeUrl(jsonObject.optString("link_to_resume"));
                container.setStatus(jsonObject.optString("current_work"));

                JSONObject urlJson = jsonObject.optJSONObject("link_to_resume");
                url = urlJson.optString("path");
                container.setId(jsonObject.optString("id"));
                container.setType(jsonObject.optString("skill_type"));
                for (int type = 0; type < skillTypeArrays.size(); type++) {
                    if (container.getType().equals(skillTypeArrays.get(type)))
                        skillType.setSelection(type);
                }
                container.setJsonArray(jsonArrayCont);
                arrayListSkill.add(container);
                setSkillList();
                setExpList(expListArray);
            }
        } catch (Exception ignored) {

        }
    }

    private void getExperienceDetails(JSONObject jsonObjectExp) {
        Container containerExp = new Container();
        containerExp.setTitle(jsonObjectExp.optString("role"));
        containerExp.setCompany(jsonObjectExp.optString("company_name"));
        containerExp.setComment(jsonObjectExp.optString("description"));
        containerExp.setAddress_details(jsonObjectExp.optString("industry"));
        containerExp.setExp(jsonObjectExp.optString("year_experience"));

        setCompDetails(containerExp);

    }

    private void setCompDetails(Container container) {
        expListArray.add(container);
    }

    private void setSkillList() {
        SkillAdapter skillAdapter = new SkillAdapter(context,arrayList);
        recyclerSkill.setAdapter(skillAdapter);
        skillAdapter.notifyDataSetChanged();
        skillAdapter.setOnItemClickListner(new SkillAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.image) {
//write code here
                }
            }
        });
        skillAdapter.setOnItemLongClickListner(new SkillAdapter.onRecyclerViewItemLongClickListner() {
            @Override
            public void onItemLongClickListner(View view, int position) {

                showDialogue(position, jsonArraySkill, "list_of_skills");
            }
        });
    }

    private void showDialogue(final int position, final JSONArray jsonArray, final String method) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Alert !!!");
        alertDialogBuilder.setMessage("Do you want to remove this skill/exp from your list ?");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
//
                        jsonArraySkill.remove(position);
                        JSONObject jsonObject = new JSONObject();

                        try {
                            jsonObject.put(method, jsonArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        updateSkillInfo(jsonObject);
//                     convJsonData(position);
                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
//write code here
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void setExpList(ArrayList<Container> arrayList) {
        ExpListAdapter skillAdapter = new ExpListAdapter(arrayList);
        recycleListExp.setAdapter(skillAdapter);
        skillAdapter.notifyDataSetChanged();
        skillAdapter.setOnItemClickListner(new ExpListAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.image) {
//write code here
                }
            }
        });
        skillAdapter.setOnItemLongClickListner(new ExpListAdapter.onRecyclerViewItemLongClickListner() {
            @Override
            public void onItemLongClickListner(View view, int position) {
                showDialogue(position, jsonArrayExp, "list_of_experiences");
            }
        });
    }

    private void updateSkillInfo(JSONObject jsonObject) {
        JSONObject jsonObjecta = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
//            if (id == null) {
//                id = sessionManager.getKeyUserId();
//            }
            jsonObjecta.put("id", container.getId());
            jsonObjectP.put("where", jsonObjecta);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "where=" + jsonObjecta.toString();
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "SkillProfiles/update?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("UPDATE_PERSON");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0) {
            recyclerSkill.setVisibility(View.VISIBLE);
            skillType.setVisibility(View.VISIBLE);
            recycleListExp.setVisibility(View.GONE);
        } else {
            recycleListExp.setVisibility(View.VISIBLE);
            skillType.setVisibility(View.GONE);
            recyclerSkill.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
//write code here
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
//write code here
    }
}