package com.engagemytime.app.Intro;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.engagemytime.app.R;

import org.w3c.dom.Text;


public class First extends Fragment {
    View view;
    private static final String ARG_LAYOUT_RES_ID = "imageId";
    private int imageId;
    private TextView title;
    private TextView disc;
    private ImageView imageback;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.intro_page, container, false);
        imageback = (ImageView) view.findViewById(R.id.imageback);
        imageback.setImageResource(R.drawable.introtwo);
        return view;
    }


}