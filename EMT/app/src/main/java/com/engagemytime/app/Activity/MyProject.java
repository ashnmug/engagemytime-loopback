package com.engagemytime.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.engagemytime.app.Adapter.ProjectAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;

import java.util.ArrayList;

public class MyProject extends BaseActivity {
    private RecyclerView recyclerProject;
    private ArrayList<Container> projectList;
    private ProjectAdapter projectAdapter;
    private Context context;
    private static final String DESIGN ="Material DESIGN";
    Integer integer[] = {R.drawable.icon1, R.drawable.icon2, R.drawable.icon3, R.drawable.icon4, R.drawable.icon5, R.drawable.icon6};
    String string[] = {DESIGN, DESIGN, DESIGN, DESIGN, DESIGN, DESIGN};

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_project);
        context = MyProject.this;
        recyclerProject = (RecyclerView) findViewById(R.id.recyclerProject);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        recyclerProject.setLayoutManager(gridLayoutManager);
        getDummyProjectList();

    }

    private void getDummyProjectList() {
        projectList = new ArrayList<>();
        for (int a = 0; a < 1; a++) {
            Container container = new Container();
            container.setTitle(string[a]);
            container.setPrice("$ " + a + 1 + "00");
            container.setImageResource(integer[a]);
            container.setClassType("project");
            projectList.add(container);
        }

        projectAdapter = new ProjectAdapter( projectList);
        recyclerProject.setAdapter(projectAdapter);
        projectAdapter.notifyDataSetChanged();
        getOnClick();
    }

    private void getOnClick() {
        projectAdapter.setOnItemClickListner(new ProjectAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.card) {

                    Intent intent = new Intent();

                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

}