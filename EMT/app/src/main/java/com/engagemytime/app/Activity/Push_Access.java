package com.engagemytime.app.Activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Push_Access extends BaseActivity {
    TabLayout daysTab;
    ArrayList<String> weekDays;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push__access);
        context = Push_Access.this;
        daysTab = (TabLayout) findViewById(R.id.tabLayout);
        weekDays = new ArrayList<>();
        weekDays = getTabDaysList();
        daysTab.setTabMode(TabLayout.MODE_SCROLLABLE);
        for (int i = 0; i < 7; i++) {
            daysTab.addTab(daysTab.newTab().setText(weekDays.get(i)));
        }

        changeTabsFont();
    }

    private ArrayList<String> getTabDaysList() {

        DateTime today = new DateTime().withTimeAtStartOfDay();
        Calendar calendar = Calendar.getInstance();
        DateTimeFormatter formatterM = DateTimeFormat.forPattern("MMMM");
        Date date = new Date();
        initToolBar(formatterM.print(today));
        String[] days = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        calendar.setTime(date);


// Specific format demanded by the StackOverflow.com question.
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd");
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            int a = today.dayOfWeek().get();
            int dayCount = a + i;
            if (a + i >= 7) {
                dayCount = a + i - 7;
            }
                arrayList.add(formatter.print(today.plusDays(i)) + "\n" + days[dayCount]);

        }
        return arrayList;
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) daysTab.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTextSize(18);

                }
            }
        }
    }
}