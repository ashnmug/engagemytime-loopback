package com.engagemytime.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.engagemytime.app.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

public class ScrollingActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = ScrollingActivity.this;
        setContentView(R.layout.content_scrolling);
        PhotoView photoView = (PhotoView) findViewById(R.id.photo_view);

        String image = getIntent().getStringExtra("image");
        Picasso.with(context)
                .load(image).placeholder(R.drawable.error).error(R.drawable.error)
                .into(photoView);
    }
}
