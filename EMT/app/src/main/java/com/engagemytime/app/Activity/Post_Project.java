package com.engagemytime.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.engagemytime.app.Fragments.Post_Project_1;
import com.engagemytime.app.Fragments.Post_Project_2;
import com.engagemytime.app.Fragments.Post_Project_3;
import com.engagemytime.app.Fragments.Post_Project_4;
import com.engagemytime.app.Model.ChangeNotify;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;
/**
 * Created by Casa Curo on 9/15/2017.
 */

public class Post_Project extends BaseActivity implements ChangeNotify {
    View view;
    private String[] title = {"Step 1", "Step 2", "Step 3", "Step 4"};
    private  Fragment fragment[];
    private  FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    public static final int STEP_1 = 0;

    public static final int STEP_2 = STEP_1 + 1;

    public static final int STEP_3 = STEP_2 + 1;

    public static final int STEP_4 = STEP_3 + 1;

    public static final int FRAGMENT_COUNT = STEP_4 + 1;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_project);

        context = Post_Project.this;
        initFragments();
    }

    @Override
    public void changeFragment(int pos) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment[pos]);
        fragmentTransaction.commit();
        setCurTitle(pos);

    }

    @Override
    public void changeFragment(int pos, String tag) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment[pos]);

        fragmentTransaction.commit();


        setCurTitle(pos);
    }

    @Override
    public void showHomeFragment() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment[STEP_1]);
        ((Post_Project_1) fragment[STEP_1]).setChangeNotifyListener(this);
        ((Post_Project_2) fragment[STEP_2]).setChangeNotifyListener(this);
        ((Post_Project_3) fragment[STEP_3]).setChangeNotifyListener(this);
        ((Post_Project_4) fragment[STEP_4]).setChangeNotifyListener(this);
        fragmentTransaction.commit();
        setCurTitle(0);
    }

    public void setCurTitle(int pos) {
        initToolBar(title[pos]);
    }

    private void initFragments() {
        fragment = new Fragment[FRAGMENT_COUNT];
        fragment[STEP_1] = new Post_Project_1();
        fragment[STEP_2] = new Post_Project_2();
        fragment[STEP_3] = new Post_Project_3();
        fragment[STEP_4] = new Post_Project_4();

        showHomeFragment();

    }


}
