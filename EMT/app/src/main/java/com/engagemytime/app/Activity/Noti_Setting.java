package com.engagemytime.app.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;

public class Noti_Setting extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_notification);
        initToolBar("Notification Setting");
        Button notification_setting = (Button) findViewById(R.id.notification_setting);
        notification_setting.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, Noti_Setting_2.class);
        startActivity(intent);
    }
}
