package com.engagemytime.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.engagemytime.app.Model.ChangeNotify;
import com.engagemytime.app.R;

/**
 * Created by Casa Curo on 9/16/2017.
 */

public class Post_Project_4  extends Fragment implements View.OnClickListener {
    View view;
    Button notification_setting;
    private ChangeNotify changeNotifyListener;
    public void setChangeNotifyListener(ChangeNotify changeNotifyListener) {
        this.changeNotifyListener = changeNotifyListener;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_post_project_4, container, false);

        return view;
    }

    @Override
    public void onClick(View view) {
// write code here

    }
}