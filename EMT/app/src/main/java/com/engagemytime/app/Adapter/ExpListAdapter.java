package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ExpListAdapter extends RecyclerView.Adapter<ExpListAdapter.MyViewHolder> {


    private final ArrayList<Container> list;
    private onRecyclerViewItemClickListner itemClickListner;
    private onRecyclerViewItemLongClickListner itemLongClickListner;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView title, comp, exp, location, desc;
        LinearLayout card;

        public MyViewHolder(View view) {
            super(view);
            card = (LinearLayout) view.findViewById(R.id.card);
            title = (TextView) view.findViewById(R.id.titleBox);
            comp = (TextView) view.findViewById(R.id.comp);
            exp = (TextView) view.findViewById(R.id.exp);
            desc = (TextView) view.findViewById(R.id.desc);
            location = (TextView) view.findViewById(R.id.location);
            card.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListner != null) {
                itemClickListner.onItemClickListner(view, getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (itemLongClickListner != null) {
                itemLongClickListner.onItemLongClickListner(view, getAdapterPosition());

            }
            return true;

        }
    }

    public void setOnItemClickListner(onRecyclerViewItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public interface onRecyclerViewItemClickListner {
        void onItemClickListner(View view, int position);
    }

    public interface onRecyclerViewItemLongClickListner {
        void onItemLongClickListner(View view, int position);
    }

    public void setOnItemLongClickListner(onRecyclerViewItemLongClickListner itemClickListner) {
        this.itemLongClickListner = itemClickListner;
    }

    public ExpListAdapter(ArrayList<Container> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exp_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container movie = list.get(position);
        holder.title.setText(movie.getTitle());
        holder.location.setText(movie.getAddress_details());
        holder.comp.setText(movie.getCompany());
        holder.exp.setText(movie.getExp());
        holder.desc.setText(movie.getComment());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
