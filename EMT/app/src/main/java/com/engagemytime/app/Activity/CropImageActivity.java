package com.engagemytime.app.Activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;

import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CropImageActivity extends BaseActivity {
    public String urlImageString;
    public Uri imageUrl;
    private CropImageView mCropView;
    Context context;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imageTool) {

            mCropView.startCrop(null, new CropCallback() {
                @Override
                public void onSuccess(Bitmap cropped) {
                    SessionManager sessionManager = new SessionManager(context);
                    sessionManager.saveUserPicture(SessionManager.encodeTobase64(cropped));

                }

                @Override
                public void onError() {
                    //  write errror code

                }
            }, new SaveCallback() {
                @Override
                public void onSuccess(Uri outputUri) {
                    //  write success code
                }

                @Override
                public void onError() {
                    //  write errror code

                }
            });
            setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        initToolBar("Crop Image");
        mCropView = findViewById(R.id.cropImageView);

        setOnClickListener(R.id.imageTool);
        context = CropImageActivity.this;

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getImage();
    }

    private void getImage() {
        try {
            urlImageString = getIntent().getStringExtra("type");
            Uri stringUri = Uri.parse(getIntent().getStringExtra("type"));
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getApplicationContext().getContentResolver(), stringUri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageUrl = stringUri;

            mCropView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
