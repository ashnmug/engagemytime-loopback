package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.engagemytime.app.Activity.Employee_details;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.MyViewHolder> {

    private final Context context;
    private final ArrayList<Container> list;
    private onRecyclerViewItemClickListner itemClickListner;
    private onRecyclerViewItemLongClickListner itemLongClickListner;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView title, exp, rating;
        ImageView image;
        CardView cardView;
        RelativeLayout card;

        public MyViewHolder(View view) {
            super(view);
            exp = (TextView) view.findViewById(R.id.exp);
            rating = (TextView) view.findViewById(R.id.ratingText);
            title = (TextView) view.findViewById(R.id.title);
            image = (ImageView) view.findViewById(R.id.image);
            card = (RelativeLayout) view.findViewById(R.id.card);
            cardView = (CardView) view.findViewById(R.id.card1);
            cardView.setOnClickListener(this);
            cardView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListner != null) {
                itemClickListner.onItemClickListner(view, getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (itemLongClickListner != null) {
                itemLongClickListner.onItemLongClickListner(v, getAdapterPosition());
            }
            return true;
        }
    }

    public interface onRecyclerViewItemClickListner {
        void onItemClickListner(View view, int position);
    }

    public interface onRecyclerViewItemLongClickListner {
        void onItemLongClickListner(View view, int position);
    }

    public void setOnItemClickListner(EmployeeAdapter.onRecyclerViewItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public void setOnItemLongClickListner(EmployeeAdapter.onRecyclerViewItemLongClickListner itemClickListner) {
        this.itemLongClickListner = itemClickListner;
    }

    public EmployeeAdapter(Context context, ArrayList<Container> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final int position) {
        final Container movie = list.get(position);
        holder.title.setText(movie.getTitle());
        holder.exp.setText(movie.getExp());
        holder.rating.setText(movie.getRating());
        if (movie.getGender().equals("Female") || movie.getGender().equals("female")) {
            Picasso.with(context)
                    .load(movie.getImage()).placeholder(R.drawable.femaleprofile).error(R.drawable.femaleprofile)
                    .into(holder.image);
        } else {
            Picasso.with(context)
                    .load(movie.getImage()).placeholder(R.drawable.maleprofile).error(R.drawable.maleprofile)
                    .into(holder.image);
        }


        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Employee_details.class);
                intent.putExtra("pid", movie.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
