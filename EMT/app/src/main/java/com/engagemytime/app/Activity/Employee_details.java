package com.engagemytime.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.engagemytime.app.Adapter.Emp_DetAdapter;
import com.engagemytime.app.Fragments.Calender_Week;
import com.engagemytime.app.Fragments.Contact;
import com.engagemytime.app.Fragments.Group;
import com.engagemytime.app.Fragments.Personal;
import com.engagemytime.app.Fragments.Review;
import com.engagemytime.app.Fragments.Skill_info;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Employee_details extends BaseActivity implements TabLayout.OnTabSelectedListener, HttpProcessor.HttpResponser {
    ViewPager viewPager;
    TabLayout tabLayout;
    Emp_DetAdapter viewPagerAdapter;
    Context context;
    FloatingActionButton assign;
    private SessionManager sessionManager;
    String id = "";
    private String PERSONAL = "Personal";
    private String SKILL = "Skill";
    private String CONTACT = "Contact";
    private String REVIEW = "Review";
    private String CALENDAR_S = "Calender";
    private String GROUP = "Group";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_project);
        context = Employee_details.this;
        id = getIntent().getStringExtra("pid");

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        assign = (FloatingActionButton) findViewById(R.id.assign);
        sessionManager = new SessionManager(context);

        getAccess();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 2) {
            Toast.makeText(context, "Project Added", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
// write code here
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
// write code here

    }

    private void getAccess() {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("personId", id);
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "accessControls?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_ACCESS");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String results, String TAG) {
        try {
            switch (TAG) {
                case "GET_ACCESS":
                    if (results.equals("[]")) {
                        getFullTab();
                    } else {
                        JSONArray jsonArrayCont = new JSONArray(results);
                        JSONObject jsonObject = new JSONObject();
                        for (int a = 0; a < jsonArrayCont.length(); a++) {
                            jsonObject = jsonArrayCont.getJSONObject(a);

                        }
                        checkSwitch(jsonObject);
                    }

                    break;
                case "UPDATE_ACCESS":

                    getAccess();


                    break;
                default:
            }
        } catch (Exception e) {
            getFullTab();
        }
    }

    private void getFullTab() {

        tabLayout.addTab(tabLayout.newTab().setText(PERSONAL));
        tabLayout.addTab(tabLayout.newTab().setText(CONTACT));
        tabLayout.addTab(tabLayout.newTab().setText(SKILL));
        tabLayout.addTab(tabLayout.newTab().setText(CALENDAR_S));
        tabLayout.addTab(tabLayout.newTab().setText(GROUP));
        tabLayout.addTab(tabLayout.newTab().setText(REVIEW));
        ArrayList<Fragment> arrayList = new ArrayList<>();
        arrayList.add(new Personal());
        arrayList.add(new Contact());
        arrayList.add(new Skill_info());
        arrayList.add(new Calender_Week());
        arrayList.add(new Group());
        arrayList.add(new Review());
        setPager(tabLayout, arrayList);
    }

    private void checkSwitch(JSONObject jsonObject) {
        ArrayList<Fragment> arrayList = new ArrayList<>();
        boolean accessPerson = jsonObject.optBoolean("access_to_person");
        boolean accessProfile = jsonObject.optBoolean("access_on_profile");
        boolean accessContact = jsonObject.optBoolean("access_on_contact");
        boolean accessJob = jsonObject.optBoolean("access_on_job");
        boolean accessCal = jsonObject.optBoolean("access_on_calendar");
        boolean accessProj = jsonObject.optBoolean("access_on_project");
        boolean accessAcc = jsonObject.optBoolean("access_on_account");
        if (accessProfile) {

            tabLayout.addTab(tabLayout.newTab().setText(PERSONAL));
            arrayList.add(new Personal());
        }
        if (accessPerson) {

            tabLayout.addTab(tabLayout.newTab().setText(SKILL));

            arrayList.add(new Skill_info());
        }
        if (accessContact) {

            tabLayout.addTab(tabLayout.newTab().setText(CONTACT));


            arrayList.add(new Contact());

        }
        if (accessAcc) {

            tabLayout.addTab(tabLayout.newTab().setText(REVIEW));


            arrayList.add(new Review());
        }
        if (accessCal) {

            tabLayout.addTab(tabLayout.newTab().setText(CALENDAR_S));

            arrayList.add(new Calender_Week());
        }
        if (accessJob) {
            tabLayout.addTab(tabLayout.newTab().setText(GROUP));
            arrayList.add(new Group());
        }

        if (accessProj) {
// waiting
        }


        setPager(tabLayout, arrayList);
    }

    private void setPager(TabLayout tabLayout, ArrayList<Fragment> arrayList) {
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPagerAdapter = new Emp_DetAdapter(getSupportFragmentManager(), arrayList);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        assign.setOnClickListener(this);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
// not in use
    }
}

