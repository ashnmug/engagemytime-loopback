package com.engagemytime.app.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.engagemytime.app.Activity.ProjectDetails;
import com.engagemytime.app.Adapter.EmployeeAdapter;
import com.engagemytime.app.Adapter.ProjectAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Casa Curo on 9/14/2017.
 */

public class History extends Fragment implements HttpProcessor.HttpResponser {
    View view;
    private RecyclerView recyclerProject;
    private ArrayList<Container> projectList;
    private ProjectAdapter projectAdapter;
    private Context context;
    SessionManager sessionManager;
    Integer integerP[] = {R.drawable.icon1, R.drawable.icon2, R.drawable.icon3, R.drawable.icon4, R.drawable.icon5, R.drawable.icon6};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_my_project, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        recyclerProject = (RecyclerView) view.findViewById(R.id.recyclerProject);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        recyclerProject.setLayoutManager(gridLayoutManager);
        getProjectList();
        return view;
    }
    private void getProjectList() {

        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("project_status", "Completed");
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "ProjectProfiles?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_PROJECT");
        httpProcessor.setHttpResponserListener(this);
    }

    private void gettProjectDetails(JSONArray jsonArray) {
        try {
            projectList = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                JSONObject jsonObject = jsonArray.getJSONObject(a);

                Container container = new Container();
                container.setTitle(jsonObject.optString("project_name"));
                container.setAddress_details(jsonObject.optString("project_details"));
                container.setStatus(jsonObject.optString("project_status"));
                container.setName(jsonObject.optString("project_lead"));
                container.setId(jsonObject.optString("id"));
                container.setPrice("$ " + a + 1 + "00");
                container.setImageResource(integerP[a]);


                projectList.add(container);
            }
        } catch (Exception ignored) {

        }
        projectAdapter = new ProjectAdapter( projectList);
        recyclerProject.setAdapter(projectAdapter);
        projectAdapter.notifyDataSetChanged();
        onClickAdapter();
    }


    private void onClickAdapter() {
        projectAdapter.setOnItemClickListner(new ProjectAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.card) {
                    Intent intent = new Intent(context, ProjectDetails.class);
                    intent.putExtra("pid", projectList.get(position).getId());
                    intent.putExtra("title", projectList.get(position).getTitle());
                    intent.putExtra("status", projectList.get(position).getStatus());
                    intent.putExtra("desc", projectList.get(position).getAddress_details());
                    intent.putExtra("lead", projectList.get(position).getName());
                    // intent.putExtra("array", projectList.get(position).getJsonArray().toString());
                    startActivityForResult(intent, 2);
                }
            }
        });
    }


    @Override
    public void responseResult(String result, String TAG) {
        try {
            switch (TAG) {
                case "GET_PROJECT":
                    JSONArray jsonArray1 = new JSONArray(result);
                    gettProjectDetails(jsonArray1);
                    break;

                default:
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
