package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.engagemytime.app.Activity.Group_Details;
import com.engagemytime.app.Activity.Job_Details;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;


public class GroupAdaper extends RecyclerView.Adapter<GroupAdaper.MyViewHolder> {

    private final Context context;
    private final ArrayList<Container> list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, textViewDate, date,member;
        TextView status;
        ImageView image;
        LinearLayout card;

        public MyViewHolder(View view) {
            super(view);
            status = (TextView) view.findViewById(R.id.desc);
            member = (TextView) view.findViewById(R.id.member);
            title = (TextView) view.findViewById(R.id.titleBox);
            card = (LinearLayout) view.findViewById(R.id.card);


        }
    }


    public GroupAdaper(Context context, ArrayList<Container> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grp_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container places = list.get(position);


        holder.title.setText(places.getTitle());
        holder.status.setText(places.getStatus());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Group_Details.class);
                intent.putExtra("title",places.getTitle());
                intent.putExtra("desc",places.getStatus());
                intent.putExtra("id",places.getId());
                intent.putExtra("groupid",places.getGrpId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
