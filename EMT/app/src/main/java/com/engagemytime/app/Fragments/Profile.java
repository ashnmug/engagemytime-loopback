package com.engagemytime.app.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.engagemytime.app.Adapter.ProfileAdapter;
import com.engagemytime.app.R;
import com.github.clans.fab.FloatingActionButton;

/**
 * Created by Casa Curo on 9/15/2017.
 */

public class Profile extends Fragment implements TabLayout.OnTabSelectedListener {
    View view;
    ViewPager viewPager;
    ProfileAdapter viewPagerAdapter;
    TabLayout tabLayout;
    FloatingActionButton assign;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_my_project, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        assign = (FloatingActionButton) view.findViewById(R.id.assign);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout.addTab(tabLayout.newTab().setText("Personal"));
        tabLayout.addTab(tabLayout.newTab().setText("Contact"));
        tabLayout.addTab(tabLayout.newTab().setText("Skill"));
        tabLayout.addTab(tabLayout.newTab().setText("Access"));
        tabLayout.addTab(tabLayout.newTab().setText("Calender"));
        tabLayout.addTab(tabLayout.newTab().setText("Group"));
        tabLayout.addTab(tabLayout.newTab().setText("Review"));
        context = getActivity();
        assign.setVisibility(View.GONE);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        viewPagerAdapter = new ProfileAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        return view;
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
      }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

// write code here
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

// write code here
    }
}

