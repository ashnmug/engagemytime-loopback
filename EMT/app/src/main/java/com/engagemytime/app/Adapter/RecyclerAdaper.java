package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.engagemytime.app.Activity.Sign_Up;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerAdaper extends RecyclerView.Adapter<RecyclerAdaper.MyViewHolder> {

    private final Context context;
    private final ArrayList<Container> list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, details, date;
        ImageView image;
        RelativeLayout relative;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            details = (TextView) view.findViewById(R.id.details);
            image = (ImageView) view.findViewById(R.id.image);
            relative = (RelativeLayout) view.findViewById(R.id.relative);


        }
    }


    public RecyclerAdaper(Context context, ArrayList<Container> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container places = list.get(position);
        holder.title.setText(places.getTitle());
        holder.details.setText(places.getAddress_details());
        Picasso.with(context)
                .load(places.getImage()).placeholder(R.drawable.select_cur_loc)
                .into(holder.image);
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("address", places.getAddress_details());
                returnIntent.putExtra("result", String.valueOf(places.getLat()) + "," + String.valueOf(places.getLng()));
                ((Activity) context).setResult(Activity.RESULT_OK, returnIntent);
                ((Activity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
