package com.engagemytime.app.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.engagemytime.app.Adapter.GroupAdaper;
import com.engagemytime.app.Adapter.ReviewAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Review extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser {
    View view;

    private ReviewAdapter reviewAdapter;
    MaterialRatingBar rating;
    private Context context;
    private ArrayList<Container> grpList;
    private RecyclerView recycler;
    private SessionManager sessionManager;
    EditText title, status;
    FloatingActionButton fab;
    Button submit;
    String id;
    private JSONObject jsonObject;
    ImageView cancel;
    CardView cardView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_group, container, false);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        context = getActivity();
        sessionManager = new SessionManager(context);
        id = getActivity().getIntent().getStringExtra("pid");
        title = (EditText) view.findViewById(R.id.title);
        rating = (MaterialRatingBar) view.findViewById(R.id.rating);
        rating.setVisibility(View.VISIBLE);
        cardView = (CardView) view.findViewById(R.id.cardView);
        status = (EditText) view.findViewById(R.id.details);
        title.setHint("Credit");
        status.setHint("Comment");
        submit = (Button) view.findViewById(R.id.submit);
        cancel = (ImageView) view.findViewById(R.id.cancel);
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        fab.setOnClickListener(this);
        cancel.setOnClickListener(this);
        submit.setOnClickListener(this);
        if (sessionManager.getKeyUserId().equals(id) || id == null) {
            fab.setVisibility(View.GONE);
            id = sessionManager.getKeyUserId();
        } else {
            fab.setVisibility(View.VISIBLE);

        }
        getReviews();
        return view;

    }

    private void getREVIEWDetails(JSONArray jsonArray) {
        try {
            grpList = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                JSONObject jsonObject = jsonArray.getJSONObject(a);
                JSONArray jsonArray1 = jsonObject.getJSONArray("comments");
                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                Container container = new Container();
                container.setTitle(jsonObject.optString("credit"));
                container.setComment(jsonObject1.optString("comment"));
                container.setRating(jsonObject.optString("stars"));
                grpList.add(container);
            }
        } catch (Exception ignored) {

        }
        reviewAdapter = new ReviewAdapter(grpList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(reviewAdapter);
        reviewAdapter.notifyDataSetChanged();
    }

    private void getReviews() {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("personId", id);
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "RatingProfiles?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_REVIEW");
        httpProcessor.setHttpResponserListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {
            JSONObject jsonObject = convJsonData();
            if (jsonObject != null) {
                addReview(jsonObject);
            }
        } else if (v.getId() == R.id.cancel) {

            cardView.setVisibility(View.GONE);

        } else if (v.getId() == R.id.fab) {

            cardView.setVisibility(View.VISIBLE);

        }
    }

    private JSONObject convJsonData() {
        this.jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {

            this.jsonObject.put("personId", id);
            this.jsonObject.put("credit", title.getText().toString());
            this.jsonObject.put("stars", rating.getRating());
            jsonObject.put("comment", status.getText().toString());
            jsonArray.put(jsonObject);
            this.jsonObject.put("comments", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this.jsonObject;
    }

    private void addReview(JSONObject jsonObject) {

        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "RatingProfiles/?access_token=" + sessionManager.getAccessToken(), HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("ADD_REVIEW");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void responseResult(String result, String TAG) {
        try {
            switch (TAG) {
                case "GET_REVIEW":
                    JSONArray jsonArrayCont = new JSONArray(result);


                    getREVIEWDetails(jsonArrayCont);

                    break;
                case "ADD_REVIEW":
                    getReviews();
                    cardView.setVisibility(View.GONE);
                    break;

                default:

            }
        } catch (Exception e) {

        }
    }
}
