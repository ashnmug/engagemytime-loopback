package com.engagemytime.app.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.engagemytime.app.Adapter.EventAdaper;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.engagemytime.app.utilities.API.getFrmtdDate;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Calender_new extends Fragment implements TabLayout.OnTabSelectedListener, View.OnClickListener, HttpProcessor.HttpResponser {
    View view;
    private Context context;
    TabLayout daysTab;
    ArrayList<Container> eventList;
    ArrayList<Container> weekDays;
    RecyclerView recycle;
    FloatingActionButton fab;
    String[] strings = {"private", "group", "public", "public", "group", "friend", "private", "friend"};
    private ArrayList<Container> fullArrayList = null;
    private Dialog dialog;
    private EditText event_desc;
    private EditText event_edate;
    private EditText event_sdate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.content_push__access, container, false);
        context = getActivity();
        daysTab = (TabLayout) view.findViewById(R.id.tabLayout);
        recycle = (RecyclerView) view.findViewById(R.id.recycle);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        weekDays = new ArrayList<>();
        weekDays = getTabDaysList();
        fab.setOnClickListener(this);
        daysTab.setTabMode(TabLayout.MODE_SCROLLABLE);
        for (int i = 0; i < 7; i++) {
            daysTab.addTab(daysTab.newTab().setText(weekDays.get(i).getDay()));
        }

        changeTabsFont();

        daysTab.addOnTabSelectedListener(this);
        getEventFromServer();
        return view;
    }

    private void getEvent(ArrayList<Container> arrayList) {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        recycle.setLayoutManager(mLayoutManager);
        EventAdaper eventAdaper = new EventAdaper(context, arrayList);
        recycle.setAdapter(eventAdaper);
        eventAdaper.notifyDataSetChanged();
    }

    private ArrayList<Container> getTabDaysList() {

        DateTime today = new DateTime().withTimeAtStartOfDay();
        Calendar calendar = Calendar.getInstance();

        Date date = new Date();
        String[] days = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        calendar.setTime(date);


// Specific format demanded by the StackOverflow.com question.
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd");
        DateTimeFormatter fullDate = DateTimeFormat.forPattern("yyyy-MM-dd");

        ArrayList<Container> arrayList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            Container container = new Container();
            int a = today.dayOfWeek().get();
            int dayCount = a + i;
            if (a + i >= 7) {
                dayCount = a + i - 7;
                container.setDay(formatter.print(today.plusDays(i)) + "\n" + days[dayCount]);
            } else {
                container.setDay(formatter.print(today.plusDays(i)) + "\n" + days[dayCount]);
            }
            container.setDate(fullDate.print(today.plusDays(i)));
            arrayList.add(container);
        }
        return arrayList;
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) daysTab.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTextSize(18);

                }
            }
        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        checkDateEvent(tab.getPosition());
    }

    private void checkDateEvent(int pos) {
        weekDays = getTabDaysList();
        if (fullArrayList != null) {
            ArrayList<Container> arrayList = new ArrayList<>();
            for (int i = 0; i < fullArrayList.size(); i++) {
                if (fullArrayList.get(i).getStart().contains(weekDays.get(pos).getDate())) {
                    arrayList.add(fullArrayList.get(i));
                }
            }
            getEvent(arrayList);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

// write code
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

// write code
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab) {
            showDialog();

        } else if (view.getId() == R.id.btn_submit) {
            addJsonEvent(event_sdate.getText().toString(), event_edate.getText().toString(), event_desc.getText().toString());
        } else if (view.getId() == R.id.event_edate) {
            showDateTimePicker(event_edate);
        } else if (view.getId() == R.id.event_sdate) {
            showDateTimePicker(event_sdate);
        } else if (view.getId() == R.id.cancel) {
            dialog.dismiss();
        }
    }

    private void showDateTimePicker(final EditText editText) {

        LocalDate localDate = LocalDate.now();
        int month = localDate.getMonthOfYear();
        int year = localDate.getYear();
        int day = localDate.getDayOfMonth();
        final SwitchDateTimeDialogFragment dateTimeDialogFragment = SwitchDateTimeDialogFragment.newInstance(
                "Title example",
                "OK",
                "Cancel"
        );

// Assign values
        dateTimeDialogFragment.startAtCalendarView();
        dateTimeDialogFragment.set24HoursMode(true);
        dateTimeDialogFragment.setMinimumDateTime(new GregorianCalendar(year, month - 1, day).getTime());
        dateTimeDialogFragment.setMaximumDateTime(new GregorianCalendar(year, month - 1, day + 7).getTime());
        dateTimeDialogFragment.setDefaultDateTime(new GregorianCalendar(year, month - 1, day, 0, 0).getTime());

        try {
            dateTimeDialogFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            //write code here
        }

// Set listener
        dateTimeDialogFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                // Date is get on positive button click
                // Do something
                String dateString = date.toString();
                String newstring = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(date);

                editText.setText(newstring);
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

// Show
        dateTimeDialogFragment.show(getActivity().getSupportFragmentManager(), "dialog_time");
    }

    private void addJsonEvent(String sdate, String edate, String desc) {
        if (!sdate.equals("") || !edate.equals("") || !desc.equals("")) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("collection", "/api/v1/calendar/1/");
                jsonObject.put("dtend", edate);
                jsonObject.put("dtstamp", "2018-01-03T06:29:30");
                jsonObject.put("dtstart", sdate);
                jsonObject.put("owner", "/api/v1/user/sandeep/");
                jsonObject.put("summary", desc);
                jsonObject.put("participant", "/api/v1/user/sandeep/");
                jsonObject.put("tzid", "Asia/Kolkata");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            addEventToServer(jsonObject);
        }
    }

    private void addEventToServer(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL_CAL + "event/?key=JvHvlP8oqqlc0hHQq49SalyB5lQpaIgfyM5wKPMP", HttpProcessor.HEADER_POST, requestBody);
        httpProcessor.executeRequest("ADD_EVENT");
        httpProcessor.setHttpResponserListener(this);

    }

    private void getEventFromServer() {
        RequestBody requestBody = new FormEncodingBuilder().build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL_CAL + "event/?key=JvHvlP8oqqlc0hHQq49SalyB5lQpaIgfyM5wKPMP", HttpProcessor.HEADER_GET, requestBody);
        httpProcessor.executeRequest("GET_EVENT");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String result, String TAG) {
        if (TAG.equals("ADD_EVENT")) {

            try {
                JSONObject addObject = new JSONObject(result);
                Toast.makeText(context, addObject.optString("error_message"), Toast.LENGTH_LONG).show();

            } catch (Exception ignored) {
                Toast.makeText(context, "Event Added", Toast.LENGTH_LONG).show();
                // write code here
            }
            dialog.cancel();
            getEventFromServer();
        } else if (TAG.equals("GET_EVENT")) {
            try {

                JSONObject getObject = new JSONObject(result);
                fullArrayList = new ArrayList<>();
                JSONArray getObjectArray = getObject.optJSONArray("objects");
                for (int i = 0; i < getObjectArray.length(); i++) {
                    Container container = new Container();
                    JSONObject jsonObject = getObjectArray.optJSONObject(i);
                    container.setEnd(jsonObject.optString("dtend"));
                    container.setStart(jsonObject.optString("dtstart"));
                    container.setTitle(jsonObject.optString("summary"));
                    container.setType("public");
                    fullArrayList.add(container);
                }
                checkDateEvent(0);
            } catch (Exception ignored) {
                // write code here
            }
        }
    }

    public void showDialog() {
        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.create_event);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        event_desc = (EditText) dialog.findViewById(R.id.event_desc);
        event_edate = (EditText) dialog.findViewById(R.id.event_edate);
        event_sdate = (EditText) dialog.findViewById(R.id.event_sdate);

        Button submit = (Button) dialog.findViewById(R.id.btn_submit);
        ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);

        cancel.setOnClickListener(this);
        event_edate.setOnClickListener(this);
        event_sdate.setOnClickListener(this);

        submit.setOnClickListener(this);

        dialog.show();

    }
}