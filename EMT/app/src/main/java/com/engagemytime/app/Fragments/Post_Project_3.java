package com.engagemytime.app.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.engagemytime.app.Activity.Post_Project;
import com.engagemytime.app.Adapter.SkillAdapter;
import com.engagemytime.app.Model.ChangeNotify;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Casa Curo on 9/16/2017.
 */

public class Post_Project_3 extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser {
    View view;
    Button next,back;
    private ChangeNotify changeNotifyListener;
    Integer integer[] = {R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html, R.drawable.php, R.drawable.java, R.drawable.html};
    String string[] = {"PHP Developer", "Java Developer", "UI Developer", "Jr. PHP Developer ", "Jr. Java Developer", "Jr. UI Developer"};
    private RecyclerView recyclerView;
    private ArrayList<Container> arrayList, viewMoreSkill, employeeList;
    private SkillAdapter skillAdapter;
    public  String skill;
    private JSONObject jsonObject;
    String id = "";
    private SessionManager sessionManager;

    public void setChangeNotifyListener(ChangeNotify changeNotifyListener) {
        this.changeNotifyListener = changeNotifyListener;
    }

    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_post_project_3, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerSkill);

        context = getActivity();
        sessionManager = new SessionManager(context);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 4);
        id = getActivity().getIntent().getStringExtra("pid");
        recyclerView.setLayoutManager(gridLayoutManager);
        next = (Button) view.findViewById(R.id.submit_project);
        back = (Button) view.findViewById(R.id.back);
        back.setOnClickListener(this);
        next.setOnClickListener(this);
        getDummyList();
        return view;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.submit_project) {
            skill = "Java Developer";
            if (null != changeNotifyListener) {
                JSONObject jsonObject = convJsonData();
                addGroup(jsonObject);
                //changeNotifyListener.changeFragment(Post_Project.STEP_4, "Step 4");
            }
        }
    }

    private JSONObject convJsonData() {
        this.jsonObject = new JSONObject();
        JSONArray jsonObject = new JSONArray();
        try {

            this.jsonObject.put("project_lead", sessionManager.getTeamLead());
            this.jsonObject.put("person_id", sessionManager.getKeyUserId());
            this.jsonObject.put("project_name", sessionManager.getProjectName());
            this.jsonObject.put("project_details", sessionManager.getProDesc());
            this.jsonObject.put("project_status", "New");
            jsonObject.put(sessionManager.getTeamLead());
            this.jsonObject.put("list_of_person", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this.jsonObject;
    }

    private void addGroup(JSONObject jsonObject) {

        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "ProjectProfiles/?access_token=" + sessionManager.getAccessToken(), HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("ADD_PROJECT");
        httpProcessor.setHttpResponserListener(this);
    }

    private void getDummyList() {
        arrayList = new ArrayList<>();
        viewMoreSkill = new ArrayList<>();
        for (int a = 0; a < 5; a++) {
            Container container = new Container();


            container.setTitle(string[a]);

            container.setImageResource(integer[a]);
            arrayList.add(container);

        }
        skillAdapter = new SkillAdapter(context,arrayList);
        recyclerView.setAdapter(skillAdapter);
        skillAdapter.notifyDataSetChanged();
        skillAdapter.setOnItemClickListner(new SkillAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.image) {
                    skill = arrayList.get(position).getTitle();
                }
            }
        });

    }

    @Override
    public void responseResult(String result, String TAG) {
        try {
            switch (TAG) {

                case "ADD_PROJECT":
                    getActivity().finish();
                    break;

                default:
            }
        } catch (Exception e) {

        }
    }
}