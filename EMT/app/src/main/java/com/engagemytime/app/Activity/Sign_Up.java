package com.engagemytime.app.Activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.hbb20.CountryCodePicker;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Sign_Up extends BaseActivity implements HttpProcessor.HttpResponser, CompoundButton.OnCheckedChangeListener {
    private Context context;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private String userName, m_name, l_name, password, email, mobileNumber, address;
    String[] resultsLat;
    JSONObject jsonObject;
    RadioButton consumer, provider;
    String role = "Consumer";
    private SessionManager sessionManager;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
     private int a = 0;
    private String token;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {

            userName = getEditText(R.id.username);
            password = getEditText(R.id.password);
            m_name = getEditText(R.id.m_name);
            l_name = getEditText(R.id.l_name);
            CountryCodePicker c_code = (CountryCodePicker) findViewById(R.id.c_code);
            mobileNumber = c_code.getFullNumberWithPlus() + getEditText(R.id.mobileNumber);
            email = getEditText(R.id.email);
            if (userName.equals("") || password.equals("") || mobileNumber.equals("") || email.equals("")) {
                AlertDialogLogOut("Field should not be empty");
            } else if (password.length() < 6 || !isValidPassword(password)) {
                AlertDialogLogOut("Password must contain one special character");
                setError(R.id.password, "Password must contain one special character");
            } else if (!email.matches(emailPattern)) {
                AlertDialogLogOut("Please enter valid email id");
                setError(R.id.email, "Please enter valid email id");

            } else if (getEditText(R.id.mobileNumber).length() != 10) {
                AlertDialogLogOut("Please enter valid mobile number");
                setError(R.id.mobileNumber, "Please enter valid mobile number");

            } else {
                convJsonData();

            }
        } else if (v.getId() == R.id.verify) {
            ViewDialog viewDialog = new ViewDialog();
            viewDialog.showDialog(context);
        }

    }

    private void AlertDialogLogOut(String string) {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Error !!!")
                .setContentText(string).show();
    }

    private void signUpServer(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("sign_up");
        httpProcessor.setHttpResponserListener(this);

    }


    @Override
    public void responseResult(String result, String TAG) {
        try {

            switch (TAG) {
                case "sign_up":
                    JSONObject results = new JSONObject(result);
                    if (results.optString("title").equals("Signed up successfully")) {

                        sessionManager.setVCode(results.optString("vcode"));
                        sessionManager.saveCredentials(userName, results.optString("userId"), email, mobileNumber);
                        Toast.makeText(context, results.optString("content"), Toast.LENGTH_SHORT).show();
                        JSONObject tokeObject = results.optJSONObject("redirect");
                        token = tokeObject.optString("token");
                        sessionManager.setAccessToken(token);
                        ViewDialog viewDialog = new ViewDialog();
                        viewDialog.showDialog(context);
                    } else {
                        JSONObject error = results.optJSONObject("error");
                        Toast.makeText(context, error.optString("message"), Toast.LENGTH_SHORT).show();
                    }

                    break;
                case "CONFIRM":

                    View_Success_Dialog viewDialog = new View_Success_Dialog();
                    viewDialog.showDialog(context);


                    break;
                default:

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void CONFIRM(String token) {
        RequestBody requestBody = new FormEncodingBuilder()

                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/confirm?uid=" + sessionManager.getKeyUserId() + "&token=" + token, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("CONFIRM");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign__up);
        initToolBar("Sign Up");

        consumer = (RadioButton) findViewById(R.id.consumer);
        provider = (RadioButton) findViewById(R.id.provider);
        setOnClickListener(R.id.submit);
        setOnClickListener(R.id.verify);
        consumer.setOnCheckedChangeListener(this);
        provider.setOnCheckedChangeListener(this);
        context = Sign_Up.this;
        sessionManager = new SessionManager(context);
        resultsLat = getIntent().getStringExtra("result").split(",");
        latitude = Double.parseDouble(resultsLat[0]);
        longitude = Double.parseDouble(resultsLat[1]);
        address = getIntent().getStringExtra("address");
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String P_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(P_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public int getRandomNumber(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private void convJsonData() {
        a = getRandomNumber(111111, 999999);
        Container container = new Container();
        String string = container.getFcmDeviceId();
        jsonObject = new JSONObject();
        JSONObject jsonObjectLoc = new JSONObject();
        try {
            jsonObjectLoc.put("lat", latitude);
            jsonObjectLoc.put("lng", longitude);
            jsonObject.put("map_coordinates", jsonObjectLoc);
            jsonObject.put("first_name", userName);
            jsonObject.put("middle_name", m_name);
            jsonObject.put("family_name", l_name);
            jsonObject.put("gender", "");
            jsonObject.put("age", null);
            jsonObject.put("role", role);
            jsonObject.put("dob", "2017-09-28T05:51:40.836Z");
            jsonObject.put("legal_person", null);
            jsonObject.put("natural_person", null);
            jsonObject.put("picture_of_person", "string");
            jsonObject.put("username", email);
            jsonObject.put("deviceToken", string);
            jsonObject.put("portal_address", address);
            jsonObject.put("email", email);
            jsonObject.put("password", password);
            jsonObject.put("vcode", a);
            jsonObject.put("mobile", mobileNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        signUpServer(jsonObject);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (consumer.isChecked()) {
            role = "Consumer";
        } else if (provider.isChecked()) {
            role = "Provider";
        }
    }


    private class ViewDialog {

        private void showDialog(Context activity) {


            final Dialog dialog = new Dialog(activity);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialogue);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            final TextView tv_msg = (TextView) dialog.findViewById(R.id.tv_msg);
            tv_msg.setText("Email Verification");
            final EditText otp_field = (EditText) dialog.findViewById(R.id.otp_field);
            otp_field.setHint("Enter OTP");
            otp_field.setInputType(InputType.TYPE_CLASS_NUMBER);
            ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
            Button dialogButton = (Button) dialog.findViewById(R.id.btn_submit);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (otp_field.getText().toString().equals(sessionManager.getVCode())) {

                        CONFIRM(sessionManager.getAccessToken());
                        dialog.dismiss();
                    }


                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    private class View_Success_Dialog {

        private void showDialog(Context activity) {


            final Dialog dialog = new Dialog(activity);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialogue_success);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button cancel = (Button) dialog.findViewById(R.id.cancel);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startNextActivity(Login.class);
                    finish();

                    dialog.dismiss();

                }
            });

            dialog.show();

        }
    }

}
