package com.engagemytime.app.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.engagemytime.app.Activity.Set_Location;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Contact extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser {
    View view;

    private Context context;
    private EditText mobile, email, skype, whatsapp;
    TextView location;
    private FloatingActionButton editImage;
    String id = "";
    private SessionManager sessionManager;
    private String address;
    private Container container;
    private String[] results;
    String cid = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.contact_info, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        mobile = (EditText) view.findViewById(R.id.mobile);
        email = (EditText) view.findViewById(R.id.email);
        skype = (EditText) view.findViewById(R.id.skype);
        whatsapp = (EditText) view.findViewById(R.id.whatsapp);
        location = (TextView) view.findViewById(R.id.location);
        editImage = (FloatingActionButton) view.findViewById(R.id.editImage);
        id = getActivity().getIntent().getStringExtra("pid");

        if (sessionManager.getKeyUserId().equals(id) || id == null) {
            editImage.setVisibility(View.VISIBLE);
            id = sessionManager.getKeyUserId();
        } else {
            editImage.setVisibility(View.GONE);

        }

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Set_Location.class);

                startActivityForResult(intent, 2);

            }
        });
        editImage.setOnClickListener(this);
        getContact();
        return view;
    }

    private void setContactDetails(JSONObject jsonObject, JSONObject jsonObjectLoc) {


        container = new Container();
        container.setEmail(jsonObject.optString("email_address"));
        container.setAddress_details(jsonObject.optString("portal_address"));
        container.setMobile(jsonObject.optString("phone_number"));
        container.setLat(Double.parseDouble(jsonObjectLoc.optString("lat")));
        container.setLng(Double.parseDouble(jsonObjectLoc.optString("lng")));
        container.setId(jsonObject.optString("id"));
        container.setWhatsApp(jsonObject.optString("whatsapp_id"));
        container.setSkype(jsonObject.optString("skype_id"));
        setText(container);

    }

    private void setText(Container container) {
        mobile.setText(container.getMobile());
        whatsapp.setText(container.getWhatsApp());
        skype.setText(container.getSkype());
        email.setText(container.getEmail());
        location.setText(container.getAddress_details());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.editImage) {

            setVisiblity();
        }
    }

    private void setVisiblity() {
        if (mobile.isEnabled()) {
            updateContactInfo();
        } else {
            mobile.setEnabled(true);
            location.setClickable(true);
            email.setEnabled(true);
            skype.setEnabled(true);
            whatsapp.setEnabled(true);
            editImage.setImageResource(R.drawable.task);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                String resultS = (String) data.getStringExtra("result");
                results = resultS.split(",");

                container.setLat(Double.parseDouble(results[0]));
                container.setLng(Double.parseDouble(results[1]));
                address = (String) data.getStringExtra("address");
                location.setText(address);
            } else if (resultCode == Activity.RESULT_CANCELED) {
//write code here
            }
        }

    }//onActivit

    private JSONObject convJsonData() {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectLoc = new JSONObject();
        try {
            jsonObjectLoc.put("lat", container.getLat());
            jsonObjectLoc.put("lng", container.getLng());
            jsonObject.put("map_coordinates", jsonObjectLoc);
            jsonObject.put("email_address", email.getText().toString());
            jsonObject.put("phone_number", mobile.getText().toString());
            jsonObject.put("skype_id", skype.getText().toString());
            jsonObject.put("whatsapp_id", whatsapp.getText().toString());
            jsonObject.put("portal_address", location.getText().toString());
            jsonObject.put("email", sessionManager.getKeyEmailId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


/////////////////////////////////API////////////////////////////////////////

    private void updateContactInfo() {
        cid = container.getId();
        JSONObject jsonObject = convJsonData();
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Contacts/" + cid + "?access_token=" + sessionManager.getAccessToken(), HttpProcessor.PATCH, requestBody);
        httpProcessor.executeRequest("UPDATE_CONTACT");
        httpProcessor.setHttpResponserListener(this);
    }

    private void getContact() {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("personId", id);
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Contacts?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_CONTACT");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String results, String TAG) {
        try {
            switch (TAG) {
                case "GET_CONTACT":

                    JSONArray jsonArrayCont = new JSONArray(results);

                    for (int a = 0; a < jsonArrayCont.length(); a++) {
                        JSONObject jsonObject = jsonArrayCont.getJSONObject(a);
                        JSONObject jsonObjectLoc = jsonObject.getJSONObject("map_coordinates");

                        setContactDetails(jsonObject, jsonObjectLoc);

                    }


                    break;
                case "UPDATE_CONTACT":

                    getContact();

                    mobile.setEnabled(false);
                    email.setEnabled(false);
                    skype.setEnabled(false);
                    whatsapp.setEnabled(false);
                    location.setClickable(false);
                    editImage.setImageResource(R.drawable.edit_mini);

                    break;
                    default:
            }
        } catch (Exception ignored) {
// write code here
        }
    }

}
