package com.engagemytime.app.Intro;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;

import com.ToxicBakery.viewpager.transforms.CubeOutTransformer;
import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.engagemytime.app.Activity.Dashboard;
import com.engagemytime.app.Activity.Login;
import com.engagemytime.app.Activity.Set_Location;
import com.engagemytime.app.Activity.Sign_Up;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.transform.Transformer;


public class IntroPage extends BaseActivity  {

    Context context;
    private SessionManager sessionManager;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.linear) {

            startNextActivity(Login.class);
            //  finish();
//        } else if (v.getId() == R.id.signUpSubmit) {
//            TedPermission.with(this)
//                    .setPermissionListener(this)
//                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
//                    .check();
//            //  finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.intro_content);
        context = IntroPage.this;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sessionManager = new SessionManager(context);
        if (sessionManager.isLoggedIn()) {
            startNextActivity(Dashboard.class);
            finish();
        }
//        viewPager = (ViewPager) findViewById(R.id.viewPager);
//        Button button = (Button) findViewById(R.id.signInSubmit);
//        radio1 = (RadioButton) findViewById(R.id.radio1);
//        radio2 = (RadioButton) findViewById(R.id.radio2);
//        radio3 = (RadioButton) findViewById(R.id.radio3);
//        radio4 = (RadioButton) findViewById(R.id.radio4);

//        setOnClickListener(R.id.signUpSubmit);
//        setOnClickListener(R.id.signInSubmit);
        setOnClickListener(R.id.linear);
//
//
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), 4);
//        viewPager.setAdapter(viewPagerAdapter);
//        viewPager.setPageTransformer(true, new CubeOutTransformer());
//        timer = new Timer();
//        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);
//
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                if (position == 0) {
//                    radio1.setChecked(true);
//                } else if (position == 1) {
//                    radio2.setChecked(true);
//                } else if (position == 2) {
//                    radio3.setChecked(true);
//                } else if (position == 3) {
//                    radio4.setChecked(true);
//                }
//            }
//
//            @Override
//            public void onPageSelected(int position) {
////write code here
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
////write code here
//            }
//        });
//        //  Timer timer = new Timer();
//        //timer.scheduleAtFixedRate(new MyTimerTask(), 4000, 4000);
//    }
//
//    @Override
//    protected void onStop() {
//        timer.cancel();
//        super.onStop();
//    }
//
//    @Override
//    public void onPermissionGranted() {
//        //startNextActivity(Set_Location.class);
//        Intent intent = new Intent(context, Set_Location.class);
//
//        startActivityForResult(intent, 2);
//
//    }
//
//    @Override
//    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
////write code here
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (requestCode == 2) {
//            if (resultCode == Activity.RESULT_OK) {
//                String resultS = (String) data.getStringExtra("result");
//                String address = (String) data.getStringExtra("address");
//                Intent intent = new Intent(context, Sign_Up.class);
//                intent.putExtra("result", resultS);
//                intent.putExtra("address", address);
//                startActivity(intent);
//                finish();
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//
//
//                //Write your code if there's no result
//            }
//        }
//
//    }//onActivit
//
//
//    public class MyTimerTask extends TimerTask {
//        @Override
//        public void run() {
//            IntroPage.this.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    if (viewPager.getCurrentItem() == 0) {
//                        viewPager.setCurrentItem(1);
//                        radio2.setChecked(true);
//                    } else if (viewPager.getCurrentItem() == 1) {
//                        viewPager.setCurrentItem(2);
//                        radio3.setChecked(true);
//                    } else if (viewPager.getCurrentItem() == 2) {
//                        viewPager.setCurrentItem(3);
//                        radio4.setChecked(true);
//                    } else if (viewPager.getCurrentItem() == 3) {
//                        viewPager.setCurrentItem(0);
//                        radio1.setChecked(true);
//                    }
//                }
//            });
//        }
//    }

    }
}