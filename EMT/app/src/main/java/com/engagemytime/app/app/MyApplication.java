package com.engagemytime.app.app;

import android.app.Application;

import com.engagemytime.app.app.helper.MyPreferenceManager;


public class MyApplication extends Application {
 
    public static final String TAG = MyApplication.class
            .getSimpleName();
 
    private  MyApplication mInstance;
 
    private MyPreferenceManager pref;
 
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
 
    public  synchronized MyApplication getInstance() {
        return mInstance;
    }
 
 
    public MyPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new MyPreferenceManager(this);
        }
 
        return pref;
    }
}