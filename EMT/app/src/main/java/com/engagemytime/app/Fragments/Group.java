package com.engagemytime.app.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.engagemytime.app.Adapter.GroupAdaper;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Group extends Fragment implements HttpProcessor.HttpResponser, View.OnClickListener {
    View view;
    String id = "";
    private Context context;
    private ArrayList<Container> grpList;
    private GroupAdaper grpListAdapter;
    private RecyclerView recycler;
    private SessionManager sessionManager;
    EditText title, status;
    FloatingActionButton fab;
    Button submit;
    private JSONObject jsonObject;
    ImageView cancel;
    CardView cardView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_group, container, false);
        context = getActivity();
        title = (EditText) view.findViewById(R.id.title);
        cardView = (CardView) view.findViewById(R.id.cardView);
        status = (EditText) view.findViewById(R.id.details);
        submit = (Button) view.findViewById(R.id.submit);
        cancel = (ImageView) view.findViewById(R.id.cancel);
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        id = getActivity().getIntent().getStringExtra("pid");
        sessionManager = new SessionManager(context);
        fab.setOnClickListener(this);
        cancel.setOnClickListener(this);
        submit.setOnClickListener(this);
        if (sessionManager.getKeyUserId().equals(id) || id == null) {
            fab.setVisibility(View.VISIBLE);
            id = sessionManager.getKeyUserId();
        } else {
            fab.setVisibility(View.GONE);

        }
        getGroups();
        return view;

    }

    private void getGroupDetails(JSONArray jsonArrayCont) {
        grpList = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObjectPart = new JSONObject();
        try {
            for (int a = 0; a < jsonArrayCont.length(); a++) {
                JSONObject jsonObject = jsonArrayCont.getJSONObject(a);

                Container container = new Container();
                container.setTitle(jsonObject.optString("group_name"));
                container.setStatus(jsonObject.optString("purpose_of_group"));
                container.setId(jsonObject.optString("id"));
                // container.setPid(jsonObject.optString("pid"));

                container.setGrpId(jsonObject.optString("group_id"));
                jsonArray.put(container.getGrpId());

                grpList.add(container);
            }
            jsonObjectPart.put("groupId", jsonArray);
            if (id.equals(sessionManager.getKeyUserId())) {
                sessionManager.setGrpList(jsonArray.toString());
            }
            updateGroup(jsonObjectPart);
        } catch (Exception ignored) {

        }

        grpListAdapter = new GroupAdaper(context, grpList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(grpListAdapter);
        grpListAdapter.notifyDataSetChanged();
    }

    private void getGroups() {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("personIds", id);
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Groups?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_GROUP");
        httpProcessor.setHttpResponserListener(this);

    }


    @Override
    public void responseResult(String result, String TAG) {

        try {
            switch (TAG) {
                case "GET_GROUP":
                    JSONArray jsonArrayCont = new JSONArray(result);
                    getGroupDetails(jsonArrayCont);
                    break;
                case "ADD_GROUP":
                    getGroups();
                    cardView.setVisibility(View.GONE);
                    break;
                case "UPDATE_GROUP":

                    break;

                default:
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {
            JSONObject jsonObject = convJsonData();
            if (jsonObject != null) {
                addGroup(jsonObject);
            }
        } else if (v.getId() == R.id.cancel) {

            cardView.setVisibility(View.GONE);

        } else if (v.getId() == R.id.fab) {

            cardView.setVisibility(View.VISIBLE);

        }
    }

    private JSONObject convJsonData() {
        this.jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray.put(id);
            int gid = (int) System.currentTimeMillis();
            jsonObject.put("group_id", gid);
            jsonObject.put("lastUpdate", "New Group");
            jsonObject.put("updatedBy", sessionManager.getKeyUserId());
            jsonObject.put("group_name", title.getText().toString());
            jsonObject.put("purpose_of_group", status.getText().toString());
            jsonObject.put("personIds", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this.jsonObject;
    }

    private void addGroup(JSONObject jsonObject) {

        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Groups/?access_token=" + sessionManager.getAccessToken(), HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("ADD_GROUP");
        httpProcessor.setHttpResponserListener(this);
    }

    private void updateGroup(JSONObject jsonObjects) {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("email", sessionManager.getKeyEmailId());
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "where=" + jsonObject.toString();
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObjects.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/update/?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("UPDATE_GROUP");
        httpProcessor.setHttpResponserListener(this);
    }
}
