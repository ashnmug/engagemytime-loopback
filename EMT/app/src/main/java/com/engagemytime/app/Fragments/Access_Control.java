package com.engagemytime.app.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.engagemytime.app.Activity.Search_Employee;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Access_Control extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, HttpProcessor.HttpResponser {
    View view;

    private Context context;

    private Switch accessAll, accessRole, accessPerson, accessProfile, accesscontact, accessAcc, accessCal, accessPrjct, accessJob;
    LinearLayout linearLayout;
    String id = "";
    private String acId = "";
    private Button update;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.access_control, container, false);
        context = getActivity();
        accessAll = (Switch) view.findViewById(R.id.accessAll);
        accessProfile = (Switch) view.findViewById(R.id.accessProfile);
        accesscontact = (Switch) view.findViewById(R.id.accesscontact);
        accessAcc = (Switch) view.findViewById(R.id.accessAcc);
        accessCal = (Switch) view.findViewById(R.id.accessCal);
        accessPrjct = (Switch) view.findViewById(R.id.accessPrjct);
        accessRole = (Switch) view.findViewById(R.id.accessRole);
        accessJob = (Switch) view.findViewById(R.id.accessJob);
        accessPerson = (Switch) view.findViewById(R.id.accessPerson);
        update = (Button) view.findViewById(R.id.update);
        update.setOnClickListener(this);
        linearLayout = (LinearLayout) view.findViewById(R.id.linear);
        checkStatus();
        id = getActivity().getIntent().getStringExtra("pid");

        accessAll.setOnCheckedChangeListener(this);

        accessAcc.setOnCheckedChangeListener(this);
        accessJob.setOnCheckedChangeListener(this);
        accessCal.setOnCheckedChangeListener(this);
        accesscontact.setOnCheckedChangeListener(this);
        accessPerson.setOnCheckedChangeListener(this);
        accessPrjct.setOnCheckedChangeListener(this);
        accessProfile.setOnCheckedChangeListener(this);
        accessRole.setOnCheckedChangeListener(this);
        getAccess();
        return view;
    }

    private void checkStatus() {
        if (accessAll.isChecked()) {
            linearLayout.setVisibility(View.GONE);
        } else {
            linearLayout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.update) {

            JSONObject jsonObject = convJsonDataAccess(accessPerson.isChecked(), accessRole.isChecked(), accessProfile.isChecked(), accesscontact.isChecked(), accessCal.isChecked(), accessAcc.isChecked(), accessJob.isChecked(), accessPrjct.isChecked());
            updateAccess(jsonObject);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton v, boolean b) {

        updateAccessMulti(v);
    }

    private void updateAccessMulti(CompoundButton v) {
        if (v.getId() == R.id.accessAll) {
            if (accessAll.isChecked()) {
                accessJob.setChecked(true);
                accessProfile.setChecked(true);
                accessPrjct.setChecked(true);
                accessPerson.setChecked(true);
                accessAcc.setChecked(true);
                accessCal.setChecked(true);
                accesscontact.setChecked(true);
                accessRole.setChecked(true);
            }
        } else {
            chechAllCechk();
        }
        update.setVisibility(View.VISIBLE);
    }

    private void chechAllCechk() {

        if (accessAcc.isChecked() && accessCal.isChecked() && accesscontact.isChecked() && accessJob.isChecked() && accessPerson.isChecked() && accessProfile.isChecked() && accessPrjct.isChecked() && accessRole.isChecked()) {
            accessAll.setChecked(true);
        } else {
            accessAll.setChecked(false);
        }
    }

    private void getAccess() {
        SessionManager sessionManager = new SessionManager(context);
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            if (id == null) {
                id = sessionManager.getKeyUserId();
            }
            jsonObject.put("personId", id);
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "accessControls?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_ACCESS");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String results, String TAG) {
        try {
            switch (TAG) {
                case "GET_ACCESS":

                    JSONArray jsonArrayCont = new JSONArray(results);

                    for (int a = 0; a < jsonArrayCont.length(); a++) {
                      JSONObject  jsonObject = jsonArrayCont.getJSONObject(a);

                        checkSwitch(jsonObject);
                    }

                    update.setVisibility(View.GONE);

                    break;
                case "UPDATE_ACCESS":

                    getAccess();
                    update.setVisibility(View.GONE);

                    break;
                    default:
            }
        } catch (Exception e) {

        }
    }

    private void updateAccess(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "accessControls", HttpProcessor.PATCH, requestBody);
        httpProcessor.executeRequest("UPDATE_ACCESS");
        httpProcessor.setHttpResponserListener(this);


    }

    private JSONObject convJsonDataAccess(boolean prsn, boolean role, boolean profile, boolean contact, boolean calender, boolean acc, boolean job, boolean project) {
        JSONObject jsonObject = new JSONObject();


        SessionManager sessionManager = new SessionManager(context);
        try {
            jsonObject.put("id", acId);
            jsonObject.put("access_to_person", prsn);
            jsonObject.put("access_on_role", role);
            jsonObject.put("access_on_profile", profile);
            jsonObject.put("access_on_contact", contact);
            jsonObject.put("access_on_calendar", calender);
            jsonObject.put("access_on_account", acc);
            jsonObject.put("access_on_job", job);
            jsonObject.put("access_on_project", project);
            jsonObject.put("personId", sessionManager.getKeyUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    private void checkSwitch(JSONObject jsonObject) {
        acId = jsonObject.optString("id");
        boolean accessPersonb = jsonObject.optBoolean("access_to_person");
        boolean accessProfileb = jsonObject.optBoolean("access_on_profile");
        boolean accessContactb = jsonObject.optBoolean("access_on_contact");
        boolean accessJobb = jsonObject.optBoolean("access_on_job");
        boolean accessCalb = jsonObject.optBoolean("access_on_calendar");
        boolean accessProjb = jsonObject.optBoolean("access_on_project");
        boolean accessAccb = jsonObject.optBoolean("access_on_account");
        boolean accessRoleb = jsonObject.optBoolean("access_on_role");
        this.accessRole.setChecked(accessRoleb);
        this.accessAcc.setChecked(accessAccb);
        this.accessProfile.setChecked(accessProfileb);
        this.accessPerson.setChecked(accessPersonb);
        this.accesscontact.setChecked(accessContactb);
        this.accessJob.setChecked(accessJobb);
        this.accessCal.setChecked(accessCalb);
        this.accessPrjct.setChecked(accessProjb);
        chechAllCechk();
    }
}
