package com.engagemytime.app.Model;

/**
 * Created by Casa Curo on 9/16/2017.
 */
public interface ChangeNotify {
    public void changeFragment(int pos);
    public void changeFragment(int pos, String Tag);
    public void showHomeFragment();
}