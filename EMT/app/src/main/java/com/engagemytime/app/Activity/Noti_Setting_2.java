package com.engagemytime.app.Activity;

import android.os.Bundle;

import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;

public class Noti_Setting_2 extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noti__setting);
        initToolBar("Notification Setting");
    }
}
