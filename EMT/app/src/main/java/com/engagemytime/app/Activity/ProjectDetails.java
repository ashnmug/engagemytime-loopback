package com.engagemytime.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import com.engagemytime.app.Adapter.JobAdaper;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class ProjectDetails extends BaseActivity implements HttpProcessor.HttpResponser {

    private RecyclerView recycler;
    private Context context;
    String id, title, statusString, lead, descString;
    private SessionManager sessionManager;
    CardView cardView;
    EditText titleEd, details;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.editText) {
            checkEnable();
        } else if (v.getId() == R.id.submit) {
            convJsonData();
        } else if (v.getId() == R.id.cancel) {

            cardView.setVisibility(View.GONE);

        } else if (v.getId() == R.id.fab) {

            cardView.setVisibility(View.VISIBLE);

        } else if (v.getId() == R.id.update) {

            updateProject();

        }

    }

    private void addJob(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "JobProfiles/?access_token=" + sessionManager.getAccessToken(), HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("ADD_JOB");
        httpProcessor.setHttpResponserListener(this);
    }


    private void convJsonData() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray.put(sessionManager.getKeyUserId());
            jsonObject.put("personIds", jsonArray);
            jsonObject.put("job_id", id);
            jsonObject.put("job_name", titleEd.getText().toString());
            jsonObject.put("job_status", "New");
            jsonObject.put("job_details", details.getText().toString());
            jsonObject.put("projectId", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addJob(jsonObject);
    }


    private void checkEnable() {
        if (isEnable(R.id.leadBox)) {
            setEnable(R.id.leadBox, false);
            setEnable(R.id.titleBox, false);
            setEnable(R.id.status, false);
            setEnable(R.id.desc, false);
            hideVisibility(R.id.update);
            recycler.setVisibility(View.VISIBLE);
        } else {
            setEnable(R.id.leadBox, true);
            setEnable(R.id.titleBox, true);
            setEnable(R.id.status, true);
            setEnable(R.id.desc, true);
            showVisibility(R.id.update);
            recycler.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details);
        initToolBar("Project Details");

        context = ProjectDetails.this;
        sessionManager = new SessionManager(context);
        id = getIntent().getStringExtra("pid");
        titleEd = (EditText) findViewById(R.id.title);
        cardView = (CardView) findViewById(R.id.cardView);

        details = (EditText) findViewById(R.id.details);
        titleEd.setHint("Job Title");
        details.setHint("Job Description");

        descString = getIntent().getStringExtra("desc");
        title = getIntent().getStringExtra("title");
        lead = getIntent().getStringExtra("lead");
        statusString = getIntent().getStringExtra("status");
        setText(statusString, R.id.status);
        setText(title, R.id.titleBox);
        setText(lead, R.id.leadBox);
        setText(descString, R.id.desc);
        recycler = (RecyclerView) findViewById(R.id.recycler);

        setOnClickListener(R.id.editText);

        setOnClickListener(R.id.cancel);
        setOnClickListener(R.id.fab);
        setOnClickListener(R.id.update);
        setOnClickListener(R.id.submit);
        getJob();
    }

    private void getJobData(JSONArray jsonArrayCont) {
        ArrayList<Container> notiList = new ArrayList<>();
        try {
            for (int a = 0; a < jsonArrayCont.length(); a++) {
                JSONObject jsonObject = jsonArrayCont.getJSONObject(a);
                JSONArray jsonObjectLoc = jsonObject.getJSONArray("personIds");

                Container container = new Container();
                container.setTitle(jsonObject.optString("job_name"));
                container.setComment(jsonObject.optString("job_details"));
                container.setStatus(jsonObject.optString("job_status"));
                container.setId(jsonObject.optString("id"));
                container.setJsonArray(jsonObjectLoc);


                notiList.add(container);
            }
        } catch (Exception ignored) {
//write code
        }

        JobAdaper notiListAdapter = new JobAdaper(context, notiList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(notiListAdapter);
        notiListAdapter.notifyDataSetChanged();
    }

    private void getJob() {

        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "ProjectProfiles/" + id + "/projectJobs?access_token=" + sessionManager.getAccessToken(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_JOB");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String result, String TAG) {
        try {
            switch (TAG) {
                case "GET_JOB":

                    JSONArray jsonArrayCont = new JSONArray(result);
                    getJobData(jsonArrayCont);


                    break;
                case "ADD_JOB":
                    getJob();
                    cardView.setVisibility(View.GONE);
                    break;
                case "UPDATE_PROJECT":


                    JSONObject jsonObject = new JSONObject(result);
                    setText(jsonObject.optString("project_name"), R.id.titleBox);
                    setText(jsonObject.optString("project_details"), R.id.desc);
                    setText(jsonObject.optString("project_status"), R.id.status);
                    setText(jsonObject.optString("project_lead"), R.id.leadBox);

                    break;
                default:
            }
        } catch (Exception ignored) {
//write code
        }
    }

    private void updateProject() {
        JSONObject jsonObject = convJsonUpdateData();
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "ProjectProfiles/" + id + "?access_token=" + sessionManager.getAccessToken(), HttpProcessor.PATCH, requestBody);
        httpProcessor.executeRequest("UPDATE_PROJECT");
        httpProcessor.setHttpResponserListener(this);
    }

    private JSONObject convJsonUpdateData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("project_name", getEditText(R.id.titleBox));
            jsonObject.put("project_details", getEditText(R.id.desc));
            jsonObject.put("project_status", getEditText(R.id.status));
            jsonObject.put("project_lead", getEditText(R.id.leadBox));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
