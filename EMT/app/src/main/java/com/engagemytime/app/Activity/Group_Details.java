package com.engagemytime.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.engagemytime.app.Adapter.EmployeeAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Group_Details extends BaseActivity implements HttpProcessor.HttpResponser {
    Context context;
    EmployeeAdapter employeeAdapter;
    ArrayList<Container> employeeList;
    RecyclerView recyclerView;
    String id, groupid;
    JSONArray jsonArrayPids = null;
    private SessionManager sessionManager;

    private String title = "";
    private String desc = "";

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add) {
            Intent intent = new Intent(context, Search_Employee.class);
            startActivityForResult(intent, 2);
        } else if (v.getId() == R.id.update) {
            convJsonData(jsonArrayPids, "Content Update");
        } else if (v.getId() == R.id.edit_grp) {
            checkEnable();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 2) {

            String personId = data.getStringExtra("id");
            if (jsonArrayPids == null) {
                jsonArrayPids = new JSONArray();
                jsonArrayPids.put(personId);
            } else {
                jsonArrayPids.put(personId);
            }
            convJsonData(jsonArrayPids, "Request");
        }

    }

    private void checkEnable() {
        if (isEnable(R.id.grpTitle)) {
            setEnable(R.id.grpTitle, false);
            setEnable(R.id.grpDesc, false);
            showVisibility(R.id.add);
            hideVisibility(R.id.update);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            setEnable(R.id.grpDesc, true);
            setEnable(R.id.grpTitle, true);
            hideVisibility(R.id.add);
            showVisibility(R.id.update);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void convJsonData(JSONArray personId, String request) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("id", id);
            jsonObject.put("lastUpdate", request);
            jsonObject.put("updatedBy", sessionManager.getKeyUserId());
            jsonObject.put("group_name", getEditText(R.id.grpTitle));
            jsonObject.put("purpose_of_group", getEditText(R.id.grpDesc));
            jsonObject.put("personIds", personId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        updateGroup(jsonObject);
    }

    private void updateGroup(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Groups?access_token=" + sessionManager.getAccessToken(), HttpProcessor.PATCH, requestBody);
        httpProcessor.executeRequest("GROUP");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group__details);
        context = Group_Details.this;
        sessionManager = new SessionManager(context);

        setOnClickListener(R.id.add);
        setOnClickListener(R.id.update);
        setOnClickListener(R.id.edit_grp);
        setOnClickListener(R.id.join);


        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        desc = intent.getStringExtra("desc");
        id = intent.getStringExtra("id");
        groupid = intent.getStringExtra("groupid");

        setText(R.id.grpTitle, title);
        setText(R.id.grpDesc, desc);
        initToolBar(title);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        getEmployeeList(id);
    }

    private void getEmployeeList(String id) {
        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Groups/" + id + "/list_of_person?access_token=" + sessionManager.getAccessToken(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_EMPLOYEE");
        httpProcessor.setHttpResponserListener(this);

    }

    private void onClickAdapter(final boolean isMem) {
        employeeAdapter.setOnItemClickListner(new EmployeeAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.card1) {
                    if (isMem) {
                        Intent intent = new Intent(context, Employee_details.class);
                        intent.putExtra("pid", employeeList.get(position).getId());
                        context.startActivity(intent);
                    } else {
                        showToast("You are not a member for this group");

                    }
                }
            }
        });
        employeeAdapter.setOnItemLongClickListner(new EmployeeAdapter.onRecyclerViewItemLongClickListner() {
            @Override
            public void onItemLongClickListner(View view, int position) {
                showDeleteDialog(position);
            }
        });
    }

    @Override
    public void responseResult(String result, String TAG) {
        try {
            switch (TAG) {
                case "GET_EMPLOYEE":
                    JSONArray jsonArray = new JSONArray(result);
                    jsonArrayPids = new JSONArray();
                    employeeList = new ArrayList<>();
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(a);


                        Container container = new Container();

                        container.setTitle(jsonObject.optString("first_name"));
                        container.setImage(jsonObject.optString("picture_of_person"));
                        container.setExp(a + " Yr");
                        container.setRating("4." + a);


                        container.setGender(jsonObject.optString("gender"));
                        container.setId(jsonObject.optString("id"));
                        jsonArrayPids.put(container.getId());
                        employeeList.add(container);
                    }
                    employeeAdapter = new EmployeeAdapter(context, employeeList);
                    recyclerView.setAdapter(employeeAdapter);
                    employeeAdapter.notifyDataSetChanged();
                    boolean isMember = false;
                    for (int a = 0; a < employeeList.size(); a++) {
                        if (sessionManager.getKeyUserId().equals(employeeList.get(a).getId())) {
                            a = employeeList.size();
                            isMember = true;
                        }
                    }
                    onClickAdapter(isMember);

                    break;
                case "GROUP":
                    JSONObject resulta = new JSONObject(result);
                    if (resulta.optString("lastUpdate").equals("Request")) {
                        showToast("Request has been sent to user");
                    } else {
                        showToast("Group updated");
                    }
                    getEmployeeList(id);
                    break;
                default:
            }
        } catch (Exception ignored) {

// write code here
        }
    }

    private void showDeleteDialog(final int position) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Alert !!!");
        alertDialogBuilder.setMessage("Do you want to remove this employee ?");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        jsonArrayPids.remove(position);
                        convJsonData(jsonArrayPids, "Delete");
                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

// write code here
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
