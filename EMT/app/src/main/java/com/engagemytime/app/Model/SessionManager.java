package com.engagemytime.app.Model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;


import com.google.firebase.iid.FirebaseInstanceId;

import java.io.ByteArrayOutputStream;

public class SessionManager {

    private static final String KEY_DEVICE_ID = "DEVICE_ID";
    private static final String KEY_IS_LOGGED_IN = "IS_LOGGED_IN";
    private static final String KEY_USER_ID = "USER_ID";
    private static final String KEY_USER_NAME = "USER_NAME";
    private static final String KEY_EMAIL_ID = "EMAIL_ID";
    private static final String KEY_PHONE_NUMBER = "PHONE_NUMBER";
    private static final String KEY_ADDRESS = "ADDRESS";
    private static final String KEY_LATITUDE = "LATITUDE";
    private static final String KEY_LONGITUDE = "LONGITUDE";
    private static final String KEY_USERTYPE = "USERTYPE";
    private static final String SET_DIST = "set_dist";
    private static final String ACCESS_TOKEN = "AccessToken";
    //    private static final String KEY_CITY = "CITY";
    private static final String KEY_PROFILE_IMAGE = "KEY_PROFILE_IMAGE";
    private static final String KEY_PROFILE_RESUME = "KEY_PROFILE_RESUME";

    private static final String KEY_USER_IMAGE = "KEY_USER_IMAGE";

    private Context context;

    private SharedPreferences pref;

    private Editor editor;

    private String PREFERENCE_NAME = "ENgageMyTime";

    private int MODE_PRIVATE = 0;
    private String KEY_NOTI = "notification";
    private String SET_GRP_LIST = "SET_GRP_LIST";
    private String SET_LAT = "se_lat";
    private String SET_LONG = "se_long";
    private String SET_INST_ID = "SET_INST_ID";
    private String Role = "ROLE";
    private String PROJECT_TITLE="PROJECT_TITLE";
    private String PRO_DESC="PRO_DESC";
    private String TEAM_LEAD="TEAM_LEAD";

    public SessionManager(Context context) {
        // TODO Auto-generated constructor stub

        this.context = context;
        pref = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE);
        editor = pref.edit();
    }

    public void saveDeviceId(String deviceId) {

        editor.putString(KEY_DEVICE_ID, deviceId);
        editor.commit();

    }

    public void saveJSON(String noti) {

        editor.putString(KEY_NOTI, noti);
        editor.commit();

    }


    public String getDeviceId() {

        return pref.getString(KEY_DEVICE_ID, "");

    }

    public String getJSON() {

        return pref.getString(KEY_NOTI, "");

    }

    public String getInstId() {

        return pref.getString(SET_INST_ID, "");

    }

    public void delString(String deviceId) {

        editor.putString("del", deviceId);
        editor.commit();

    }

    public String getDelString() {

        return pref.getString("del", "");

    }

    public String getRole() {

        return pref.getString(Role, "");

    }

    public void saveUserPicture(String bp) {

        editor.putString(KEY_USER_IMAGE, bp);

        editor.commit();
    }

    public String getUserPicture() {
        return pref.getString(KEY_USER_IMAGE, "");
    }

    public void setGrpList(String bp) {

        editor.putString(SET_GRP_LIST, bp);

        editor.commit();
    }

    public String getGrpList() {
        return pref.getString(SET_GRP_LIST, "");
    }

    // method for bitmap to base64
    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        return imageEncoded;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public void saveCredentials(String userName, String userId, String emailId, String phoneNumber) {


        editor.putString(KEY_USER_ID, userId);
        editor.putString(KEY_USER_NAME, userName);
        editor.putString(KEY_EMAIL_ID, emailId);
        editor.putString(KEY_PHONE_NUMBER, phoneNumber);
        editor.commit();

    }

    public void setProfileImagePath(String profileImage) {

        editor.putString(KEY_PROFILE_IMAGE, profileImage);
        editor.commit();
    }

    public String getProfileImagePath() {

        return pref.getString(KEY_PROFILE_IMAGE, null);
    }

    public String getResumePath() {

        return pref.getString(KEY_PROFILE_RESUME, "");
    }

    public void setResumePath(String resume) {

        editor.putString(KEY_PROFILE_RESUME, resume);
        editor.commit();
    }

    public void saveLoggedIn(boolean status) {

        editor.putBoolean(KEY_IS_LOGGED_IN, status);
        editor.commit();
    }


    public String getKeyUserId() {

        return pref.getString(KEY_USER_ID, "0");
    }


    public boolean isLoggedIn() {

        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public String getKeyUserName() {

        return pref.getString(KEY_USER_NAME, "");
    }

    public String getKeyEmailId() {

        return pref.getString(KEY_EMAIL_ID, "");
    }

    public String getKeyPhoneNumber() {

        return pref.getString(KEY_PHONE_NUMBER, "");
    }

    public void setOrig(String orig) {

        editor.putString(KEY_ADDRESS, orig);

        editor.commit();

    }

    public String getOrig() {
        return pref.getString(KEY_ADDRESS, "");
    }

    public String getLatitude() {
        return pref.getString(KEY_LATITUDE, "0.0");
    }

    public String getLongitude() {
        return pref.getString(KEY_LONGITUDE, "0.0");
    }

    public void setAccessToken(String admin) {
        editor.putString(ACCESS_TOKEN, admin);

        editor.commit();
    }

    public void setRole(String role) {
        editor.putString(Role, role);

        editor.commit();
    }

    public String getAccessToken() {
        return pref.getString(ACCESS_TOKEN, null);
    }

    public void setProjectName(String admin) {
        editor.putString(PROJECT_TITLE, admin);

        editor.commit();
    }

    public String getProjectName() {
        return pref.getString(PROJECT_TITLE, null);
    }

    public void setProDesc(String admin) {
        editor.putString(PRO_DESC, admin);

        editor.commit();
    }

    public String getProDesc() {
        return pref.getString(PRO_DESC, null);
    }

    public void setTeamLead(String admin) {
        editor.putString(TEAM_LEAD, admin);

        editor.commit();
    }

    public String getTeamLead() {
        return pref.getString(TEAM_LEAD, null);
    }

    public String getDistance() {
        return pref.getString(SET_DIST, "0");
    }

    public void setDistance(int setDist) {
        editor.putString(SET_DIST, String.valueOf(setDist));

        editor.commit();
    }

    public void setCircleLatitude(double lat) {
        editor.putString(SET_LAT, String.valueOf(lat));

        editor.commit();
    }

    public void setCircleLongitude(double lng) {
        editor.putString(SET_LONG, String.valueOf(lng));

        editor.commit();
    }

    public double getCircleLat() {
        return Double.parseDouble(pref.getString(SET_LAT, "0.0"));
    }

    public double getCircleLong() {
        return Double.parseDouble(pref.getString(SET_LONG, "0.0"));
    }

    public void saveAccessGenreted(String keyUserId) {
        editor.putBoolean(keyUserId + "access", true);
        editor.commit();
    }

    public void saveNotiAct(boolean status) {
        editor.putBoolean("push", status);
        editor.commit();
    }

    public boolean isNotiAct() {
        return pref.getBoolean("push", false);
    }

    public void saveThird(boolean status) {
        editor.putBoolean("third", status);
        editor.commit();
    }

    public boolean isThird() {
        return pref.getBoolean("third", false);
    }


    public void setVCode(String instId) {
        editor.putString(SET_INST_ID, instId);

        editor.commit();
    }

    public String getVCode() {
        return pref.getString(SET_INST_ID, "");
    }



}

