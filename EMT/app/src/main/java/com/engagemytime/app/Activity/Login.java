package com.engagemytime.app.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import com.engagemytime.app.Model.Constants;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.google.code.linkedinapi.schema.Person;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;


public class Login extends BaseActivity implements PermissionListener, GoogleApiClient.OnConnectionFailedListener, HttpProcessor.HttpResponser {

    private SessionManager sessionManager;
    private String userName, password;
    private Dialog dialog;
    private Button submit;
    private ImageView cancel;
    private EditText et_reset_pass;
    private static final String TAG = Login.class.getSimpleName();
    private static final int RC_SIGN_IN = 7;
    private GoogleApiClient mGoogleApiClient;
    private Context context;
    private String thirdP = "", thirdPersonTitle = "", thirdPersonName = "", thirdEmail = "", thirdPhotoUrl = "";

    private CallbackManager callbackManager;
    private String thirdGender = "";
    private static final String KEY_EMAIL = "email";

    private LinkedInOAuthService oAuthService;
    private LinkedInApiClientFactory factory;
    private LinkedInRequestToken liToken;
    private LinkedInApiClient client;

    @Override
    public void onClick(View v) {
        userName = getEditText(R.id.username);
        password = getEditText(R.id.password);

        if (v.getId() == R.id.signInSubmit) {
            if (userName.equals("") || password.equals("")) {
                Toast.makeText(context, "Invalid username or password", Toast.LENGTH_SHORT).show();
                startNextActivity(Dashboard.class);
            } else {
                convJsonData(userName, password);

            }
        } else if (v.getId() == R.id.signupHere) {
            Intent intent = new Intent(context, Set_Location.class);
            startActivityForResult(intent, 2);
        } else if (v.getId() == R.id.forgotPass) {
            showDialog();
        } else if (v.getId() == R.id.btn_submit) {

            updatePassword();

        } else if (v.getId() == R.id.cancel) {
            dialog.dismiss();
        } else if (v.getId() == R.id.facebook) {

            facebookLogin();
        } else if (v.getId() == R.id.gmail) {
            signGmailIn();

        } else if (v.getId() == R.id.linkdien) {
            signIngLinkIDn();
        }

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 2) {
                String resultS = (String) data.getStringExtra("result");
                String address = (String) data.getStringExtra("address");
                Intent intent = new Intent(context, Sign_Up.class);
                intent.putExtra("result", resultS);
                intent.putExtra("address", address);
                startActivity(intent);
                finish();
            } else if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult resultGoogle = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(resultGoogle);
            } else {

                callbackManager.onActivityResult(requestCode, resultCode, data);
                callbackManager.getClass().getName();
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {


            //Write your code if there's no result

        }

    }//onActivit

    private void convJsonData(String email, String password) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_EMAIL, email);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        loginServer(jsonObject);

    }

    private void loginServer(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/login", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("Login");
        httpProcessor.setHttpResponserListener(this);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = Login.this;
        sessionManager = new SessionManager(context);
        setOnClickListener(R.id.facebook);
        setOnClickListener(R.id.gmail);
        setOnClickListener(R.id.linkdien);

        setOnClickListener(R.id.signInSubmit);
        setOnClickListener(R.id.signupHere);
        setOnClickListener(R.id.forgotPass);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onPermissionGranted() {
        startNextActivity(Dashboard.class);
        finish();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void responseResult(String results, String TAG) {
        try {
            switch (TAG) {
                case "Login":
                    JSONObject resultJson = new JSONObject(results);
                    if (!resultJson.optString("id").equals("")) {
                        if (thirdEmail.equals("")) {
                            sessionManager.saveThird(false);
                        } else {
                            userName = thirdEmail;
                            sessionManager.saveThird(true);
                        }
                        sessionManager.setAccessToken(resultJson.optString("id"));
                        sessionManager.setRole(resultJson.optString("roleName"));
                        sessionManager.saveCredentials(userName, resultJson.optString("userId"), userName, "0");
                        nextActivit();
                    } else {
                        Toast.makeText(context, "Invalid username or password", Toast.LENGTH_SHORT).show();

                    }

                    break;
                case "sign_up":
                    JSONObject signUp = new JSONObject(results);
                    if (signUp.optString("title").equals("Signed up successfully")) {


                        sessionManager.saveCredentials(thirdEmail, signUp.optString("userId"), thirdEmail, "");
                        JSONObject tokeObject = signUp.optJSONObject("redirect");
                        String token = tokeObject.optString("token");

                        CONFIRM(token);
                    } else {
                        userName = thirdEmail;
                        convJsonData(thirdEmail, thirdP);
                    }
                    break;

                case "CONFIRM":
                    convJsonData(thirdEmail, thirdP);
                    break;
                case "UPDATE_PASS":

                    et_reset_pass.setText("");
                    break;
                default:
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void nextActivit() {
        sessionManager.saveLoggedIn(true);

        TedPermission.with(this)
                .setPermissionListener(this)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();
    }

    private void updatePassword() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_EMAIL, et_reset_pass.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/reset", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("UPDATE_PASS");
        httpProcessor.setHttpResponserListener(this);
    }

    public void showDialog() {
        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialogue);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        et_reset_pass = (EditText) dialog.findViewById(R.id.otp_field);

        submit = (Button) dialog.findViewById(R.id.btn_submit);
        cancel = (ImageView) dialog.findViewById(R.id.cancel);

        cancel.setOnClickListener(this);

        submit.setOnClickListener(this);

        dialog.show();

    }

    private void signGmailIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            thirdPersonName = acct.getDisplayName();
            thirdPersonTitle = acct.getFamilyName();
            thirdP = acct.getId();
            thirdPhotoUrl = acct.getPhotoUrl().toString();
            thirdEmail = acct.getEmail();
            sessionManager.setRole("Consumer");
            convJsonData();
        } else {
            Toast.makeText(context, "Invalid credential", Toast.LENGTH_SHORT).show();
        }


    }

    private void convJsonData() {
        Container container = new Container();
        String string = container.getFcmDeviceId();
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectLoc = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jsonObjectLoc.put("lat", 0);
            jsonObjectLoc.put("lng", 0);
            jsonObject.put("map_coordinates", jsonObjectLoc);
            jsonObject.put("first_name", thirdPersonName);
            jsonObject.put("middle_name", "");
            jsonObject.put("family_name", thirdPersonTitle);
            jsonObject.put("gender", thirdGender);
            jsonObject.put("age", 25);
            jsonObject.put("role", "Consumer");
            jsonObject.put("dob", "2017-09-28T05:51:40.836Z");
            jsonObject.put("legal_person", "Yes");
            jsonObject.put("natural_person", "Yes");
            jsonObject.put("picture_of_person", thirdPhotoUrl);
            jsonObject.put("username", thirdEmail);
            jsonObject.put("portal_address", "");
            jsonObject.put(KEY_EMAIL, thirdEmail);
            jsonObject.put("deviceToken", string);
            jsonObject.put("emailVerified", true);
            jsonObject.put("groupId", jsonArray);
            jsonObject.put("vcode", 0);
            jsonObject.put("password", thirdP);
            jsonObject.put("mobile", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        signUpServer(jsonObject);
    }

    private void signUpServer(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("sign_up");
        httpProcessor.setHttpResponserListener(this);

    }

    private void CONFIRM(String token) {
        RequestBody requestBody = new FormEncodingBuilder()

                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/confirm?uid=" + sessionManager.getKeyUserId() + "&token=" + token, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("CONFIRM");
        httpProcessor.setHttpResponserListener(this);

    }

    public void facebookLogin() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                try {
                                    thirdPersonName = object.getString("name");
                                    thirdP = object.getString("id");
                                    thirdEmail = object.getString(KEY_EMAIL);
                                    thirdGender = object.getString("gender");
                                    convJsonData();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(context, "Request cancel", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(context, "Error while login with facebook", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void signIngLinkIDn() {
        oAuthService = LinkedInOAuthServiceFactory.getInstance()
                .createLinkedInOAuthService(Constants.CONSUMER_KEY,
                        Constants.CONSUMER_SECRET);

        factory = LinkedInApiClientFactory.newInstance(
                Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);

        liToken = oAuthService
                .getOAuthRequestToken(Constants.OAUTH_CALLBACK_URL);

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(liToken
                .getAuthorizationUrl()));
        startActivity(i);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
            linkedInImport(intent);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void linkedInImport(Intent intent) {
        String verifier = intent.getData().getQueryParameter("oauth_verifier");

        LinkedInAccessToken accessToken = oAuthService.getOAuthAccessToken(
                liToken, verifier);
        client = factory.createLinkedInApiClient(accessToken);
        Person profile = client.getProfileForCurrentUser(EnumSet.of(
                ProfileField.ID, ProfileField.FIRST_NAME,
                ProfileField.LAST_NAME, ProfileField.HEADLINE));
        Toast.makeText(context, profile.getFirstName(), Toast.LENGTH_SHORT).show();

    }
}