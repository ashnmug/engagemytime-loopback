package com.engagemytime.app.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.engagemytime.app.Activity.CropImageActivity;
import com.engagemytime.app.Activity.ScrollingActivity;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.engagemytime.app.utilities.Utility;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.os.Environment.getExternalStoragePublicDirectory;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Personal extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser {
    View view;


    private Context context;
    FloatingActionButton fab;
    EditText first_name, middle_name, family_name, age;
    Spinner gender;
    private static final String TAKE_PHOTO = "Take Photo";
    private static final String CHOOSE_GALLERY = "Choose from Gallery";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private Bitmap bitmap;
    TextView uploadImage;
    ImageView imageProfile;
    private String mCurrentPhotoPath;
    private String id = "0";
    private SessionManager sessionManager;
    private JSONObject jsonObject;
    private Container container;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.personal_info, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        first_name = (EditText) view.findViewById(R.id.first_name);
        middle_name = (EditText) view.findViewById(R.id.middle_name);
        family_name = (EditText) view.findViewById(R.id.family_name);
        uploadImage = (TextView) view.findViewById(R.id.uploadImage);

        id = getActivity().getIntent().getStringExtra("pid");
        imageProfile = (ImageView) view.findViewById(R.id.imageProfile);
        age = (EditText) view.findViewById(R.id.age);
        gender = (Spinner) view.findViewById(R.id.gender);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Male");
        arrayList.add("Female");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_activated_1, arrayList);
        gender.setAdapter(arrayAdapter);
        imageProfile.setOnClickListener(this);
        uploadImage.setOnClickListener(this);
        fab.setOnClickListener(this);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if (sessionManager.getKeyUserId().equals(id) || id == null) {
            fab.setVisibility(View.VISIBLE);
            id = sessionManager.getKeyUserId();
        } else {
            fab.setVisibility(View.GONE);

        }


        getPersonalDetails();
        return view;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab) {
            if (first_name.isEnabled()) {

                convJsonData();


            } else {
                middle_name.setEnabled(true);
                first_name.setEnabled(true);
                family_name.setEnabled(true);
                age.setEnabled(true);
                gender.setEnabled(true);
                uploadImage.setVisibility(View.VISIBLE);
                fab.setImageResource(R.drawable.task);
            }
        } else if (view.getId() == R.id.uploadImage) {
            selectImage();
        } else if (view.getId() == R.id.imageProfile) {
            Intent intent = new Intent(context, ScrollingActivity.class);
            intent.putExtra("image", container.getImage());
            startActivity(intent);
        }
    }


    private void selectImage() {
        final boolean result = Utility.checkPermission(getActivity());
        final CharSequence[] items = {TAKE_PHOTO, CHOOSE_GALLERY,
                "Remove Picture"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(TAKE_PHOTO)) {
                    userChoosenTask = TAKE_PHOTO;
                    if (result)
                        cameraIntent();

                } else if (items[item].equals(CHOOSE_GALLERY)) {
                    userChoosenTask = CHOOSE_GALLERY;
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Remove Picture")) {

                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {

            //   Log.d("mylog", "Exception while creating file: " + ex.toString());
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            //  Log.d("mylog", "Photofile not null");
            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                    "com.engagemytime.app.Fragments",
                    photoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intent, REQUEST_CAMERA);
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        //  Log.d("mylog", "Path: " + mCurrentPhotoPath);
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                galleryIntent();

            } else if (requestCode == 12) {
                setCropedPicture();
            }
        }
    }

    private void setCropedPicture() {
        Bitmap cropedBitMap = SessionManager.decodeBase64(sessionManager.getUserPicture());
        if (cropedBitMap != null) {


            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            cropedBitMap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            //you can create a new file name "test.jpg" in sdcard folder.
            File f = new File(Environment.getExternalStorageDirectory()
                    + File.separator + System.currentTimeMillis() + ".jpg");

            try (FileOutputStream fo = new FileOutputStream(f)) {

                f.createNewFile();
                //write the bytes in file

                fo.write(bytes.toByteArray());
                // remember close de FileOutput


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            uploadImageCall(f);

        }
    }

    private void uploadImageCall(File f) {
        if (f.exists()) {

            Toast.makeText(context, "Image Found : " + f.getAbsolutePath(), Toast.LENGTH_SHORT).show();
//
            uploadFile(new File(f.getAbsolutePath()), context);

        } else {
            Toast.makeText(context, "Image Not Found", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals(TAKE_PHOTO))
                        cameraIntent();
                    else if (userChoosenTask.equals(CHOOSE_GALLERY))

                        galleryIntent();
                } else {
                    //code for deny
                }
                break;

            default:
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        bitmap = null;
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                Intent intent = new Intent(context, CropImageActivity.class);
                intent.putExtra("type", String.valueOf(data.getData()));
                startActivityForResult(intent, 12);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    ////////////////////////////API////////////////////

    private void getPersonalDetails() {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObjectP.put("where", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String filter = "filter=" + jsonObjectP.toString();
        RequestBody requestBody = new FormEncodingBuilder()
                .build();
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_PERSON");
        httpProcessor.setHttpResponserListener(this);

    }

    private void updatePersonalInfo(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/" + id + "?access_token=" + sessionManager.getAccessToken(), HttpProcessor.PATCH, requestBody);
        httpProcessor.executeRequest("UPDATE_PERSON");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void responseResult(String results, String TAG) {
        try {
            switch (TAG) {

                case "GET_PERSON":

                    JSONArray jsonArrayCont = new JSONArray(results);

                    for (int a = 0; a < jsonArrayCont.length(); a++) {
                        JSONObject jsonObject = jsonArrayCont.getJSONObject(a);

                        setPersonalDetails(jsonObject);

                    }
                    break;
                case "UPDATE_PERSON":
                    middle_name.setEnabled(false);
                    first_name.setEnabled(false);
                    family_name.setEnabled(false);
                    age.setEnabled(false);
                    gender.setEnabled(false);
                    fab.setImageResource(R.drawable.edit_mini);
                    uploadImage.setVisibility(View.GONE);
                    getPersonalDetails();
                    Toast.makeText(context, "Profile updated", Toast.LENGTH_SHORT).show();
                    break;
                case "GET_IMAGE":
                    JSONObject imageJson = new JSONObject(results);
                    JSONObject filesJson = imageJson.optJSONObject("result");
                    JSONObject fileJson = filesJson.optJSONObject("files");
                    JSONArray fileJsonArray = fileJson.optJSONArray("file");
                    JSONObject newJsonDetails = fileJsonArray.getJSONObject(0);
                    //   Toast.makeText(context, results, Toast.LENGTH_SHORT).show();
                    sessionManager.setProfileImagePath(API.IMAGE_URL + newJsonDetails.optString("name"));
                    convJsonData();
                    break;
                default:
            }
        } catch (Exception e) {

        }
    }

    private void setPersonalDetails(JSONObject jsonObject) {
        container = new Container();
        container.setFirst(jsonObject.optString("first_name"));
        container.setMiddle(jsonObject.optString("middle_name"));
        container.setLast(jsonObject.optString("family_name"));
        container.setAge(jsonObject.optString("age"));
        container.setImage(jsonObject.optString("picture_of_person"));
        container.setGender(jsonObject.optString("gender"));
        container.setDate(jsonObject.optString("dob"));
        container.setMobile(jsonObject.optString("mobile"));
        container.setPhone_number(jsonObject.optString("phone_number"));
        setText(container);
    }

    private void setText(Container container) {
        first_name.setText(container.getFirst());
        middle_name.setText(container.getMiddle());
        family_name.setText(container.getLast());
        age.setText(container.getAge());
        if (sessionManager.getKeyUserId().equals(id) || id == null) {
            sessionManager.setProfileImagePath(container.getImage());
        }
        if (container.getGender().equals("Female") || container.getGender().equals("female")) {
            gender.setSelection(1);
            Picasso.with(context)
                    .load(container.getImage()).placeholder(R.drawable.femaleprofile).error(R.drawable.femaleprofile)
                    .into(imageProfile);

        } else {
            Picasso.with(context)
                    .load(container.getImage()).placeholder(R.drawable.maleprofile).error(R.drawable.maleprofile)
                    .into(imageProfile);

            gender.setSelection(0);
        }
    }

    /////////////////////JSON_PARSE//////////////////////


    private void convJsonData() {
        jsonObject = new JSONObject();
        try {
            jsonObject.put("first_name", first_name.getText().toString());
            jsonObject.put("middle_name", middle_name.getText().toString());
            jsonObject.put("family_name", family_name.getText().toString());
            jsonObject.put("gender", gender.getSelectedItem().toString());
            jsonObject.put("age", age.getText().toString());
            jsonObject.put("picture_of_person", sessionManager.getProfileImagePath());
            jsonObject.put("email", sessionManager.getKeyEmailId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        updatePersonalInfo(jsonObject);
    }


    public void uploadFile(File file, final Context v) {
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse("Image/JPEG"), file))
                .addFormDataPart("result", "my_image")
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "uploadFiles/profilepic/upload", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("GET_IMAGE");
        httpProcessor.setHttpResponserListener(this);
    }


}
