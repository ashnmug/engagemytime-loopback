package com.engagemytime.app.Fragments;

import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.engagemytime.app.Adapter.SkillAdapter1;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;

public class DashboardFragment extends Fragment implements View.OnClickListener {
    int[] images = {R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java, R.drawable.java,
            R.drawable.java,
            R.drawable.java, R.drawable.java};
    String[] strings = {"Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java", "Java"};
    private RecyclerView recyclerEmp;
    ImageView search;
    private View view;
    CardView prvtKey, publicKey;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        context = getActivity();
        recyclerEmp = (RecyclerView) view.findViewById(R.id.recycler);
        search = (ImageView) view.findViewById(R.id.search);
        prvtKey = (CardView) view.findViewById(R.id.prvtKey);
        publicKey = (CardView) view.findViewById(R.id.publicKey);
        prvtKey.setOnClickListener(this);
        publicKey.setOnClickListener(this);
        search.setOnClickListener(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 5);
        recyclerEmp.setLayoutManager(gridLayoutManager);
        ArrayList<Container> arrayList = new ArrayList<>();
        for (int a = 0; a < 6; a++) {
            Bitmap icon = BitmapFactory.decodeResource(getResources(), images[a]);
            Container container1 = new Container();
            container1.setImageResourceBit(icon);
            container1.setTitle(strings[a]);
            arrayList.add(container1);
        }

        SkillAdapter1 employeeAdapter = new SkillAdapter1(context, arrayList);
        recyclerEmp.setAdapter(employeeAdapter);
//        employeeAdapter.setOnItemClickListner(new EmployeeAdapter.onRecyclerViewItemClickListner() {
//            @Override
//            public void onItemClickListner(View view, int position) {
//                startActivity(new Intent(context, DetailsProduct.class));
//            }
//        });

        return view;
    }

    private void showDialog(Context activity) {


        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialogue);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }
        });

        dialog.show();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.search) {
            showDialog(context);
        } else if (view.getId() == R.id.publicKey) {
            prvtKey.setCardBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
            publicKey.setCardBackgroundColor(ContextCompat.getColor(context, R.color.red));
        } else if (view.getId() == R.id.prvtKey) {
            publicKey.setCardBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
            prvtKey.setCardBackgroundColor(ContextCompat.getColor(context, R.color.red));

        }
    }
}


