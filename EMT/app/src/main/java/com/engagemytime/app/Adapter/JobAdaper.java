package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.engagemytime.app.Activity.Job_Details;
import com.engagemytime.app.Activity.ProjectDetails;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;


public class JobAdaper extends RecyclerView.Adapter<JobAdaper.MyViewHolder> {

    private final Context context;
    private final ArrayList<Container> list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, textViewDate, date;
        TextView status;
        ImageView image;
        CardView card;

        public MyViewHolder(View view) {
            super(view);
            status = (TextView) view.findViewById(R.id.status);
            title = (TextView) view.findViewById(R.id.title);
            card = (CardView) view.findViewById(R.id.card);


        }
    }


    public JobAdaper(Context context, ArrayList<Container> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_job, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container places = list.get(position);


        holder.title.setText(places.getTitle());
        holder.status.setText(places.getComment());
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Job_Details.class);
                intent.putExtra("array",places.getJsonArray().toString());
                intent.putExtra("id",places.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
