package com.engagemytime.app.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.engagemytime.app.Fragments.Access_Control;
import com.engagemytime.app.Fragments.Calender_Week;
import com.engagemytime.app.Fragments.Contact;
import com.engagemytime.app.Fragments.Group;
import com.engagemytime.app.Fragments.Personal;

import com.engagemytime.app.Fragments.Review;
import com.engagemytime.app.Fragments.Skill_info;


public class ProfileAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ProfileAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Personal tab1 = new Personal();
                return tab1;
            case 1:
                Contact tab2 = new Contact();
                return tab2;
            case 2:
                Skill_info tab3 = new Skill_info();
                return tab3;
            case 3:
                Access_Control tab4 = new Access_Control();
                return tab4;
            case 4:
                Calender_Week tab5 = new Calender_Week();
                return tab5;
            case 5:
                Group tab6 = new Group();
                return tab6;
            case 6:
                Review tab7 = new Review();
                return tab7;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}