package com.engagemytime.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.engagemytime.app.Adapter.EmployeeAdapter;
import com.engagemytime.app.Adapter.SkillAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Job_Details extends BaseActivity implements HttpProcessor.HttpResponser {
    private ArrayList<Container> empList;
    private RecyclerView recycler;
    private Context context;
    EditText titleBox, desc, status;

    private Button update;
    FloatingActionButton add_employee;
    String id;
    private SessionManager sessionManager;
    private EmployeeAdapter employeeAdapter;
    private JSONArray jsonArrayPids = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 2) {
            String personId = data.getStringExtra("id");
            if (jsonArrayPids == null) {
                jsonArrayPids = new JSONArray();
                jsonArrayPids.put(personId);
            } else {
                jsonArrayPids.put(personId);
            }
            convJsonData(jsonArrayPids, "Request");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.editText) {
            checkEnable();
        } else if (v.getId() == R.id.add_employee) {
            Intent intent = new Intent(context, Search_Employee.class);

            startActivityForResult(intent, 2);
        } else if (v.getId() == R.id.update && jsonArrayPids != null) {
            convJsonData(jsonArrayPids, "Update");
        }

    }

    private void checkEnable() {
        if (titleBox.isEnabled()) {
            titleBox.setEnabled(false);
            status.setEnabled(false);
            desc.setEnabled(false);
            update.setVisibility(View.GONE);
        } else {
            titleBox.setEnabled(true);
            desc.setEnabled(true);
            status.setEnabled(true);

            update.setVisibility(View.VISIBLE);

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job__details);
        initToolBar("Job Details");
        recycler = (RecyclerView) findViewById(R.id.recycler);
        add_employee = (FloatingActionButton) findViewById(R.id.add_employee);
        titleBox = (EditText) findViewById(R.id.titleBox);
        desc = (EditText) findViewById(R.id.desc);
        update = (Button) findViewById(R.id.update);
        status = (EditText) findViewById(R.id.status);
        id = getIntent().getStringExtra("id");

        setOnClickListener(R.id.editText);
        setOnClickListener(R.id.update);
        setOnClickListener(R.id.add_employee);
        context = Job_Details.this;
        sessionManager = new SessionManager(context);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);

        recycler.setLayoutManager(gridLayoutManager);
        getEmployeeList(id);
        GetJobInfo();
    }


    private void updateJobInfo(JSONObject jsonObject) {

        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "JobProfiles?access_token=" + sessionManager.getAccessToken(), HttpProcessor.PATCH, requestBody);
        httpProcessor.executeRequest("UPDATE_JOB");
        httpProcessor.setHttpResponserListener(this);
    }

    private void GetJobInfo() {

        RequestBody requestBody = new FormEncodingBuilder()

                .build();
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "JobProfiles/" + id + "?access_token=" + sessionManager.getAccessToken(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_JOB");
        httpProcessor.setHttpResponserListener(this);
    }

    private void getEmployeeList(String id) {

        RequestBody requestBody = new FormEncodingBuilder()

                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "JobProfiles/" + id + "/list_of_person?access_token=" + sessionManager.getAccessToken(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_EMPLOYEE");
        httpProcessor.setHttpResponserListener(this);

    }


    private void convJsonData(JSONArray personId, String request) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("id", id);
            jsonObject.put("lastUpdate", request);
            jsonObject.put("updatedBy", sessionManager.getKeyUserId());
            jsonObject.put("job_name", getEditText(R.id.titleBox));
            jsonObject.put("job_details", getEditText(R.id.desc));
            jsonObject.put("job_status", getEditText(R.id.status));

            jsonObject.put("personIds", personId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        updateJobInfo(jsonObject);
    }

    @Override
    public void responseResult(String result, String TAG) {
        try {
            switch (TAG) {
                case "GET_EMPLOYEE":

                    JSONArray jsonArray = new JSONArray(result);
                    jsonArrayPids = new JSONArray();
                    empList = new ArrayList<>();
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(a);


                        Container container = new Container();

                        container.setTitle(jsonObject.optString("first_name"));
                        container.setImage(jsonObject.optString("picture_of_person"));
                        container.setExp(a + " Yr");
                        container.setRating("4." + a);


                        container.setGender(jsonObject.optString("gender"));
                        container.setId(jsonObject.optString("id"));

                        this.jsonArrayPids.put(container.getId());


                        empList.add(container);
                    }
                    setJobAdapter();

                    break;
                case "UPDATE_JOB":
                    JSONObject resulta = new JSONObject(result);
                    if (resulta.optString("lastUpdate").equals("Request")) {
                        Toast.makeText(context, "Request has been sent to user", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Job updated", Toast.LENGTH_SHORT).show();
                    }
                    ;
                    getEmployeeList(id);
                    GetJobInfo();

                    break;
                case "GET_JOB":
                    setData(result);
                    getEmployeeList(id);
                    break;
                default:
            }
        } catch (Exception ignored) {
        //write code here
        }

    }

    private void setJobAdapter() {
        employeeAdapter = new EmployeeAdapter(context, empList);
        recycler.setAdapter(employeeAdapter);


        employeeAdapter.notifyDataSetChanged();
        boolean isMember = false;
        for (int a = 0; a < empList.size(); a++) {
            if (sessionManager.getKeyUserId().equals(empList.get(a).getId())) {
                a = empList.size();
                isMember = true;
            }
        }
        onClickAdapter(isMember);

    }

    private void onClickAdapter(final boolean isMem) {
        employeeAdapter.setOnItemClickListner(new EmployeeAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.card1) {
                    if (isMem) {
                        Intent intent = new Intent(context, Employee_details.class);
                        intent.putExtra("pid", empList.get(position).getId());
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, "You don't have permission ", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });
        employeeAdapter.setOnItemLongClickListner(new EmployeeAdapter.onRecyclerViewItemLongClickListner() {
            @Override
            public void onItemLongClickListner(View view, int position) {
                showDeleteDialog(position);
            }
        });
    }

    private void showDeleteDialog(final int position) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Alert !!!");
        alertDialogBuilder.setMessage("Do you want to remove this employee ?");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        jsonArrayPids.remove(position);
                        convJsonData(jsonArrayPids, "Delete");
                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

// write code here
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void setData(String result) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            setText(R.id.titleBox, jsonObject.optString("job_name"));
            setText(R.id.desc, jsonObject.optString("job_details"));
            setText(R.id.status, jsonObject.optString("job_status"));

            update.setVisibility(View.GONE);
            titleBox.setEnabled(false);
            desc.setEnabled(false);
            status.setEnabled(false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}