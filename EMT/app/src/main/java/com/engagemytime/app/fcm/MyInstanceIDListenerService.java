package com.engagemytime.app.fcm;

import android.util.Log;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

public class MyInstanceIDListenerService extends FirebaseInstanceIdService implements HttpProcessor.HttpResponser {

    private static final String TAG = MyInstanceIDListenerService.class.getSimpleName();
    private String refreshedToken;

    @Override
    public void onTokenRefresh() {
        Log.e(TAG, "onTokenRefresh");
        Container container = new Container();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        container.setFcmDeviceId(refreshedToken);




    }

    private void serverToken(String refreshedToken) {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        this.refreshedToken = refreshedToken;
        RequestBody requestBody = new FormEncodingBuilder()
                .add(API.ACTION, "")
                .add("deviceid", refreshedToken)
                .add("uid", sessionManager.getKeyUserId())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(getApplicationContext(), false, API.BASE_URL_PUSH+"installations/update?where=%7B%22deviceToken%22%3A%22"+sessionManager.getDeviceId()+"%22%7D", HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String results, String TAG) {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        JSONObject result = null;
        try {
            result = new JSONObject(results);
            if (result.optString("status").equals("success")) {
                sessionManager.saveDeviceId(refreshedToken);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}