package com.engagemytime.app.Fragments;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.engagemytime.app.Adapter.ViewPagerAdapter;
import com.engagemytime.app.R;
import com.github.clans.fab.FloatingActionButton;

/**
 * Created by Casa Curo on 9/15/2017.
 */

public class MyProject_Frag extends Fragment implements TabLayout.OnTabSelectedListener {
    View view;
    ViewPager viewPager;
    TabLayout tabLayout;
    ViewPagerAdapter viewPagerAdapter;
    Context context;
    FloatingActionButton assign;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_my_project, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        assign = (FloatingActionButton) view.findViewById(R.id.assign);
        assign.setVisibility(View.GONE);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout.addTab(tabLayout.newTab().setText("On Going"));
        tabLayout.addTab(tabLayout.newTab().setText("History"));
        context = getActivity();

        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        return view;
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

// write code
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

// write code
    }
}

