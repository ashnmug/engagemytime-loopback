package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;


public class SkillAdapter1 extends RecyclerView.Adapter<SkillAdapter1.MyViewHolder> {

    private final Context context;
    private final ArrayList<Container> list;
    private onRecyclerViewItemClickListner itemClickListner;
    private onRecyclerViewItemLongClickListner itemLongClickListner;

    int selected_position = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView title, exp, rating;
        ImageView image;
        TextView text;
        CardView cardView;
        RelativeLayout card;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            text = (TextView) view.findViewById(R.id.text);
//            cardView = (CardView) view.findViewById(R.id.card1);
//            cardView.setOnClickListener(this);
//            cardView.setOnLongClickListener(this);
            image.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListner != null) {
                itemClickListner.onItemClickListner(view, getAdapterPosition());
            }
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
        }

        @Override
        public boolean onLongClick(View v) {
            if (itemLongClickListner != null) {
                itemLongClickListner.onItemLongClickListner(v, getAdapterPosition());
            }
            return true;
        }
    }

    public interface onRecyclerViewItemClickListner {
        void onItemClickListner(View view, int position);
    }

    public interface onRecyclerViewItemLongClickListner {
        void onItemLongClickListner(View view, int position);
    }

    public void setOnItemClickListner(onRecyclerViewItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public void setOnItemLongClickListner(onRecyclerViewItemLongClickListner itemClickListner) {
        this.itemLongClickListner = itemClickListner;
    }

    public SkillAdapter1(Context context, ArrayList<Container> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboar_recycler, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Container movie = list.get(position);
        holder.image.setImageBitmap(movie.getImageResourceBit());
        holder.text.setText(movie.getTitle());
        holder.itemView.setBackgroundColor(selected_position == position ? ContextCompat.getColor(context, R.color.colorPrimary) : Color.TRANSPARENT);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
