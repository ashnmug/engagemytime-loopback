package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {


    private final ArrayList<Container> list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, comment;
        MaterialRatingBar status;

        public MyViewHolder(View view) {
            super(view);
            comment = (TextView) view.findViewById(R.id.comment);
            status = (MaterialRatingBar) view.findViewById(R.id.rating);
            title = (TextView) view.findViewById(R.id.credit);


        }
    }


    public ReviewAdapter(ArrayList<Container> list) {
        this.list = list;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_review, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container places = list.get(position);


        holder.title.setText(places.getTitle());
        holder.comment.setText(places.getComment());
        holder.status.setRating(Float.parseFloat(places.getRating()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
