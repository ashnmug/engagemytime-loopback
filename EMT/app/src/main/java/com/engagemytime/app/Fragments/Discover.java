package com.engagemytime.app.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.os.ResultReceiver;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.engagemytime.app.Activity.ChangePassword;
import com.engagemytime.app.Activity.Employee_details;
import com.engagemytime.app.Activity.Login;
import com.engagemytime.app.Activity.MyProject;
import com.engagemytime.app.Activity.ProjectDetails;
import com.engagemytime.app.Activity.Set_Location;
import com.engagemytime.app.Activity.Sign_Up;
import com.engagemytime.app.Adapter.EmployeeAdapter;
import com.engagemytime.app.Adapter.ProjectAdapter;
import com.engagemytime.app.Adapter.SkillAdapter;
import com.engagemytime.app.Intro.IntroPage;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.MyMarker;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Casa Curo on 9/15/2017.
 */

public class Discover extends Fragment implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, HttpProcessor.HttpResponser, SeekBar.OnSeekBarChangeListener, OnPlaceSelectedListener {
    View view;
    ///////////Map/////////////
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient, client;
    LatLng latLng;
    GoogleMap mGoogleMap;
    SupportMapFragment mFragment;
    Marker currLocationMarker;
    private ArrayList<Container> placeLists;
    private Location mLastLocation;
    ArrayList<MyMarker> markerArrayList;
    private HashMap<Marker, Container> mMarkersHashMap;
    private HashMap<Marker, Container> mMarkersHashMap1;

    private TextWatcher adreeWatcher;
    ////////////////////////


    private ProjectAdapter projectAdapter;
    private Circle mCircle = null;
    private RecyclerView recyclerView, recyclerEmp;
    private ArrayList<Container> arrayList, viewMoreSkill, employeeList, projectList;
    private SkillAdapter skillAdapter;
    private EmployeeAdapter employeeAdapter;
    private Context context;
    ImageView imageView, cancel, menu;
    TextView textProgress, okPrg;
    PlacesAutocompleteTextView autocompleteTextView;
    LinearLayout linearLayoutMap, viewMore;
    RelativeLayout linearProgress;
    FloatingActionButton viewMap;
    VerticalSeekBar progressBar;
    Integer integerP[] = {R.drawable.icon1, R.drawable.icon2, R.drawable.icon3, R.drawable.icon4, R.drawable.icon5, R.drawable.icon6};
    JSONArray empArray = null;
    private String addressCur;
    private Marker currentMarkerSelf = null;
    private SessionManager sessionManager;
    private CircleOptions circleOptions;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager layoutManager;

    ImageView imageError;
    private Toolbar toolbar;
    private PopupMenu popup;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_discover, container, false);
        imageError = (ImageView) view.findViewById(R.id.imageError);
        menu = (ImageView) view.findViewById(R.id.menu);


        getInit();
        onClickFunction();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        resetClient();


    }

    private void resetClient() {
        if (client != null) {
            client.stopAutoManage(getActivity());
            client.disconnect();
        }
    }

    private void getInit() {

        context = getActivity();
        sessionManager = new SessionManager(context);
        imageView = (ImageView) view.findViewById(R.id.search);
        cancel = (ImageView) view.findViewById(R.id.cancel);
        textProgress = (TextView) view.findViewById(R.id.textProgress);
        okPrg = (TextView) view.findViewById(R.id.okPrg);
        progressBar = (VerticalSeekBar) view.findViewById(R.id.progressBar1);
        autocompleteTextView = (PlacesAutocompleteTextView) view.findViewById(R.id.autocompleteTextView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerEmp = (RecyclerView) view.findViewById(R.id.recyclerEmployee);
        linearLayoutMap = (LinearLayout) view.findViewById(R.id.linearLayoutMap);
        viewMore = (LinearLayout) view.findViewById(R.id.viewMore);
        linearProgress = (RelativeLayout) view.findViewById(R.id.linearProgress);
        viewMap = (FloatingActionButton) view.findViewById(R.id.viewMap);
        gridLayoutManager = new GridLayoutManager(context, 2);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setHasOptionsMenu(true);
        menu.setOnClickListener(this);
        textProgress.setText("" + sessionManager.getDistance() + " Km");
        progressBar.setMax(100);

        showPopUp();
        getVisiblity();

    }

    public void signOut() {
        try {
            Auth.GoogleSignInApi.signOut(client).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // updateUI(false);
                        }
                    });
        } catch (Exception e) {
            getLogout();

        }
    }

    private void getVisiblity() {
        autocompleteTextView.setOnPlaceSelectedListener(this);
        if (sessionManager.getRole().equals("Consumer")) {
//            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                    .requestIdToken(getString(R.string.default_web_client_id))
//                    .requestEmail()
//                    .build();
//
//            client = new GoogleApiClient.Builder(context)
//                    .enableAutoManage(getActivity(), this)
//                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                    .build();

            getProjectList();
            viewMap.setVisibility(View.GONE);


        } else if (sessionManager.getRole().equals("Provider")) {

            viewMap.setVisibility(View.VISIBLE);

            initMap();
        }
    }

    private void onClickFunction() {
        viewMore.setOnClickListener(this);
        imageView.setOnClickListener(this);
        cancel.setOnClickListener(this);
        viewMap.setOnClickListener(this);
        okPrg.setOnClickListener(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerEmp.setLayoutManager(gridLayoutManager);
        progressBar.setOnSeekBarChangeListener(this);
    }

    private void showPopUp() {
        popup = new PopupMenu(context, menu);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.menu_scrolling, popup.getMenu());

        if (sessionManager.isThird()) {
            popup.getMenu().getItem(2).setVisible(false);
        }
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();


                if (id == R.id.change_password) {

                    Intent intent = new Intent(context, ChangePassword.class);
                    startActivityForResult(intent, 11);
                } else if (id == R.id.logout) {
                    sessionManager.saveLoggedIn(false);
                    getActivity().finish();
                    startActivity(new Intent(context,IntroPage.class));
                } else if (id == R.id.consumer) {
                    sessionManager.setRole("Consumer");
                    getVisiblity();
                } else if (id == R.id.provider) {
                    sessionManager.setRole("Provider");
                    getVisiblity();
                }
                return true;
            }
        });

        //showing popup menu
    }

    private void getLogout() {

        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/logout?access_token=" + sessionManager.getAccessToken(), HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("LOGOUT");
        httpProcessor.setHttpResponserListener(this);
    }

    private void getProjectList() {

        gettProjectList();
    }

    private void gettProjectList() {
        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "ProjectProfiles?access_token=" + sessionManager.getAccessToken(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_PROJECT");
        httpProcessor.setHttpResponserListener(this);
    }

    private void gettProjectDetails(JSONArray jsonArray) {
        try {
            projectList = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                JSONObject jsonObject = jsonArray.getJSONObject(a);

                Container container = new Container();
                container.setTitle(jsonObject.optString("project_name"));
                container.setAddress_details(jsonObject.optString("project_details"));
                container.setStatus(jsonObject.optString("project_status"));
                container.setName(jsonObject.optString("project_lead"));
                container.setId(jsonObject.optString("id"));
                container.setPrice("$ " + a + 1 + "00");
                container.setImageResource(integerP[a]);


                projectList.add(container);
            }
        } catch (Exception ignored) {

        }
        projectAdapter = new ProjectAdapter(projectList);
        recyclerEmp.setAdapter(projectAdapter);
        projectAdapter.notifyDataSetChanged();
        onClickProjectAdapter();
    }

    private void onClickProjectAdapter() {
        projectAdapter.setOnItemClickListner(new ProjectAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.card) {
                    Intent intent = new Intent(context, ProjectDetails.class);
                    intent.putExtra("pid", projectList.get(position).getId());
                    intent.putExtra("title", projectList.get(position).getTitle());
                    intent.putExtra("status", projectList.get(position).getStatus());
                    intent.putExtra("desc", projectList.get(position).getAddress_details());
                    intent.putExtra("lead", projectList.get(position).getName());
                    // intent.putExtra("array", projectList.get(position).getJsonArray().toString());
                    startActivityForResult(intent, 2);
                }
            }
        });
    }


    private void getEmployeeList(JSONObject jsonObject) {
        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "SkillProfiles?access_token=" + sessionManager.getAccessToken() + "&filter=" + jsonObject.toString(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_EMPLOYEE");
        httpProcessor.setHttpResponserListener(this);

    }

    private void listUpdate(ArrayList<Container> employeeList) {
        employeeAdapter = new EmployeeAdapter(context, employeeList);
        recyclerEmp.setAdapter(employeeAdapter);
        employeeAdapter.notifyDataSetChanged();

        onClickAdapter();
    }

    private void onClickAdapter() {
        employeeAdapter.setOnItemClickListner(new EmployeeAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.card1) {
                    Intent intent = new Intent(context, Employee_details.class);
                    intent.putExtra("pid", employeeList.get(position).getId());
                    startActivityForResult(intent, 3);
                }
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.search) {
            if (autocompleteTextView.getVisibility() == View.VISIBLE) {
                autocompleteTextView.setVisibility(View.GONE);
                autocompleteTextView.setText("");
                imageView.setImageResource(R.drawable.search);
            } else {
                imageView.setImageResource(R.drawable.up_arrow);
                autocompleteTextView.setVisibility(View.VISIBLE);
            }
        } else if (view.getId() == R.id.viewMap) {
            if (linearLayoutMap.getVisibility() == View.VISIBLE) {
                linearLayoutMap.setVisibility(View.GONE);
                linearProgress.setVisibility(View.GONE);
                recyclerEmp.setVisibility(View.VISIBLE);
                viewMap.setImageResource(R.drawable.marker_mini);
                linearProgress.setVisibility(View.GONE);
            } else {

                recyclerEmp.setVisibility(View.GONE);
                linearLayoutMap.setVisibility(View.VISIBLE);
                viewMap.setImageResource(R.drawable.full_screen_mini);

                linearProgress.setVisibility(View.VISIBLE);
                progressBar.setProgress(Integer.parseInt(sessionManager.getDistance()));
            }
        } else if (view.getId() == R.id.viewMore) {
            int tempSize = arrayList.size() - layoutManager.findLastVisibleItemPosition();
            if (tempSize > 4) {
                recyclerView.getLayoutManager().scrollToPosition(layoutManager.findLastVisibleItemPosition() + 3);
            } else {
                recyclerView.getLayoutManager().scrollToPosition(arrayList.size() - 1);
                viewMore.setVisibility(View.GONE);
            }
        } else if (view.getId() == R.id.cancel) {
            autocompleteTextView.setText("");
        } else if (view.getId() == R.id.menu) {

            popup.show();

        }
    }


    @Override
    public void responseResult(String results, String TAG) {
        double lat = 0.0, lng = 0.0;
        try {
            switch (TAG) {
                case "place":
                    JSONObject result = new JSONObject(results);
                    lng = ((JSONArray) result.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lng");
                    lat = ((JSONArray) result.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lat");
                    refresMap(lat, lng);
                    break;
                case "GET_EMPLOYEE":
                    if (results.equals("[]")) {
                        recyclerEmp.setVisibility(View.GONE);
                        imageError.setVisibility(View.VISIBLE);
                    } else {
                        recyclerEmp.setVisibility(View.VISIBLE);
                        imageError.setVisibility(View.GONE);
                    }
                    JSONArray jsonArray = new JSONArray(results);
                    setEmployeeAdapter(jsonArray);
                    break;
                case "GET_PROJECT":
                    JSONArray jsonArray1 = new JSONArray(results);
                    gettProjectDetails(jsonArray1);
                    break;
                case "LOGOUT":
                    sessionManager.saveLoggedIn(false);
                    getActivity().finish();
                    startActivity(new Intent(context, IntroPage.class));
                    break;

                default:
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setEmployeeAdapter(JSONArray jsonArray) {
        try {
            employeeList = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                JSONObject jsonObject = jsonArray.getJSONObject(a);
                JSONObject personJson = jsonObject.optJSONObject("person");
                JSONObject contJson = personJson.optJSONObject("contacts");
                JSONObject mapJson = contJson.optJSONObject("map_coordinates");

                Container container = new Container();

                container.setTitle(personJson.optString("first_name"));
                container.setImage(personJson.optString("picture_of_person"));
                container.setExp(a + " Yr");
                container.setRating("4." + a);
                container.setAddress_details(contJson.optString("portal_address"));
                //   container.setImageResource(R.drawable.maleprofile);
                container.setLat(mapJson.optDouble("lat"));
                container.setLng(mapJson.optDouble("lng"));
                container.setId(personJson.optString("id"));
                container.setGender(personJson.optString("gender"));
                if (contJson.optString("id").equals(sessionManager.getKeyUserId())) {
                    sessionManager.setGrpList(jsonObject.optJSONArray("groupId").toString());
                }

                employeeList.add(container);

            }
            listUpdate(employeeList);
            plotMarkers(employeeList, mLastLocation);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 11) {
                Intent intent = new Intent(context, Login.class);
                startActivity(intent);
                getActivity().finish();
            } else if (requestCode == 2) {
                getVisiblity();

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {


            //Write your code if there's no result
        }
    }


    private void refresMap(double lat, double lng) {
        mMarkersHashMap1 = new HashMap<>();
        sessionManager.setCircleLatitude(lat);
        sessionManager.setCircleLongitude(lng);


        float tempZoom = 0;
        if (Integer.parseInt(sessionManager.getDistance()) <= 30 && Integer.parseInt(sessionManager.getDistance()) > 20) {
            tempZoom = 10;
        } else if (Integer.parseInt(sessionManager.getDistance()) <= 60 && Integer.parseInt(sessionManager.getDistance()) > 30) {
            tempZoom = 9;
        } else if (Integer.parseInt(sessionManager.getDistance()) <= 100 && Integer.parseInt(sessionManager.getDistance()) > 60) {
            tempZoom = 8;
        } else {
            tempZoom = 12;
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lng)).zoom(tempZoom).build();

        mGoogleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

//        Toast.makeText(context, lat + "," + lng, Toast.LENGTH_SHORT).show();
        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(lat);
        location.setLongitude(lng);
        locationFilter(location);
    }

/////////////////////////////  MAP  //////////////////////////////////////////////
//////////////////////----------------//////////////////////////////////


    protected void getLatLng(String address) {
        String uri = "http://maps.google.com/maps/api/geocode/json?address="
                + address + "&sensor=false";
        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, uri, HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("place");
        httpProcessor.setHttpResponserListener(this);
    }

    private void getCustomMarker() {
        if (mGoogleMap != null) {
            mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {
                    marker.showInfoWindow();


                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(marker.getPosition()).zoom(11).build();

                    mGoogleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));

                    return true;
                }
            });
        } else
            Toast.makeText(getActivity(), "Unable to create Maps", Toast.LENGTH_SHORT).show();

    }

    private void initMap() {
        mFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mFragment.getMapAsync(this);
    }

    private void plotMarkers(ArrayList<Container> placeLists, Location mLastLocation) {
        mGoogleMap.clear();

        circleOptions = new CircleOptions();

        circleOptions.center(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())).strokeWidth(3).strokeColor(Color.BLUE).fillColor(Color.parseColor("#5E5AD9A0")).radius(Double.parseDouble(sessionManager.getDistance()) * 1000);
        mCircle = mGoogleMap.addCircle(circleOptions);


        mMarkersHashMap = new HashMap<>();

        if (placeLists.size() > 0) {
            for (Container myMarker : placeLists) {

                // Create user marker with custom icon and other options
                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(myMarker.getLat(), myMarker.getLng()));
                markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_m_marker));

                Marker currentMarker = mGoogleMap.addMarker(markerOption);
                mMarkersHashMap.put(currentMarker, myMarker);

                mGoogleMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
            }
        }
        addCurMarker(mLastLocation.getLatitude(), mLastLocation.getLongitude());


    }

    private void addCurMarker(double lat, double lng) {
        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
        markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_m_marker));


        currentMarkerSelf = mGoogleMap.addMarker(markerOption);

        if (currentMarkerSelf != null) {
            currentMarkerSelf.setPosition(new LatLng(lat, lng));
            Container container = new Container();
            container.setTitle("Current Location");
            container.setAddress_details(addressCur);
            container.setImageResource(R.drawable.red_m_marker);
            container.setLat(mLastLocation.getLatitude());
            container.setLng(mLastLocation.getLongitude());
            container.setGender("Male");
            container.setExp(" ");
            container.setRating("");
            mMarkersHashMap.put(currentMarkerSelf, container);
        }
        mGoogleMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        sessionManager.setDistance(i);
        textProgress.setText("" + sessionManager.getDistance() + " Km");

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

// write code
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        sessionManager.setDistance(seekBar.getProgress());

        refresMap(sessionManager.getCircleLat(), sessionManager.getCircleLong());
        //  linearProgress.setVisibility(View.GONE);
    }

    @Override
    public void onPlaceSelected(@NonNull Place place) {
        addressCur = place.description;
        autocompleteTextView.setText(addressCur);
        getLatLng(place.description);
    }

    private class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View holder = getLayoutInflater().inflate(R.layout.infowindow_layout, null);

            Container myMarker = mMarkersHashMap.get(marker);

            CircleImageView markerIcon = (CircleImageView) holder.findViewById(R.id.marker_icon);

            TextView markerLabel = (TextView) holder.findViewById(R.id.marker_label);
            TextView markerdetails = (TextView) holder.findViewById(R.id.marker_details);
            TextView ratingM = (TextView) holder.findViewById(R.id.ratingM);
            if (myMarker.getGender().equals("Female")) {
                Picasso.with(context)
                        .load(myMarker.getImage()).placeholder(R.drawable.femaleprofile)
                        .into(markerIcon);
            } else {
                Picasso.with(context)
                        .load(myMarker.getImage()).placeholder(R.drawable.maleprofile)
                        .into(markerIcon);
            }

            markerLabel.setText(myMarker.getTitle());
            ratingM.setText(myMarker.getRating());
            markerdetails.setText(myMarker.getAddress_details());

            return holder;
        }

    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        mGoogleMap = gMap;
        mGoogleMap.setMyLocationEnabled(true);
        buildGoogleApiClient();
        mGoogleApiClient.connect();

    }

    public void locationFilter(Location latLng) {

        if (employeeList != null) {
            ArrayList<Container> arrayList = new ArrayList<>();
            for (int count = 0; count < employeeList.size(); count++) {
                Location temp = new Location(LocationManager.GPS_PROVIDER);
                temp.setLatitude(employeeList.get(count).getLat());
                temp.setLongitude(employeeList.get(count).getLng());
                float distance = latLng.distanceTo(temp);
                if (distance <= Float.parseFloat(sessionManager.getDistance()) * 1000) {
                    arrayList.add(employeeList.get(count));
                }
            }
            listUpdate(arrayList);
            plotMarkers(arrayList, latLng);
        }


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
//            currLocationMarker = mGoogleMap.addMarker(markerOptions);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng).zoom(11).build();

            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            getCustomMarker();

            convJsonDataAccess("java");
            refresMap(latLng.latitude, latLng.longitude);
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


    }

    @Override
    public void onConnectionSuspended(int i) {
//        Toast.makeText(this, "onConnectionSuspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
//        Toast.makeText(this, "onConnectionFailed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location mLastLocation) {
        this.mLastLocation = mLastLocation;


    }

    /////////////////////////////////////JSON_CONVERT/////////////////
    private void convJsonDataAccess(String skill) {
        JSONObject json1 = new JSONObject();
        JSONObject json2 = new JSONObject();
        JSONObject json3 = new JSONObject();
        JSONObject json4 = new JSONObject();
        JSONObject json5 = new JSONObject();
        JSONObject json6 = new JSONObject();
        JSONArray jsonA5 = new JSONArray();
        JSONArray jsonA6 = new JSONArray();
        try {

            json2.put("list_of_skills", skill);

            jsonA5.put("first_name");


            json3.put("relation", "person");
            json5.put("relation", "contacts");
            jsonA6.put("map_coordinates");
            jsonA6.put("portal_address");
            json6.put("fields", jsonA6);
            json5.put("scope", json6);
            json4.put("include", json5);
            json3.put("fields", jsonA5);
            json3.put("scope", json4);
            json1.put("where", json2);
            json1.put("include", json3);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        getEmployeeList(json1);
    }


}