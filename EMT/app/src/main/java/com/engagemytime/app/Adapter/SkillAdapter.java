package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.MyViewHolder> {


    private final ArrayList<Container> list;
    private final Context context;
    private onRecyclerViewItemClickListner itemClickListner;
    private onRecyclerViewItemLongClickListner itemLongClickListner;
    int selected_position = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView title, date;
        CircleImageView image;

        LinearLayout linear;

        public MyViewHolder(View view) {
            super(view);
            linear = (LinearLayout) view.findViewById(R.id.linear);
            title = (TextView) view.findViewById(R.id.title);
            image = (CircleImageView) view.findViewById(R.id.image);
            image.setOnClickListener(this);

            image.setOnLongClickListener(this);
            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListner != null) {
                itemClickListner.onItemClickListner(view, getAdapterPosition());

            }
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
        }

        @Override
        public boolean onLongClick(View v) {
            if (itemLongClickListner != null) {
                itemLongClickListner.onItemLongClickListner(v, getAdapterPosition());

            }
            return true;
        }
    }

    public void setOnItemLongClickListner(onRecyclerViewItemLongClickListner itemClickListner) {
        this.itemLongClickListner = itemClickListner;
    }

    public interface onRecyclerViewItemLongClickListner {
        void onItemLongClickListner(View view, int position);
    }

    public void setOnItemClickListner(onRecyclerViewItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public interface onRecyclerViewItemClickListner {
        void onItemClickListner(View view, int position);
    }


    public SkillAdapter(Context context,ArrayList<Container> list) {
        this.list = list;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.skill_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container movie = list.get(position);
        holder.title.setText(movie.getTitle());


        // Here I am just highlighting the background
        holder.itemView.setBackgroundColor(selected_position == position ? ContextCompat.getColor(context, R.color.colorPrimary) : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
