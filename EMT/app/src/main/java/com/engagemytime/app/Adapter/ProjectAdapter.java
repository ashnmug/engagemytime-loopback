package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.engagemytime.app.Activity.ProjectDetails;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;

import java.util.ArrayList;


public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.MyViewHolder> {


    private final ArrayList<Container> list;

    private onRecyclerViewItemClickListner itemClickListner;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, exp, price;
        ImageView image;
        CardView card;

        public MyViewHolder(View view) {
            super(view);
            price = (TextView) view.findViewById(R.id.ratingText);
            title = (TextView) view.findViewById(R.id.title);
            image = (ImageView) view.findViewById(R.id.image);
            card = (CardView) view.findViewById(R.id.card);

            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListner != null) {
                itemClickListner.onItemClickListner(view, getAdapterPosition());
            }
        }
    }
    public interface onRecyclerViewItemClickListner {
        void onItemClickListner(View view, int position);
    }

    public void setOnItemClickListner(ProjectAdapter.onRecyclerViewItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }
    public ProjectAdapter(ArrayList<Container> list) {
        this.list = list;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ongoing_project, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container movie = list.get(position);
        holder.title.setText(movie.getTitle());
        holder.price.setText(movie.getPrice());
        holder.image.setImageResource(movie.getImageResource());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
