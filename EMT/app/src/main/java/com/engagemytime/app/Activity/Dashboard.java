package com.engagemytime.app.Activity;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import android.view.View;
import android.view.WindowManager;

import com.engagemytime.app.Fragments.Calender_Week;
import com.engagemytime.app.Fragments.DashboardFragment;
import com.engagemytime.app.Fragments.MyProject_Frag;
import com.engagemytime.app.Fragments.Notification;
import com.engagemytime.app.Fragments.Profile;

import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;

import com.engagemytime.app.utilities.BaseActivity;

import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Dashboard extends BaseActivity {

    private Context context;
    private Fragment[] fragments;
    private String[] fragmentsTAG;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private View profileView, notiView, viewPostProject, projectView, discoverView;
    private SessionManager sessionManager;

    @Override
    public void onClick(View v) {

        if (v.getId() != R.id.postProjectImage) {
            profileView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
            notiView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
            projectView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
            discoverView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            viewPostProject.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
        }
        if (v.getId() == R.id.discoverLinear) {
            initFragment(0);
            discoverView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        } else if (v.getId() == R.id.postProjLinear) {
            initFragment(1);
            projectView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            discoverView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
        } else if (v.getId() == R.id.profileLinear) {
            initFragment(3);
            profileView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            discoverView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
        } else if (v.getId() == R.id.notiLinear) {
            initFragment(2);
            notiView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            discoverView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
        } else if (v.getId() == R.id.postProjectImage) {
            startNextActivity(Post_Project.class);

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        context = Dashboard.this;
        notiView = (View) findViewById(R.id.viewNoti);
        discoverView = (View) findViewById(R.id.viewDiscover);
        profileView = (View) findViewById(R.id.viewProfile);
        viewPostProject = (View) findViewById(R.id.viewPostProject);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        projectView = (View) findViewById(R.id.viewMyProject);
        discoverView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        sessionManager = new SessionManager(context);


        setOnClickListener(R.id.discoverLinear);

        setOnClickListener(R.id.postProjectImage);

        setOnClickListener(R.id.postProjLinear);

        setOnClickListener(R.id.profileLinear);

        setOnClickListener(R.id.notiLinear);

        initFragment(0);

    }

    @Override
    public void onBackPressed() {

        AlertDialogLogOut();
    }

    private void AlertDialogLogOut() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Alert !!!")
                .setContentText("Are you sure you want to exit ?")
                .setConfirmText("Yes")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        finish();
                    }
                })
                .show();
    }

    private void initFragment(int a) {
        DashboardFragment discover = new DashboardFragment();
        MyProject_Frag myProject_frag = new MyProject_Frag();
        Notification notification = new Notification();
        Profile profile = new Profile();
        Calender_Week calender_week = new Calender_Week();
        fragments = new Fragment[]{discover, myProject_frag, notification, profile, calender_week};
        fragmentsTAG = new String[]{"Discover", "My Project", "Notification", "Profile", "Calendar"};
        if (sessionManager.isNotiAct()) {
            sessionManager.saveNotiAct(false);
            changeFragment(2);
            notiView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            discoverView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));

        } else {
            changeFragment(a);
        }
    }

    public void changeFragment(int pos) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_container, fragments[pos], fragmentsTAG[0]);
        fragmentTransaction.commit();


    }


}