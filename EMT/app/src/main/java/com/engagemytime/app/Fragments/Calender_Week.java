package com.engagemytime.app.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.engagemytime.app.Adapter.EventAdaper;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.github.clans.fab.FloatingActionButton;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Casa Curo on 9/21/2017.
 */

public class Calender_Week extends Fragment implements WeekView.EventClickListener, WeekView.EventLongPressListener, View.OnClickListener, HttpProcessor.HttpResponser, WeekView.EmptyViewClickListener {
    View view;
    private Context context;
    private WeekView mWeekView;
    FloatingActionButton fab;
    private Dialog dialog;
    private EditText event_desc;
    private EditText event_edate, event_etime;
    private EditText event_sdate, event_stime;
    private List<WeekViewEvent> events = null;
    int[] colors = {R.color.material_blue, R.color.color_green_primary, R.color.colorAccent, R.color.red, R.color.materialyellow, R.color.input_colour_hint};
    private String newstring, endString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.calender_week_view, container, false);
        context = getActivity();
        mWeekView = view.findViewById(R.id.weekView);
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        events = new ArrayList<WeekViewEvent>();
        mWeekView.setOnEventClickListener(this);
//        mWeekView.setMonthChangeListener(this);
        mWeekView.setEventLongPressListener(this);
        mWeekView.setEmptyViewClickListener(this);
        getEventFromServer();
        mWeekView.setMonthChangeListener(new MonthLoader.MonthChangeListener() {
            @Override
            public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
                // Populate the week view with some events.


                // Return only the events that matches newYear and newMonth.
                List<WeekViewEvent> matchedEvents = new ArrayList<WeekViewEvent>();
                for (WeekViewEvent event : events) {
                    if (eventMatches(event, newYear, newMonth)) {
                        matchedEvents.add(event);
                    }
                }
                return matchedEvents;
            }
        });

        return view;
    }

    /**
     * Checks if an event falls into a specific year and month.
     *
     * @param event The event to check for.
     * @param year  The year.
     * @param month The month.
     * @return True if the event matches the year and month.
     */
    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && event.getStartTime().get(Calendar.MONTH) == month - 1) || (event.getEndTime().get(Calendar.YEAR) == year && event.getEndTime().get(Calendar.MONTH) == month - 1);
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {

    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {

    }

//    @Override
//    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
//         if (events == null) {
//             events = new ArrayList<WeekViewEvent>();
//         }
//        return events;
//    }

    private Calendar getConvertedDate(String strDate) {
        Calendar cal = Calendar.getInstance();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = sdf.parse(strDate);
            cal.setTime(date);
        } catch (Exception ignored) {
            // write code here
        }
        return cal;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab) {
            if (mWeekView.getNumberOfVisibleDays() == 1) {
                mWeekView.setNumberOfVisibleDays(3);
            } else if (mWeekView.getNumberOfVisibleDays() == 3) {
                mWeekView.setNumberOfVisibleDays(7);
            } else {
                mWeekView.setNumberOfVisibleDays(1);
            }

        } else if (view.getId() == R.id.btn_submit) {
            if (!event_edate.getText().toString().equals("") || !event_etime.getText().toString().equals("")) {
                addJsonEvent(newstring, endString, event_desc.getText().toString());
            } else {
                Toast.makeText(context, "Please select date first", Toast.LENGTH_LONG).show();
            }

        } else if (view.getId() == R.id.event_edate) {
            showDateTimePicker(event_edate);
        } else if (view.getId() == R.id.event_sdate) {
            showDateTimePicker(event_sdate);
        } else if (view.getId() == R.id.cancel) {
            dialog.dismiss();
        }
    }

    private void getEventFromServer() {
        RequestBody requestBody = new FormEncodingBuilder().build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL_CAL + "event/?key=JvHvlP8oqqlc0hHQq49SalyB5lQpaIgfyM5wKPMP&limit=20&offset=25", HttpProcessor.HEADER_GET, requestBody);
        httpProcessor.executeRequest("GET_EVENT");
        httpProcessor.setHttpResponserListener(this);

    }

    private void getBlockSlotFromServer() {
        RequestBody requestBody = new FormEncodingBuilder().build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL_CAL + "slots/?key=JvHvlP8oqqlc0hHQq49SalyB5lQpaIgfyM5wKPMP&status=4", HttpProcessor.HEADER_GET, requestBody);
        httpProcessor.executeRequest("GET_SLOT");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String result, String TAG) {
        if (TAG.equals("ADD_EVENT")) {

            try {
                JSONObject addObject = new JSONObject(result);
                Toast.makeText(context, addObject.optString("error_message"), Toast.LENGTH_LONG).show();

            } catch (Exception ignored) {
                Toast.makeText(context, "Event Added", Toast.LENGTH_LONG).show();
                // write code here
            }
            dialog.cancel();
            getEventFromServer();
        } else if (TAG.equals("GET_EVENT")) {
            try {

                JSONObject getObject = new JSONObject(result);
                events = new ArrayList<WeekViewEvent>();
                JSONArray getObjectArray = getObject.optJSONArray("objects");

                for (int i = 0; i < getObjectArray.length(); i++) {
                    WeekViewEvent event = new WeekViewEvent();
                    Container container = new Container();
                    JSONObject jsonObject = getObjectArray.optJSONObject(i);
                    container.setEnd(jsonObject.optString("dtend"));
                    container.setStart(jsonObject.optString("dtstart"));
                    container.setTitle(jsonObject.optString("summary"));
                    container.setType("public");

                    event.setEndTime(getConvertedDate(jsonObject.optString("dtend")));
                    event.setStartTime(getConvertedDate(jsonObject.optString("dtstart")));
                    event.setName(container.getTitle());
                    event.setColor(getResources().getColor(colors[i % 6]));

                    events.add(event);

                }


                getBlockSlotFromServer();
            } catch (Exception ignored) {
                // write code here
            }
        } else if (TAG.equals("GET_SLOT")) {
            try {

                JSONObject getObject = new JSONObject(result);
                JSONArray getObjectArray = getObject.optJSONArray("objects");

                for (int i = 0; i < getObjectArray.length(); i++) {
                    WeekViewEvent event = new WeekViewEvent();
                    Container container = new Container();
                    JSONObject jsonObject = getObjectArray.optJSONObject(i);
                    container.setEnd(jsonObject.optString("end"));
                    container.setStart(jsonObject.optString("start"));
                    container.setTitle(jsonObject.optString("custom_range"));
                    container.setType("public");

                    event.setEndTime(getConvertedDate(jsonObject.optString("end")));
                    event.setStartTime(getConvertedDate(jsonObject.optString("start")));
                    event.setName("Reserved Slot");
                    event.setColor(getResources().getColor(R.color.input_colour_hint));

                    events.add(event);

                }

                mWeekView.notifyDatasetChanged();

            } catch (Exception ignored) {
                // write code here
            }
        }
    }


    public void showDialog(Calendar time) {
        Date date = time.getTime();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFrmt = new SimpleDateFormat("dd-MM-yy");
        SimpleDateFormat timeFrmt = new SimpleDateFormat("HH:mm:ss");

        String dateString = dateFrmt.format(date);
        String timeString = timeFrmt.format(date);
        newstring = format1.format(date);
        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.create_event);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        event_etime = (EditText) dialog.findViewById(R.id.event_etime);
        event_stime = (EditText) dialog.findViewById(R.id.event_stime);
        event_desc = (EditText) dialog.findViewById(R.id.event_desc);
        event_edate = (EditText) dialog.findViewById(R.id.event_edate);
        event_sdate = (EditText) dialog.findViewById(R.id.event_sdate);

        Button submit = (Button) dialog.findViewById(R.id.btn_submit);
        ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);

        cancel.setOnClickListener(this);
        event_edate.setOnClickListener(this);
        event_sdate.setOnClickListener(this);
        event_sdate.setText(dateString);
        event_stime.setText(timeString);
        submit.setOnClickListener(this);

        dialog.show();

    }

    @Override
    public void onEmptyViewClicked(Calendar time) {
        showDialog(time);
    }

    private void addJsonEvent(String sdate, String edate, String desc) {
        if (!sdate.equals("") || !edate.equals("") || !desc.equals("")) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("collection", "/api/v1/calendar/1/");
                jsonObject.put("dtend", edate);
                jsonObject.put("dtstamp", "2018-01-03T06:29:30");
                jsonObject.put("dtstart", sdate);
                jsonObject.put("owner", "/api/v1/user/sandeep/");
                jsonObject.put("summary", desc);
                jsonObject.put("participant", "/api/v1/user/sandeep/");
                jsonObject.put("tzid", "Asia/Kolkata");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            addEventToServer(jsonObject);
        }
    }

    private void addEventToServer(JSONObject jsonObject) {
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL_CAL + "event/?key=JvHvlP8oqqlc0hHQq49SalyB5lQpaIgfyM5wKPMP", HttpProcessor.HEADER_POST, requestBody);
        httpProcessor.executeRequest("ADD_EVENT");
        httpProcessor.setHttpResponserListener(this);

    }

    private void showDateTimePicker(final EditText editText) {

        LocalDate localDate = LocalDate.now();
        int month = localDate.getMonthOfYear();
        int year = localDate.getYear();
        int day = localDate.getDayOfMonth();
        final SwitchDateTimeDialogFragment dateTimeDialogFragment = SwitchDateTimeDialogFragment.newInstance(
                "Title example",
                "OK",
                "Cancel"
        );

// Assign values
        dateTimeDialogFragment.startAtCalendarView();
        dateTimeDialogFragment.set24HoursMode(true);
        dateTimeDialogFragment.setMinimumDateTime(new GregorianCalendar(year, month - 1, day).getTime());
        dateTimeDialogFragment.setMaximumDateTime(new GregorianCalendar(year, month - 1, day + 7).getTime());
        dateTimeDialogFragment.setDefaultDateTime(new GregorianCalendar(year, month - 1, day, 0, 0).getTime());

        try {
            dateTimeDialogFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            //write code here
        }

// Set listener
        dateTimeDialogFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                // Date is get on positive button click
                // Do something
                SimpleDateFormat dateFrmt = new SimpleDateFormat("dd-MM-yy");
                SimpleDateFormat timeFrmt = new SimpleDateFormat("HH:mm:ss");

                if (editText.getId() == R.id.event_edate) {
                    endString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);
                    String dateStr = dateFrmt.format(date);
                    String timeString = timeFrmt.format(date);
                    event_edate.setText(dateStr);
                    event_etime.setText(timeString);

                } else {
                    newstring = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);
                    String dateStr = dateFrmt.format(date);
                    String timeString = timeFrmt.format(date);
                    event_sdate.setText(dateStr);
                    event_stime.setText(timeString);
                }

            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

// Show
        dateTimeDialogFragment.show(getActivity().getSupportFragmentManager(), "dialog_time");
    }

}