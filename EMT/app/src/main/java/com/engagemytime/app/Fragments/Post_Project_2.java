package com.engagemytime.app.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.engagemytime.app.Activity.Post_Project;
import com.engagemytime.app.Activity.Search_Employee;
import com.engagemytime.app.Model.ChangeNotify;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.ArrayList;


/**
 * Created by Casa Curo on 9/16/2017.
 */

public class Post_Project_2 extends Fragment implements View.OnClickListener {
    View view;
    Button next, back;
    public String lead;
    private ChangeNotify changeNotifyListener;
    private TextView textView;
    private ArrayList<Container> employeeList;
    private ArrayList<String> employeeName;
    private ArrayAdapter<String> adapter;
    private String personId = null;
    SessionManager sessionManager;

    public void setChangeNotifyListener(ChangeNotify changeNotifyListener) {
        this.changeNotifyListener = changeNotifyListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_post_project_2, container, false);
        next = (Button) view.findViewById(R.id.submit_project);
        next.setOnClickListener(this);
        back = (Button) view.findViewById(R.id.back);
        back.setOnClickListener(this);
        employeeName = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, employeeName);
        textView = (TextView)
                view.findViewById(R.id.textView);
        sessionManager = new SessionManager(getActivity());
        textView.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.submit_project) {
            if (null != changeNotifyListener) {
                if (personId == null) {
                    Toast.makeText(getActivity(), "Please select a lead for this project", Toast.LENGTH_SHORT).show();

                } else {
                    lead = personId;

                    sessionManager.setTeamLead(lead);
                    changeNotifyListener.changeFragment(Post_Project.STEP_3, "Step 3");
                }
            }
        } else if (view.getId() == R.id.back) {
            if (null != changeNotifyListener) {
                if (personId == null) {
                    Toast.makeText(getActivity(), "Please select a lead for this project", Toast.LENGTH_SHORT).show();

                } else {
                    lead = personId;
                    sessionManager.setTeamLead(lead);
                    changeNotifyListener.changeFragment(Post_Project.STEP_1, "Step 1");
                }
            }
        } else if (view.getId() == R.id.textView) {
            Intent intent = new Intent(getActivity(), Search_Employee.class);
            startActivityForResult(intent, 2);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 2) {

                personId = data.getStringExtra("id");
                String name = data.getStringExtra("name");
                textView.setText(name);
            }
        }

    }


}