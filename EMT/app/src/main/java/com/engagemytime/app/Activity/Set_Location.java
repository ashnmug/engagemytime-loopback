package com.engagemytime.app.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.engagemytime.app.Adapter.RecyclerAdaper;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.MyMarker;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Set_Location extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, HttpProcessor.HttpResponser, PermissionListener {

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    LatLng latLng;
    GoogleMap mGoogleMap;
    SupportMapFragment mFragment;
    Marker currLocationMarker;
    RelativeLayout relativeLayout;
    LinearLayout linearLayout;
    RecyclerView recycler;
    private Context context;
    private ArrayList<Container> placeLists;
    private Location mLastLocation;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.resize) {
            int a = linearLayout.getLayoutParams().height;
            if (a == -1) {
                RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 750);
                linearLayout.setLayoutParams(parms);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng).zoom(18).build();

                mGoogleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
                setImageResource(R.drawable.full_screen, R.id.resize);
            } else {
                RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                linearLayout.setLayoutParams(parms);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng).zoom(15).build();

                mGoogleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
                setImageResource(R.drawable.normal_screen, R.id.resize);
            }
        } else if (v.getId() == R.id.relative) {
            if (mLastLocation != null) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("address", placeLists.get(0).getAddress_details());
                returnIntent.putExtra("result", String.valueOf(mLastLocation.getLatitude()) + "," + String.valueOf(mLastLocation.getLongitude()));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                Toast.makeText(context, "Error in find location", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        initToolBar("Step 1");
        context = Set_Location.this;
        TedPermission.with(this)
                .setPermissionListener(this)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();

    }

    private void checkLocation() {
        boolean gps_enabled = false;
        boolean network_enabled = false;

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        //write code
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        //write code here
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(" GPS is not enabled please enable GPS");
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);

                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }

    }

    private void getIniti() {
        checkLocation();
        mFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mFragment.getMapAsync(this);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative);
        linearLayout = (LinearLayout) findViewById(R.id.linear);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        setOnClickListener(R.id.resize);
        setOnClickListener(R.id.relative);

        RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 750);
        linearLayout.setLayoutParams(parms);

        setImageResource(R.drawable.full_screen, R.id.resize);
    }

    private void setDummyList(ArrayList<Container> arrayList) {
        RecyclerAdaper mAdapter = new RecyclerAdaper(context, arrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        mGoogleMap = gMap;
        mGoogleMap.setMyLocationEnabled(true);
        buildGoogleApiClient();
        mGoogleApiClient.connect();

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            currLocationMarker = mGoogleMap.addMarker(markerOptions);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng).zoom(18).build();

            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            getPlaces(mLastLocation);
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


    }

    @Override
    public void onConnectionSuspended(int i) {
//      write code
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
//      write code
    }

    @Override
    public void onLocationChanged(Location mLastLocation) {
        this.mLastLocation = mLastLocation;
        latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.cancel));

        if (currLocationMarker == null) {
            currLocationMarker = mGoogleMap.addMarker(markerOptions);
        } else {
            currLocationMarker.setPosition(latLng);

        }


    }

    private void getPlaces(Location location) {
        String API_KEY = "AIzaSyC1_FdTpyALgy5OKtWOhlO1VFcuFlYVPMs";
        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=" + API_KEY + "&location=" + String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()) + "&radius=50&type=", HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("place");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void responseResult(String results, String TAG) {
        try {
            JSONObject result = new JSONObject(results);
            if (TAG.equals("place")) {
                JSONArray jsonArray = result.getJSONArray("results");
                placeLists = new ArrayList<>();
                for (int a = 0; a < jsonArray.length(); a++) {
                    Container container = new Container();
                    JSONObject jsonObject = jsonArray.getJSONObject(a);
                    JSONObject jsonArrayGeo = jsonObject.getJSONObject("geometry");
                    JSONObject locJson = jsonArrayGeo.getJSONObject("location");
                    double latitude = Double.parseDouble(locJson.getString("lat"));
                    double longitude = Double.parseDouble(locJson.getString("lng"));
                    container.setTitle(jsonObject.getString("name"));
                    container.setAddress_details(jsonObject.getString("vicinity"));
                    container.setImage(jsonObject.getString("icon"));
                    container.setLat(latitude);
                    container.setLng(longitude);
                    placeLists.add(container);

                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.green_nearby);
                    Bitmap b = bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, 30, 30, false);
                    LatLng latLng1 = new LatLng(latitude, longitude);
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng1)
                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

                }
                setDummyList(placeLists);
            }
        } catch (Exception ignored) {

// write code
        }
    }

    @Override
    public void onPermissionGranted() {
        getIniti();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

// write code
    }
}