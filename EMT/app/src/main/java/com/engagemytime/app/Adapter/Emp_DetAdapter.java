package com.engagemytime.app.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.engagemytime.app.Fragments.Access_Control;
import com.engagemytime.app.Fragments.Contact;
import com.engagemytime.app.Fragments.Group;
import com.engagemytime.app.Fragments.Personal;
import com.engagemytime.app.Fragments.Review;
import com.engagemytime.app.Fragments.Skill_info;

import java.util.ArrayList;


public class Emp_DetAdapter extends FragmentStatePagerAdapter {
    ArrayList<Fragment> mNumOfTabs;

    public Emp_DetAdapter(FragmentManager fm, ArrayList<Fragment> mNumOfTabs) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                return  this.mNumOfTabs.get(0);
            case 1:
                return  this.mNumOfTabs.get(1);
            case 2:
                return  this.mNumOfTabs.get(2);
            case 3:
                return  this.mNumOfTabs.get(3);
            case 4:
                return  this.mNumOfTabs.get(4);
            case 5:
                return  this.mNumOfTabs.get(5);
            case 6:
                return  this.mNumOfTabs.get(6);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs.size();
    }
}