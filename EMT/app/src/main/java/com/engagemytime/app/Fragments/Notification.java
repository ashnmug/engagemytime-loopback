package com.engagemytime.app.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.engagemytime.app.Activity.Noti_Setting;
import com.engagemytime.app.Adapter.RecyclerListAdaper;
import com.engagemytime.app.Adapter.SkillAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Casa Curo on 9/15/2017.
 */

public class Notification extends Fragment implements View.OnClickListener, HttpProcessor.HttpResponser {
    View view;
    ImageView notification_setting;
    private RecyclerView recycler;
    private ArrayList<Container> notiList;
    JSONArray personIds = null;
    private RecyclerListAdaper notiListAdapter;
    private Context context;
    private static final String UPDATE_JOB = "UPDATE_JOB";
    private SessionManager sessionManager;
    private Container container;
    private JSONObject jsonObjectEvent = null;
    int positionG = 0;
    private JSONArray jsonArrayCont = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_notification, container, false);
        context = getActivity();
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        sessionManager = new SessionManager(context);


        notification_setting = (ImageView) view.findViewById(R.id.notification_setting);
        notification_setting.setOnClickListener(this);
        getContact();
        return view;
    }

    private void getNotiData(final ArrayList<Container> notiList) {


        notiListAdapter = new RecyclerListAdaper(context, notiList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(notiListAdapter);
        notiListAdapter.notifyDataSetChanged();
        notiListAdapter.setOnItemClickListner(new RecyclerListAdaper.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.accept) {
                  positionG = position;
                    if (notiList.get(position).getJsonObject().optString("lastUpdate").equals("Request")) {

                        if (notiList.get(position).getTitle().equals("Job Update")) {
                            updateJob(notiList.get(position).getJsonObject(), "JobProfiles", UPDATE_JOB);
                        } else if (notiList.get(position).getTitle().equals("Group Update")) {
                            updateJob(notiList.get(position).getJsonObject(), "Groups", UPDATE_JOB);
                        }

                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), Noti_Setting.class);
        startActivity(intent);
    }

    private void getContact() {
        RequestBody requestBody = new FormEncodingBuilder()
                //   .add("access_token", sessionManager.getAccessToken())
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL_PUSH + "Events?filter[order]=Timestamp%20DESC&access_token=" + sessionManager.getAccessToken(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_NOTIFICATION");
        httpProcessor.setHttpResponserListener(this);

    }

    private void updateJob(JSONObject jsonObject, String target, String request) {

        String baseUrl = "";
        try {
            if (target.equals("Events")) {
                baseUrl = API.BASE_URL_PUSH;
                jsonObject.put("EventOriginator", "Accepted");
            } else {

                jsonObject.put("lastUpdate", "Accepted");

                baseUrl = API.BASE_URL;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());

        HttpProcessor httpProcessor = new HttpProcessor(context, true, baseUrl + target + "?access_token=" + sessionManager.getAccessToken(), HttpProcessor.PATCH, requestBody);
        httpProcessor.executeRequest(request);
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String result, String TAG) {
        try {

            switch (TAG) {
                case "GET_NOTIFICATION":
                    notiList = new ArrayList<>();

                    jsonArrayCont = new JSONArray(result);

                    for (int a = 0; a < jsonArrayCont.length(); a++) {
                        jsonObjectEvent = jsonArrayCont.getJSONObject(a);
                        JSONObject jsonObject1 = jsonObjectEvent.getJSONObject("noti");
                        container = new Container();
                        container.setTitle(jsonObjectEvent.optString("EventName"));
                        container.setStatus(jsonObjectEvent.optString("EventOriginator"));
                        container.setFirst(jsonObjectEvent.optString("EventDestination"));
                        container.setDate(jsonObjectEvent.optString("Timestamp"));
                        container.setComment(jsonObjectEvent.optString("description"));
                        container.setImage(jsonObjectEvent.optString("profilepic"));
                        container.setStatus(jsonObjectEvent.optString("status"));
                        container.setJsonObject(jsonObject1);
                        container.setJsonArray(jsonObject1.optJSONArray("personIds"));
                        notiList.add(container);
                    }
                    getNotiData(notiList);
                    sessionManager.saveNotiAct(false);
                    break;
                case UPDATE_JOB:
                    JSONObject jsonObject = jsonArrayCont.getJSONObject(positionG);
                    jsonObject.put("status", 1);
                    updateJob(jsonObject, "Events", "UPDATE_EVENT");
                    break;
                case "UPDATE_EVENT":

                    getContact();
                    break;

                default:
            }
        } catch (Exception ignored) {

        }
    }
}
