package com.engagemytime.app.utilities;

import android.content.Context;

import com.squareup.okhttp.MediaType;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Casa Curo on 5/20/2017.
 */

public class API {
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

  //  public static final String IMAGE_URL ="http://88.198.38.226:3001/api/uploadFiles/profilepic/download/";
     public static final String IMAGE_URL ="http://localhost:3001/api/uploadFiles/profilepic/download/";
    public static final String RESUME_URL ="http://localhost:3001/api/uploadFiles/resume/download/";
  //  public static final String RESUME_URL ="http://88.198.38.226:3001/api/uploadFiles/resume/download/";

    public static final String BASE_URL = "http://localhost:3001/api/";
    public static final String BASE_URL_CAL = "http://localhost:8000/api/v1/";
  //  public static final String BASE_URL = "http://88.198.38.226:3001/api/";
    public static final String BASE_URL_PUSH = "http://localhost:3003/api/";
  //  public static final String BASE_URL_PUSH = "http://88.198.38.226:3003/api/";
    public static final String ACTION = "action";

    public static String getFrmtdDate(String userInput, String formate) {
        String date = "";

        try {
            SimpleDateFormat spf = new SimpleDateFormat(formate);
            Date newDate = spf.parse(userInput);

            spf = new SimpleDateFormat("HH:mm");
            date = spf.format(newDate);

        } catch (ParseException e) {
            // execution will come here if the String that is given
            // does not match the expected format.
            e.printStackTrace();
        }
        return date;
    }
}
