package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.R;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.engagemytime.app.utilities.API.getFrmtdDate;


public class EventAdaper extends RecyclerView.Adapter<EventAdaper.MyViewHolder> {

    private final Context context;
    private final ArrayList<Container> list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, start, end;
        ImageView image;
        RelativeLayout relative;
        CardView card;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            start = (TextView) view.findViewById(R.id.start);
            end = (TextView) view.findViewById(R.id.end);
            image = (ImageView) view.findViewById(R.id.image);
            card = (CardView) view.findViewById(R.id.card);


        }
    }


    public EventAdaper(Context context, ArrayList<Container> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container places = list.get(position);
        holder.title.setText(places.getTitle());
        holder.start.setText(getFrmtdDate(places.getStart(),"yyyy-MM-dd'T'hh:mm:ss"));
        holder.end.setText(getFrmtdDate(places.getEnd(),"yyyy-MM-dd'T'hh:mm:ss"));
        switch (places.getType()) {
            case "private":
                holder.image.setImageResource(R.drawable.pswrd);
                holder.card.setCardBackgroundColor(ContextCompat.getColorStateList(context, R.color.red));
                break;
            case "group":
                holder.image.setImageResource(R.drawable.grp);
                holder.card.setCardBackgroundColor(ContextCompat.getColorStateList(context, R.color.material_blue));
                break;
            case "friend":
                holder.image.setImageResource(R.drawable.grp);
                holder.card.setCardBackgroundColor(ContextCompat.getColorStateList(context, R.color.materialyellow));
                break;
            default:
                holder.image.setImageResource(R.drawable.cal_img);
                holder.card.setCardBackgroundColor(ContextCompat.getColorStateList(context, R.color.color_green_primary));
                break;
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
