package com.engagemytime.app.fcm;

import android.os.Bundle;

import java.util.Map;

/**
 * Created by Casa Curo on 5/21/2017.
 */

public interface NotiLstner {
    public void messageReceived(Map messageText);
}