package com.engagemytime.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Add_Experience extends BaseActivity implements HttpProcessor.HttpResponser {

   private Context context;
    private SessionManager sessionManager;
    JSONArray array = null;
    String id;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {
            JSONObject jsonObject1 = new JSONObject();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("company_name", getEditText(R.id.company));
                jsonObject.put("current_work", true);
                jsonObject.put("description", getEditText(R.id.desc));
                jsonObject.put("industry", getEditText(R.id.Industry));
                jsonObject.put("year_experience", getEditText(R.id.exp));
                jsonObject.put("role", getEditText(R.id.title));

                if (array == null) {
                    array = new JSONArray();
                    array.put(jsonObject);
                } else {
                    array.put(jsonObject);
                }
                jsonObject1.put("list_of_experiences", array);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            updateSkillInfo(jsonObject1);
        } else if (v.getId() == R.id.submitSkill) {
            JSONObject jsonObject = new JSONObject();
            if (array == null) {
                array = new JSONArray();
                array.put(getEditText(R.id.skill));
            } else {
                array.put(getEditText(R.id.skill));
            }
            try {
                jsonObject.put("list_of_skills", array);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            updateSkillInfo(jsonObject);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__experience);
        initToolBar("Add Experience");
        context = Add_Experience.this;
        setOnClickListener(R.id.submit);
        setOnClickListener(R.id.submitSkill);
        sessionManager = new SessionManager(context);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Intent intent = this.getIntent();


        String jsonArray = intent.getStringExtra("skill");
        id = intent.getStringExtra("id");

        String type = intent.getStringExtra("type");
        if (type.equals("add_skill")) {
            hideVisibility(R.id.linear);
            showVisibility(R.id.linearSkill);

        } else {
            showVisibility(R.id.linear);
            hideVisibility(R.id.linearSkill);

        }
        try {
            if (jsonArray.equals("")) {
                array = new JSONArray();
            } else {
                array = new JSONArray(jsonArray);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void updateSkillInfo(JSONObject jsonObject) {
        JSONObject jsonObjecta = new JSONObject();
        JSONObject jsonObjectP = new JSONObject();
        try {
            jsonObjecta.put("id", id);
            jsonObjectP.put("where", jsonObjecta);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filter = "where=" + jsonObjecta.toString();
        RequestBody requestBody = RequestBody.create(API.JSON, jsonObject.toString());
        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "SkillProfiles/update?access_token=" + sessionManager.getAccessToken() + "&" + filter, HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("UPDATE_PERSON");
        httpProcessor.setHttpResponserListener(this);
    }

    @Override
    public void responseResult(String result, String TAG) {

        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }
}
