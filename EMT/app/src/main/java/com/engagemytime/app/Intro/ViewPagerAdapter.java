package com.engagemytime.app.Intro;

/**
 * Created by Casa Curo on 5/14/2017.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ViewPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                First tab1 = new First();
                return tab1;
            case 1:
                Second tab2 = new Second();
                return tab2;
            case 2:
                Third tab3 = new Third();
                return tab3;
            case 3:
                Second tab4 = new Second();
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}