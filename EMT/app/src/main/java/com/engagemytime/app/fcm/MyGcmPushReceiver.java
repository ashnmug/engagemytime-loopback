package com.engagemytime.app.fcm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.engagemytime.app.Activity.Dashboard;
import com.engagemytime.app.Intro.IntroPage;
import com.engagemytime.app.Model.SessionManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;


public class MyGcmPushReceiver extends FirebaseMessagingService {
    public static NotiLstner mListener;
    private NotificationUtils notificationUtils;
    private Intent resultIntent;
    private String EventName = "";
    private String EventOriginator = "";
    private String personId = "";
    private String groupid = "";

    /**
     * Called when message is received.
     * <p>
     * //  * @param from   SenderID of the sender.
     * // * @param bundle Data bundle containing message data as key/value pairs.
     * For Set of keys use data.keySet().
     */

    @Override
    public void onMessageReceived(RemoteMessage map) {
        Map bundle = map.getData();

        try {
            EventName = bundle.get("EventName").toString();
            EventOriginator = bundle.get("description").toString();

            personId = bundle.get("EventDestination").toString();
            String time = bundle.get("Timestamp").toString();
            String imgUrl = bundle.get("profilepic").toString();
            SessionManager sessionManager = new SessionManager(getApplicationContext());
            sessionManager.saveNotiAct(true);

            resultIntent = new Intent(getApplicationContext(), Dashboard.class);


            showNotificationMessage(getApplicationContext(), EventName, EventOriginator, personId, resultIntent, imgUrl);

        } catch (Exception e) {
            showNotificationMessage(getApplicationContext(), EventName, EventOriginator, personId, resultIntent, "");

        }
        if (null != mListener) {
            //Pass on the text to our listener.
            mListener.messageReceived(bundle);
        }
    }

    public static void bindListener(NotiLstner listener) {
        mListener = listener;
    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent, String imgUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imgUrl);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}