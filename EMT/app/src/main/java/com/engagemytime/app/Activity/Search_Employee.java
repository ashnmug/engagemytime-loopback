package com.engagemytime.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.engagemytime.app.Adapter.EmployeeAdapter;
import com.engagemytime.app.Adapter.SkillAdapter;
import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Casa Curo on 9/15/2017.
 */

public class Search_Employee extends BaseActivity implements View.OnClickListener, HttpProcessor.HttpResponser {
    View view;
    private RecyclerView recyclerEmp;
    private ArrayList<Container> employeeList;
    private EmployeeAdapter employeeAdapter;
    private Context context;
    ImageView imageView;
    PlacesAutocompleteTextView autocompleteTextView;

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_discover);

        context = Search_Employee.this;
        sessionManager = new SessionManager(context);
        imageView = (ImageView) findViewById(R.id.search);
        autocompleteTextView = (PlacesAutocompleteTextView) findViewById(R.id.autocompleteTextView);
        recyclerEmp = (RecyclerView) findViewById(R.id.recyclerEmployee);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);

        imageView.setOnClickListener(this);
        recyclerEmp.setLayoutManager(gridLayoutManager);


        convJsonDataAccess("java");
    }

    private void getEmployeeList(JSONObject jsonObject) {
        RequestBody requestBody = new FormEncodingBuilder()
                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "SkillProfiles?access_token=" + sessionManager.getAccessToken() + "&filter=" + jsonObject.toString(), HttpProcessor.GET, requestBody);
        httpProcessor.executeRequest("GET_EMPLOYEE");
        httpProcessor.setHttpResponserListener(this);

    }

    private void convJsonDataAccess(String skill) {
        JSONObject json1 = new JSONObject();
        JSONObject json2 = new JSONObject();
        JSONObject json3 = new JSONObject();
        JSONObject json4 = new JSONObject();
        JSONObject json5 = new JSONObject();
        JSONObject json6 = new JSONObject();
        JSONArray jsonA5 = new JSONArray();
        JSONArray jsonA6 = new JSONArray();


        try {

            json2.put("list_of_skills", skill);

            jsonA5.put("first_name");


            json3.put("relation", "person");
            json5.put("relation", "contacts");
            jsonA6.put("map_coordinates");
            jsonA6.put("portal_address");
            json6.put("fields", jsonA6);
            json5.put("scope", json6);
            json4.put("include", json5);
            json3.put("fields", jsonA5);
            json3.put("scope", json4);
            json1.put("where", json2);
            json1.put("include", json3);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        getEmployeeList(json1);
    }


    @Override
    public void onClick(View view) {
// write code
    }


    @Override
    public void responseResult(String results, String TAG) {

        double lat = 0.0, lng = 0.0;


        try {
            switch (TAG) {
                case "place":
                    JSONObject result = new JSONObject(results);
                    JSONObject locOblect = ((JSONArray) result.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location");
                    lng = locOblect.getDouble("lng");
                    lat = locOblect.getDouble("lat");
                    Toast.makeText(context, lat + "," + lng, Toast.LENGTH_SHORT).show();
                    break;
                case "GET_EMPLOYEE":

                    JSONArray jsonArray = new JSONArray(results);

                    employeeList = new ArrayList<>();
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(a);
                        JSONObject personJson = jsonObject.optJSONObject("person");
                        JSONObject contJson = personJson.optJSONObject("contacts");
                        JSONObject mapJson = contJson.optJSONObject("map_coordinates");

                        Container container = new Container();

                        container.setTitle(personJson.optString("first_name"));
                        container.setFirst(container.getTitle());
                        container.setLast(personJson.optString("family_name"));
                        container.setImage(personJson.optString("picture_of_person"));
                        container.setExp(a + " Yr");
                        container.setRating("4." + a);
                        container.setAddress_details(contJson.optString("portal_address"));

                        container.setLat(mapJson.optDouble("lat"));
                        container.setLng(mapJson.optDouble("lng"));
                        container.setId(contJson.optString("personId"));
                        container.setGender(personJson.optString("gender"));
                        employeeList.add(container);
                    }

                    listUpdate();

                    break;
                default:
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void listUpdate() {

        employeeAdapter = new EmployeeAdapter(context, employeeList);
        recyclerEmp.setAdapter(employeeAdapter);
        employeeAdapter.notifyDataSetChanged();

        onClickAdapter();
    }

    private void onClickAdapter() {
        employeeAdapter.setOnItemClickListner(new EmployeeAdapter.onRecyclerViewItemClickListner() {
            @Override
            public void onItemClickListner(View view, int position) {
                if (view.getId() == R.id.card1) {
                    Intent intent = new Intent();
                    intent.putExtra("id", employeeList.get(position).getId());
                    intent.putExtra("name", employeeList.get(position).getFirst() + " " + employeeList.get(position).getLast());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });


    }
}