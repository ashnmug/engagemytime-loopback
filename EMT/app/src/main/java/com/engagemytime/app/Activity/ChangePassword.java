package com.engagemytime.app.Activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.engagemytime.app.utilities.API;
import com.engagemytime.app.utilities.BaseActivity;
import com.engagemytime.app.utilities.HttpProcessor;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;


public class ChangePassword extends BaseActivity implements HttpProcessor.HttpResponser {
    Context context;
    SessionManager sessionManager;
    private String newPassword, oldPassword;

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.submit) {
            oldPassword = getEditText(R.id.oldPassword);
            newPassword = getEditText(R.id.newPassword);
            String cnewPassword = getEditText(R.id.cnewPassword);

            if (oldPassword.equals("") || cnewPassword.equals("") || newPassword.equals("")) {
                showToast("Field should not be empty");
            } else if (!newPassword.equals(cnewPassword)) {

                showToast("New Password and Confirm Password mut be same");
            } else if (oldPassword.length() < 4 || newPassword.length() < 4 || cnewPassword.length() < 4) {
                showToast("Password length should be more then 4 character");
            } else {
                changePassword();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        context = ChangePassword.this;
        initToolBar("Change Password");
        sessionManager = new SessionManager(context);
        setOnClickListener(R.id.submit);

    }

    private void changePassword() {
        RequestBody requestBody = new FormEncodingBuilder().add("oldPassword", oldPassword).add("newPassword", newPassword)

                .build();

        HttpProcessor httpProcessor = new HttpProcessor(context, true, API.BASE_URL + "Persons/change-password?access_token=" + sessionManager.getAccessToken(), HttpProcessor.POST, requestBody);
        httpProcessor.executeRequest("Change_Password");
        httpProcessor.setHttpResponserListener(this);

    }

    @Override
    public void responseResult(String results, String TAG) {
        try {

            switch (TAG) {
                case "Change_Password":

                    JSONObject jsonObject = new JSONObject(results);
                    if (jsonObject.optString("title").equals("Password changed successfully")) {
                        String title = jsonObject.optString("title") + "\n" + jsonObject.optString("content");
                        View_Success_Dialog view_success_dialog = new View_Success_Dialog();
                        view_success_dialog.showDialog(context, title);


                    }
                    break;
                default:
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class View_Success_Dialog {

        private void showDialog(Context activity, String title) {


            final Dialog dialog = new Dialog(activity);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialogue_success);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button cancel = (Button) dialog.findViewById(R.id.cancel);
            TextView titleText = (TextView) dialog.findViewById(R.id.title);
            TextView tempPswrd = (TextView) dialog.findViewById(R.id.tempPswrd);
            titleText.setText(title);
            tempPswrd.setText(getEditText(R.id.newPassword));
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    setResult(RESULT_OK);
                    finish();

                }
            });

            dialog.show();

        }
    }

}
