package com.engagemytime.app.Adapter;

/**
 * Created by Casa Curo on 6/5/2017.
 */

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.engagemytime.app.Model.Container;
import com.engagemytime.app.Model.SessionManager;
import com.engagemytime.app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerListAdaper extends RecyclerView.Adapter<RecyclerListAdaper.MyViewHolder> {

    private final Context context;
    private final ArrayList<Container> list;
    private onRecyclerViewItemClickListner itemClickListner;



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, accept, textViewDate, date,desc;
        CardView status;
        CircleImageView image;


        public MyViewHolder(View view) {
            super(view);
            status = (CardView) view.findViewById(R.id.status);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            accept = (TextView) view.findViewById(R.id.accept);
            date = (TextView) view.findViewById(R.id.date);
            textViewDate = (TextView) view.findViewById(R.id.textViewDate);
            image = (CircleImageView) view.findViewById(R.id.imageView);
            accept.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (itemClickListner != null) {
                itemClickListner.onItemClickListner(v, getAdapterPosition());
            }
        }
    }
    public interface onRecyclerViewItemClickListner {
        void onItemClickListner(View view, int position);
    }

    public void setOnItemClickListner(RecyclerListAdaper.onRecyclerViewItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }


    public RecyclerListAdaper(Context context, ArrayList<Container> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_notification, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Container places = list.get(position);
        final Container olDplaces;
        if (position > 0) {
            olDplaces = list.get(position - 1);
            if (olDplaces.getDate().equals(places.getDate())) {
                holder.date.setVisibility(View.GONE);
            } else {
                holder.date.setVisibility(View.VISIBLE);
            }
        }
        if (places.getJsonObject().optString("lastUpdate").equals("Request")) {
           if(places.getStatus().equals("0")){
               holder.status.setVisibility(View.VISIBLE);
           }else{
               holder.status.setVisibility(View.GONE);
           }
        }
        holder.title.setText(places.getTitle());
        holder.desc.setText(places.getComment());
        holder.date.setText(places.getDate());
        holder.textViewDate.setText(places.getDate());

        Picasso.with(context)
                .load(places.getImage()).placeholder(R.drawable.maleprofile).error(R.drawable.maleprofile)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
